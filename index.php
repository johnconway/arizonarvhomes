<!doctype html>
<html>
<head>
<title>Arizona RV Homes is Valley View @ Sunrise Hills - Custom Built Arizona Houses with Large RV Garges</title>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description" content="Arizona RV Homes are Unique Homes with RV Garages that are located inside of Valley View @ Sunrise Hills. We are part of a master planned community of 500 acres that stretches from Joy Lane to Boundary Cone Rd. and starts at 2530 Nez Perce Rd. Ft. Mohave, AZ 86427 ">
<meta property="og:title" content="Arizona RV Homes">
<meta name="author" content="DonFeightner@Gmail.com">
<meta property="og:description" content="Homes with Large RV Garages on 500 acres that stretches from Joy Lane to Boundary Cone Rd. in Ft. Mohave AZ. Stop by or call (928) 768-2900 to discuss our lots/land and homes available for purchase.">
<meta property="og:url" content="http://arizonarvhomes.com">
<meta property="og:image" content="http://arizonarvhomes.com/images/azrvhomes-thumbnail.jpg">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="gallery/JS/jquery.migrate.1.1.1.min.js"></script>
<script type="text/javascript" src="JS/modernizr.custom.29473.js"></script>
<script type="text/javascript" src="JS/inline.js"></script>
<link rel="stylesheet" type="text/css" href="css/slider.css" />
<script type="text/javascript" src="JS/modernizr.custom.28468.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;hl=en&amp;key=" type="text/javascript"></script>
<link href='http://fonts.googleapis.com/css?family=Economica:700,400italic' rel='stylesheet' type='text/css'>
<noscript><link rel="stylesheet" type="text/css" href="css/nojs.css" /></noscript>
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-buttons.css" />
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-thumbs.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css"  />
<link rel="stylesheet" type="text/css" href="css/font.min.css">
<link rel="stylesheet" type="text/css" href="css/faq.css">
<link rel="stylesheet" type="text/css" href="font/titillium/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/animation.css">
<script type="text/javascript" src="gallery/Plugins/FancyBox/jquery.fancybox.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-easing.js"></script>
<link rel="stylesheet" type="text/css" href="gallery/CSS/jquery.fb-album.iframe.css" />
<link rel='stylesheet' href='css/style.css' media='screen'  type='text/css'/>
<link rel="stylesheet" href="css/headerstyle.css" type="text/css">
<style type="text/css">@import url(css.css);</style>
<!--[if IE]>
<style type="text/css">
.map {background:transparent;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#333333,endColorstr=#333333);zoom: 1;}
</style>
<![endif]-->
</head>
<body>

<div class="header2"> 
  <div id="hero-container" class=""> 
    <div class="arizona-rv-home-title azrv-edit" style="float:left;">Arizona RV Homes</div>
    <div style="margin:0 0 0 10px; padding: 50px 0;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
    <div style="clear:both;"></div>
    <div class="hwriting call-to-action1 azrv-edit"><p>CALL OR STOP&nbsp;BY TODAY!&nbsp;<span style="color: #ffff00 !important; font-family: sans-serif !important;">928-768-2900</span><span style="color: #ffff00 !important; font-family: sans-serif !important;"><br /></span></p></div>
    <div class="priced-from hwriting">Priced <span style="color: #ff0 !important;font-family: sans-serif !important;font-size: .5em !important;">from</span></div>
    <div class="the-mid hwriting">the mid <span style="color: #ff0 !important;font-family: sans-serif !important; font-size: .4em !important;" class="azrv-edit">$100's</span></div>
    <div class="valley-view">VALLEY VIEW</div>
    <div class="at">@</div>
    <div class="sunrise-hills-title">Sunrise Hills</div>
    <div class="size"><span class="azrv-edit">9,500+ Sq. Ft.</span></div>
    <div class="lots">Lots</div>
    <div class="hero-anim hero-hoverable">
    <div id="hero-rv" class="hero-image" style="float:left;"></div>
    </div>
    <div class="bullet-points azrv-edit"><ul>
<li>2 hours south of Las Vegas</li>
<li>11 Laughlin, NV Casinos</li>
<li>7 nearby Golf Courses</li>
<li>Colorado River</li>
<li>Lake Mohave</li>
<li>Off Roading</li>
<li>Boating</li>
<li>Fishing</li>
</ul></div>
    <div class="contact-button"><a class="contact" href="#contact-form">Contact</a></div>
    <div class="map-button"><a class="googlemap" href="#map-overlay">Map</a></div>
    <div id="contact-form" style="display:none;"> 
      <div style="float:left;"> <img src="images/map-outline.png" alt="Valley View @ Sunrise Hills"> 
        <div class="azrv-edit">
        <ul>
          <li>2530 Nez Perce Rd</li>
          <li>Ft. Mohave AZ 86427</li>
          <li>928-768-2900</li>
        </ul>
        </div>
      </div><form method='post' action='test-form.php' style="float:right; width:420px;"> 
      <div class="azrv-edit">If you are viewing our site after hours and would like more information and are not able to call or stop by soon please send us a brief message and we will be in contact with you shortly..</div>
      <p>&nbsp;</p>
      <p><label for='firstname'>Name:</label> 
        <input name='firstname' type='text'>
      </p>
      <br>
      <p><label for='phone'>Phone:</label> 
        <input name='phone' type='text'>
      </p>
      <br>
      <p><label for='email'>Email:</label> 
        <input name='email' type='text'>
      </p>
      <br>
      <label for='message'>Message:</label><br>
      <textarea name='message' rows='15' cols='40'></textarea>
      <br>
      <input type='submit'>
    </form>
    </div>
    <div id="loading"> Sending your message.... </div>
    <div id="success"> </div>
  </div>
</div>
<div style="clear:both;"></div>
        <div class="container azrv-edit" style="margin:-145px 0 97px 0;">

			<div id="slider" class="slider">
				<div class="slide">
					<h2>Not your typical old design</h2>
					<p>Our homes are smaller with upscale features that everyone looks for in a quality constructed home. For those with RV's the homes provide you with a garage specifically designed to allow you to not only store your RV but also incorporate your RV as part of the living space. The large garages are designed with plenty of room to extend the slide outs and provide you with full hookups as well. Those who have visiting friends with RV's can also incorporate an optional parking slab alongside your garage with full hookups as well.</p>
					<a href="#contact-form" class="contact link">Contact us | (928) 768-2900</a>
					<div class="img"><img src="images/not-typical-design.png" alt="image01" /></div>
				</div>
				<div class="slide">
					<h2>Something unique about Valley View</h2>
					<p>is the size of the lots. With over 9,000 square foot of space and being on sewer, you have the ability to put in your own pool or use the space to expand your living area. There is plenty of room to spread out. We also offer you the ability to design your own custom home to suit your needs. You can say that the possibilities are unlimited.</p>
					<a href="#contact-form" class="contact link">Contact us | (928) 768-2900</a>
					<div class="img"><img src="images/pool-large.png" alt="image01" /></div>
				</div>
				<div class="slide">
					<h2>How Big is your Garage?</h2>
					<p>The Large Garages these homes offer can also be used to store other vehicles that fit your lifestyle like boats and ATV's.
Think about the possibilities.
Easy accessibility to your RV, no more storage needs or monthly costs. Best of all, your batteries are always charged and your ready to go.</p>
					<a href="#contact-form" class="contact link">Contact us | (928) 768-2900</a>
					<div class="img"><img src="images/garage.png" alt="image01" /></div>
				</div>
				<div class="slide">
					<h2>You're Invited!</h2>
					<p>to see the area for yourself and see how much there is to offer. Once you see what our community has and the value you will receive, you will be telling your friends how nice the area is and the deal you made in purchasing a new home in Valley View at Sunrise Hills.</p>
					<a href="#contact-form" class="contact link">Contact us | (928) 768-2900</a>
					<div class="img"><img src="images/boundary-cone.jpg" alt="image01" /></div>
				</div>
				<nav class="arrows">
					<span class="arrows-prev"></span>
					<span class="arrows-next"></span>
				</nav>
			</div>
        </div>
<!--START-->
<div class="about-cont"> 
  <div id="aboutColumnText"> 
    <div class="transparent-steelblue"> 
      <div style="margin: 0 auto;width: 100%;"> 
        <div class="azrv-edit">
        <h2 style="font-size:85px;border-radius:10px;padding:20px 0 20px 0;">What are <span style="color:#fff;">Arizona RV Homes ?</span></h2>
        </div>
        <div style="float:right; margin:-50px 50px 0 0"  class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
        <div class="azrv-edit"></div>
      <p style="width:100%;">
      <div id="ads" style="margin-bottom: -115px;width: 282px;float: left;height: 345px;"> 
        <img src="images/ron-drone-shot.jpg"> 
      </div>
      <div class="azrv-edit">Valley View is a new concept for Custom Built Arizona Houses. Our exclusive Homes Only area has unique houses that are built with large RV Garages on lot sizes from approximately 8500-15,000 square feet.
<p>&nbsp;</p>
Our large RV Garages are big enough for your 45+ footer with all the slide outs extended with full hookups inside the garage, and a full hookup pad outside which allows you to also park and enjoy the views or invite a friend over.
<p>&nbsp;</p>
With each home being designed for its owner, we say our project has unlimited possibilities. With our smallest lot at 9,500 square feet you can build that 4000 square foot monster home, however we like that 1000 square foot home with the monster garage. Either way you like it we can, and will accommodate all serious buyers.
<p>&nbsp;</p>
Valley View and other areas of Sunrise Hills will have their own set of CCR'S to insure conformity to community standards. Access to the community is currently through the Sun Valley subdivision and our newly constructed (during phase 2) Sunrise Hills Parkway, a planned 4 lane divided roadway for community access. Sales prices for the first phase of lots are reasonable when compared to similar areas.
<p>&nbsp;</p>
If you are looking for that special area with &ldquo;something to do&rdquo; and not a boring area, please come and see for yourself what Valley View has to offer.
<p>&nbsp;</p></div>
      </div>
    </div>
  </div>
  <!--end -->
  <p>&nbsp;</p>
</div>

<div style="margin: -300px 0 0 0;">
<div> 
  <div style="width: 940px;margin: 0 auto; color:#262626; text-align:center;">
    <h2 class="azrv-edit">View Our Arizona RV Model Homes </h2>
  </div>
</div>
<div style="width:941px; height:100%; margin: 0 auto;">
<div style="background:url('images/top-round.png') no-repeat; width:941px; height:59px;"></div>
<div style="width:256px;height:328px;float:left;background:url('images/trans-343434.png')">
<h3 class="azrv-edit" style="font-family:inherit;padding:10px;background:#d5b03d;border-radius:10px;width:50%;color:#fff;margin:-10px 0 10px 28px;">$Call For Price</h3>
<div style="width:205px;margin:0 auto;color:#fff;" class="azrv-edit">
<ul>
<li><strong>Size:</strong>&nbsp;766 sq ft.</li>
<li><strong>Lot Size:</strong> 9500 sq ft.</li>
<li><strong>Bedrooms:</strong> 1</li>
<li><strong>Bathrooms:</strong> 1</li>
<li><strong>1 RV Garage</strong></li>
<li><strong>Garage Size:</strong>&nbsp;1144 sq ft.</li>
<li><strong>Landscape:</strong> Yes</li>
<li><strong>Flooring:</strong> Carpet and Tile</li>
<li><strong>Master Bath:</strong> Double Sink</li>
<li><strong>Shower:</strong> walk in</li>
<li><strong>Appliances:</strong> Yes (No Ref)</li>
<li><strong>Washer/Dryer:</strong> Hookups</li>
<li><strong>DateAvailable:&nbsp;</strong>Immediately&nbsp;</li>
<li><strong>HOA:</strong>&nbsp;$100 year</li>
<li><strong>Year Built:</strong> 2013</li>
</ul></div>
<a class="model1" href="#one-bedroom-overlay" style="float:right;padding:7px;background:#d5b03d;margin:-15px 0px 0 0;color:#fff;">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 20px 0 0 298px;"> 1 Bedroom 1 Bathroom 1 RV Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;margin-top: -18px;"><img src="images/1bed-hero.png"></div>
</div>
<div class="cb"></div>
<div style="width:100%; height:100%; margin:0;"> 
  <div style="width:940px; margin:0px auto -10px; padding:0px 10px 25px 10px;"> 
    <!-- Start 1 bedroom content -->
    <div id="one-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - The Large Arizona RV Garage Home Can Also Store Other Vehicles That Fit Your Lifestyle Like Boats & ATV's.</p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedroom</span> - Come Home And Relax In Your New Warm And Cozy Bedroom That Offer Plenty Of Space To Suit Any Of Your Needs. </p>
      <p>&nbsp;</p>
      <p><span tyle="background:#ccc; font-weight:bold;">The Kitchen</span> - Combined With Elegance & Functionality This Kitchen Will Welcome You Home With A Blend Of Upscale Features And Appliances. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathroom</span> - Gleaming With A Granite Counter Top, Custom Sinks And A Framed Mirror, There Is Also A Walk In Shower With Multiple Jets And Over-Sized Shower Head. 
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 1 Bedroom With 1 Bathroom With A Beautiful Walk-In Shower, An RV Garage With RV Hookups Inside And RV Hookups Outside With Extra Parking Room for 2 more. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home! </p>
      <p>&nbsp;</p>
      <p>928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 1 bedroom content -->
  
    <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 2 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 2 bedroom content -->

    <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 3 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 3 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 4 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 4 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 5 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 5 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 6 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 6 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 7 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 7 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 8 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 8 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 9 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 9 bedroom content -->

        <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
 
<div style="width:941px; height:350px; margin: 60px auto 0;">
<div style="width:256px; height:347px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10px;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div style="width:222px;margin:0 auto;color:#fff;" class="azrv-edit"><ul>
<li><strong>Size</strong>: 1339 sq ft.&nbsp;</li>
<li><strong>Lot Size</strong>: 9500 sq ft.&nbsp;</li>
<li><strong>Bedrooms</strong>: 2 Bathrooms: 2&nbsp;</li>
<li><strong>1 RV Garage, 1144 sq.ft&nbsp;</strong></li>
<li><strong>1 Car Garage, 336 sq.ft.&nbsp;</strong></li>
<li><strong>Landscape</strong>: Yes&nbsp;</li>
<li><strong>Flooring</strong>: Carpet and Tile Master Bath:&nbsp;</li>
<li><strong>Double Sink Shower</strong>: walk in&nbsp;</li>
<li><strong>Appliances</strong>: Yes</li>
<li><strong>Washer/Dryer</strong>: Hookups&nbsp;</li>
<li><strong>Date Available</strong>: Immediately&nbsp;</li>
<li><strong>HOA</strong>:&nbsp;$100 year&nbsp;</li>
<li><strong>Year Built</strong>: 2013</li>
</ul></div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 15px 0 0 298px;"> 2 Bedroom 2 Bathroom RV Garage + Boat Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 10 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 10 bedroom content -->

    <div id="two-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="two-bedroom" src="gallery/two-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:15px auto;"><img src="images/bg-bar.jpg" /></div>
      <div style="width: 980px;margin: 50px auto 0;">
      <h2 class="azrv-edit">View Our Floorplans or Design Your Own</h2>
      <p></p>
    </div><center>
    <div id="floorplans" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <img src="images/floorplans/Page-5-Model-777.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-6-Model-855.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-7-Model-929.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-8-Model-929-with-Storage.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-9-Model-1091.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-10-Model-1212.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-11-Model-1218.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-12-Model-1339.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-13-Model-1400.jpg" alt="floorplans" height="320" width="250">
      <img src="images/floorplans/Page-14-Model-1800.jpg" alt="floorplans" height="320" width="250">
    </center>
</div>
<!-- Start FAQS -->

<div class="nav azrv-edit"><h2 style="margin-left: 5px;">General Questions About Arizona RV Homes</h2>
<input id="label-1" checked="checked" name="faq" type="radio" /> <label id="item1" for="label-1"><em>&nbsp;</em>How big is    the RV Garage?</label>
<div id="a1" class="content">
<h4>Our standard size is 52 foot deep and 22 foot wide. We can also custom build it to your specifications and make it larger or smaller.</h4>
</div>Page-5-Model-777
<div id="faqs" style="width: 980px; height: 100%; display: block;">
<div><input id="label-2" name="faq" type="radio" /> <label id="item2" for="label-2"><em></em>Can you park on the street?</label>
<div id="a2" class="content">
<h4>We provide you with a long driveway and the ability to park along the side of your home (behind the setbacks) or for up to 72 hours in front of your home per CCR&rsquo;s</h4>
</div>
</div>
<div><input id="label-3" name="faq" type="radio" /> <label id="item3" for="label-3"><em></em>Will my truck and boat fit?</label>
<div id="a3" class="content">
<h4>It depends on the size of both. We can adjust the driveway and the garage to accommodate both. Flexibility is the rule here.</h4>
</div>
</div>
<div><input id="label-4" name="faq" type="radio" /> <label id="item4" for="label-4"><em></em>What size homes will you build?</label>
<div id="a4" class="content">
<h4>Our homes can range from 500 square foot to 3500 square foot. You can say that we build what YOU want. Every home will be custom unless you buy        an existing model.</h4>
</div>
</div>
<div><input id="label-5" name="faq" type="radio" /> <label id="item5" for="label-5"><em></em>Do I own the land?</label>
<div id="a5" class="content">
<h4>Our land is &ldquo;Fee Simple&rdquo; which means that you own it and it is not leased.</h4>
</div>
</div>
<div><input id="label-6" name="faq" type="radio" /> <label id="item6" for="label-6"><em></em>How big are the lots?</label>
<div id="a6" class="content">
<h4>Our smallest lots are over 9,500 square foot (74.5&rsquo; X 127.6&rsquo;) in Phase 1 (66 lots)</h4>
</div>
</div>
<div><input id="label-7" name="faq" type="radio" /> <label id="item7" for="label-7"><em></em>What are the setbacks?</label>
<div id="a7" class="content">
<h4>We have a 20 foot in front, 5 foot on side (10 feet on a corner side lot) and 25 foot in the rear.</h4>
</div>
</div>
<div><input id="label-8" name="faq" type="radio" /> <label id="item8" for="label-8"><em></em>Is all of my lot useable?</label>
<div id="a8" class="content">
<h4>With front yard utility easements and municipal sewer, your lot is fully useable and you have plenty of room for a pool or larger home and garage.</h4>
</div>
</div>
<div><input id="label-9" name="faq" type="radio" /> <label id="item9" for="label-9"><em></em>How many lots are there?</label>
<div id="a9" class="content">
<h4>In Phase 1 we have 66 lots. We intend to have a total of 375 lots in 6 phases.</h4>
</div>
</div>
<div><input id="label-10" name="faq" type="radio" /> <label id="item10" for="label-10"><em></em>Do you have CCR'S?</label>
<div id="a10" class="content">
<h4>Yes we do and they are specifically tailored for RV, Boat and Off Road Vehicle usage.</h4>
</div>
</div>
<div><input id="label-11" name="faq" type="radio" /> <label id="item11" for="label-11"><em></em>How much are the association dues?</label>
<div id="a11" class="content">
<h4>To start out they are $100 a year. We DO NOT have any common areas, clubhouse or pool to pay for. The money covers the administrative actions and legalities        needed for Arizona law.</h4>
</div>
</div>
<div><input id="label-12" name="faq" type="radio" /> <label id="item12" for="label-12"><em></em>Are you a gated community?</label>
<div id="a12" class="content">
<h4>To keep costs low and not have the expense of road maintenance and other associated costs, we will NOT be gated.</h4>
<div id="a11" class="content"></div>
</div>
</div>
</div></div> 
<div style="width:980px; margin:-20px auto 25px;"><p><a href="javascript: toggle('faqs');">Please click here to view more or less questions</a></p></div>
<!-- End FAQS -->
<div style="width:100%; background-color:#fff;"> 
  <div style="padding:20px 0;background-color:#fff;width:980px;height:198px;margin:0 auto;" class="local-shopping-wrapper"> 
    <div class="logo_strip"> 
      <h2 class="azrv-edit">Ft Mohave is a great area with close shopping to:</h2>
      <a href="http://g.co/maps/f5phy" target="_blank" title="Smiths Google Map Directions From Arizona RV Homes"> 
      <div class="smiths"></div>
      </a> <a href="http://g.co/maps/vwevw" target="_blank" title="Safeway Google Map Directions From Arizona RV Homes"> 
      <div class="safeway"></div>
      </a> <a href="http://g.co/maps/eqr6j" target="_blank" title="Target Google Map Directions From Arizona RV Homes"> 
      <div class="target"></div>
      </a> <a href="http://g.co/maps/wjsua" target="_blank" title="The Home Depot Google Map Directions From AriArizona aona RV Homes"> 
      <div class="home-depot"></div>
      </a> <a href="http://g.co/maps/tmf6x" target="_blank" title="Sams Club Google Map Directions From Arizona RV Homes"> 
      <div class="sams-club"></div>
      </a> <a href="http://g.co/maps/shd2w" target="_blank" title="Walmart Google Map Directions From Arizona RV Homes"> 
      <div class="walmart"></div>
      </a> <a href="http://g.co/maps/9ugqc" target="_blank" title="Lowes Google Map Directions From Arizona RV Homes"> 
      <div class="lowes"></div>
      </a> <a href="http://g.co/maps/zf3f3" target="_blank" title="Kohls Google Map Directions From Arizona RV Homes"> 
      <div class="kohls"></div>
      </a> </div>
    <div class="azrv-edit" style="width:900px;">(click logos above for google map directions from arizona rv homes) </div>
  </div>
</div>
</div>
<div style="width:100%;background: url(&quot;images/footer-1.jpg&quot;) repeat scroll 0 0 #1A1A1B;color: #fff;border-top:1px solid #333; margin:25px 0 0 0; padding:15px 0 0 0;height: 71px;text-align: left;"> 
  <div style="width:980px; margin:0 auto;">
  <h2 class="azrv-edit" style="font-size:56px; padding-top:10px; color:#D5B03D;">Like What You See? Call or Stop By Today!</h2>
    <a class="contact" href="#contact-form"><div class="now-selling"></div></a>
</div>
</div>

<div id="map-overlay" style="display:none;"> 
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&amp;libraries=places"></script> 
      <script type="text/javascript" src="JS/drag-map.js"></script>
      <p>&nbsp;</p>
      <div class="local-map-wrapper">
        <h2 class="azrv-edit">Map Places close to Arizona RV Homes <span class="body-text">drag &amp; zoom map around to see places nearby</span></h2>
        <div id="map_canvas"></div>
        <div id="listing">
          <div id="controls">
            <form name="controls">
              <input type="radio" name="type" value="establishment" onclick="search()" checked="checked"/>
              All<br/>
              <input type="radio" name="type" value="restaurant" onclick="search()" />
              Restaurants<br/>
              <input type="radio" name="type" value="lodging" onclick="search()" />
              Lodging
            </form>
          </div>
          <table id="results">
          </table>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div class="recommended-places">
        <h2 class="azrv-edit">Recommended Places close to Arizona RV Homes<span class="body-text">mileage is apporoximate</span></h2>
       
<div id="info" style="margin-top:-30px;">
<b id="tab1" class="hiddenTarget"></b>
<b id="tab2" class="hiddenTarget"></b>
<b id="tab3" class="hiddenTarget"></b>
<b id="tab4" class="hiddenTarget"></b>
<b id="tab5" class="hiddenTarget"></b>
<b id="tab6" class="hiddenTarget"></b>
<b id="tab7" class="hiddenTarget"></b>
<b id="tab8" class="hiddenTarget"></b>
<b id="tab9" class="hiddenTarget"></b>
<div class="tabbedPages">
	<ul class="tabs">
		<li><a href="#tab1" class="default tab1 tabpages">Shopping and Food</a></li>
		<li><a href="#tab2" class="tab2 tabpages">Nearby Casinos</a></li>
		<li><a href="#tab3" class="tab3 tabpages">Places of Interest</a></li>
		<li><a href="#tab4" class="tab4 tabpages">Los Loagos</a></li>
		<li><a href="#tab5" class="tab5 tabpages">Desert Lakes</a></li>
		<li><a href="#tab6" class="tab6 tabpages">Chaparral</a></li>
		<li><a href="#tab7" class="tab7 tabpages">El Rio</a></li>
		<li><a href="#tab8" class="tab8 tabpages">Willow Springs</a></li>
		<li><a href="#tab9" class="tab9 tabpages">Laughlin Ranch </a></li>
	</ul>
	<div id="view1" class="tabcontent default">
		<ul id="places-list">
          <li>Gas Station/mini market (2 miles)</li>
          <li>Casa Serrano Mexican Food(3 miles)</li>
          <li>Taco Bell, Long John Silver (3 ½ miles)</li>
          <li>Smiths, Safeway, Bank of America, CVS, Mc Donald’s, Burger  King &nbsp;(5 

miles)</li>
          <li>Target, Kohl’s Marshall’s (6 ½ miles)</li>
          <li>Wal-Mart (9 miles)</li>
          <li>Lowe’s (11 miles)</li>
          <li>Sam’s Club, Home Depot, Chili’s, I-HOP, Carl’s Jr. (13miles)</li>
        </ul>
	</div>
	
	<div id="view2" class="tabcontent">
		<ul>
          <li>Avi&nbsp; (6 miles)</li>
          <li>Riverside, Tropicana, Colorado Belle, Pioneer, Golden  Nugget, Aquarius, River 

Palms (16miles)</li>
          <li>Harrah’s (17 ½ miles)</li>
        </ul>
	</div>
	<div id="view3" class="tabcontent">
		<ul>
          <li>Oatman, Arizona, Route 66, (12 miles)</li>
          <li>Needles, California, I40 (14 miles)</li>
          <li>Lake Mohave (18miles)</li>
          <li>Kingman, Arizona (50 miles)</li>
          <li>Lake Havasu City, Arizona (50 miles)</li>
          <li>Las Vegas, Nevada (100 miles)</li>
        </ul>
	</div>
	
	<div id="view4" class="tabcontent">
	 <p><strong><img src="images/los-lagos-golf-club.jpg" alt="Los Lagos Golf Club" style="padding:0 15px 0 0;" align="left" width="194" height="129">Los Lagos Golf Club, Los Lagos Golf Course</strong><br>
            Los Lagos Golf Club is a public 18-hole golf course located in Fort Mohave, Arizona. Opened for play in 2007 and designed by Ted Robinson. </p>
          <p> Sculpted from the desert terrain of the Colorado River Valley,   this stunning course layout includes four sets of tees and beautiful   mountain views from every tee box. Featuring championship public golf, a   complete practice facility, and a spectacular residential community,   Los Lagos has something for everyone. </p>
          <ul style="margin:50px auto 0;">
            <li> Blue tees: par-72, 6,921 yards, 72.0 / 126 </li>
            <li> White tees: par-72, 6,570 yards, 70.9 / 124 </li>
            <li> Gold tees: par-72, 5,863 yards, 68.5 / 115 </li>
            <li> Red tees: par-72, 5,040 yards, 72.0 / 124 <br>
              <br>
            </li>
            <li>18 holes over 6,921 yards with a par of 72 (Public) <br>
              <br>
              6365 S Entrada Via Verdes<br>
              Fort Mohave, AZ&nbsp;86427<br>
              (928) 768-7778</li>
          </ul>
	</div>
	
	<div id="view5" class="tabcontent">
	<p><strong><img src="images/desert-lakes-golf-course-logo.png" alt="Desert Lakes Golf Course" style="padding:0 15px 0 0;" align="left" width="118" height="124">Desert Lakes Golf Course, Desert Lakes</strong><br>
            Desert Lakes Golf Club is a Resort 18 hole golf course located in Fort Mohave, Arizona. Opened for play in 1990 and designed by Bob E. and Robert L. Baldock. </p>
          <p> Par for the course is 72. From the back tees the course plays to   6,569 yards. From the forward tees the course measures 5,440 yards. The   longest hole on the course is # 3, a par-5 that plays to 525 yards. The   shortest hole on the course is # 15, a par-3 that plays to 163 yards   from the back tees. </p>
          <p> Watch out for # 1, a 421 yard par-4 challenge and the #1   handicap hole on the course. The easiest hole at Desert Lakes Golf Club   is # 15, the 163 yard par-3. </p>
          <ul>
            <li>18 holes over 6,569 yards with a par of 72(Public) <br>
              <br>
              5835 S Desert Lakes Dr<br>
              Fort Mohave, AZ&nbsp;86426-9106<br>
              (928) 768-1000</li>
          </ul>
	</div>
	
	<div id="view6" class="tabcontent">
	<p><strong><img src="images/chaparral-country-club-logo.png" alt="Chaparral Golf &amp; Country Club" style="padding:0 15px 0 0;" align="left" width="200" height="71">Chaparral Golf &amp; Country Club, Chaparral Golf Course</strong><br>
            Chaparral Country Club is a Semi-Private , 9 hole golf course located in Bullhead City , Arizona and first opened for play in 1970. </p>
          <ul>
            <li>9 holes over 2,344 yards	with a par of 32 (Public) <br>
              <br>
              1260 Mohave Dr<br>
              Bullhead City, AZ&nbsp;86442-7643<br>
              (928) 758-6330</li>
          </ul>
	</div>
	
	<div id="view7" class="tabcontent">
	<p><strong><img src="images/el-rio-golf-country-club-logo.jpg" alt="El Rio Golf and Country Club" style="padding:0 15px 0 0;" align="left" width="194" height="149">El Rio Golf and Country Club, El Rio Golf Course</strong><br>
            El Rio Golf &amp; Country Club, is a Public, 18 hole golf course located in Mohave Valley, Arizona. </p>
          <p> The 16,000 square foot Spanish, mission style   clubhouse and  golf course are open to the public. </p>
          <p> El Rio Country Club golf course opened for play in 2005. The course was designed by Matthew E. Dye. </p>
          <p> The El Rio Country Club golf course is an upscale, daily fee   18-hole championship golf course.  The El Rio golf course has scenic   views of the surrounding mountains, gentle rolling and forgiving   fairways, undulating greens, and four holes with water features.    Par for the El Rio Country Club 7,100 yard golf course is 72. the course   offers four sets of tees ranging from 5,300 to 7,100 yards in length to   accommodate all playing levels. </p>
          <ul style="margin:20px auto 0;">
            <li> Black tees: par-72, 7,115 yards, 72.9/123. </li>
            <li> Copper tees: par-72, 6,019 yards, 67.7/113. </li>
            <li> Gold tees: par-72, 6,609 yards, 70.6/117. </li>
            <li> Teal tees: par-72, 5,342 yards. <br>
              <br>
            </li>
            <li>18 holes over 7,115 yards with a par of 72 (Public) <br>
              <br>
              1 Paseo El Rio<br>
              Mohave Valley, AZ&nbsp;86440-9140<br>
              (866) 389-2445</li>
          </ul> 	
	</div>
	
	<div id="view8" class="tabcontent">
	<p><strong>Willow Springs Golf Course, Willow Springs</strong></p>
          <blockquote>
            <p>9 holes (Public) <br>
              8011 Highway 95<br>
              Mohave Valley, AZ&nbsp;86440<br>
              (928) 768-4414 </p>
          </blockquote>
	</div>
	
	<div id="view9" class="tabcontent">
	<p><strong>Laughlin Ranch Golf Club, Laughlin Golf Course</strong></p>
          <blockquote>
            <p>18 holes over 7,192 yards with a par of 72 (Public) <br>
              1360 William Hardy Dr<br>
              Bullhead City, AZ&nbsp;86429-1146<br>
              (866) 684-4653</p>
          </blockquote>
	</div>
	</div>
	</div>
</div>        
<div style="clear:both;margin:20px;"></div>
<div class="azrv-edit" style="line-height: 18px;margin-top: 20px;width: 99%;background-color: #ddd;float: left;"><strong>There are many other places not listed on this page &amp; we 
recommend you travel the area. All distances are approximate and your route will dictate the actual distance. This is just a guide so you can see what this area offers.</strong></div>
</div>      	
<div id="footer"> 
  <div class="centerContainer"> 
    <div style="width:490px; margin:0 auto;"> 
      <div style="float:left; width:135x;"><img src="images/equal-opp.png" alt="Equal Oppotunity Housing Logo" width="115" height="116"></div>
      <div style="margin:35px 0 0; float:right; width:370px;"> 
       <h1 class="azrv-edit" style="font-size: 66px !important; font-color:#fff; text-align:center;font-family: 'handwriting', Arial, sans-serif !important; text-transform:capitalize; padding-bottom:7px;">Arizona 
          RV Homes </h1>
        <h1 class="azrv-edit">928-768-2900</h1>
      </div>
      <div style="clear:both;"></div>
    </div>
    <p class="azrv-edit" style="font-size:18px; text-align:center;">&#169; 2013 Arizona RV Homes. All rights reserved. 2530 Nez Perce Rd | Ft Mohave, AZ 86427 </p>
    <div style="margin:20px 0; width:910px; padding-bottom:1px;"> 
      <p> 
      <div style="float:left;"> <a style="color:#d0d0d0 !important; font-family:'titilliumregular', Palatino, Georgia, serif;" target="_blank" href="http://www.facebook.com/arizonarvhomes">Like Our Facebok FanPage</a> 
        <div style="margin: -16px 0 0 232px; display: block;" class="fb-like" data-href="http://www.facebook.com/arizonarvhomes" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left; font-family:'titilliumregular', Palatino, Georgia, serif;"> Like Our Website 
        <div style="margin: -16px 0 0 184px; display: 
              block;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" 
              data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left;; "> <a style="color:#d0d0d0 !important; font-family:'titilliumregular', Palatino, Georgia, serif;" target="_blank" href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US">Like Our Google+ Places Page</a> 
        <div style="margin: -16px 0 0 250px; display: block;" class="fb-like" data-href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
    </div>
  </div>
</div>
<div id="preload"> <img src="images/yellow-ribbon-small-over.png" /> <img src="images/smiths.jpg" /> <img src="images/safeway.jpg" /> 
  <img src="images/target.jpg" /> <img src="images/home-depot.jpg" /> <img src="images/sams-club.jpg" /> 
  <img src="images/walmart.jpg" /> <img src="images/lowes.jpg" /> <img src="images/kohls.jpg" /> <img src="images/now-selling.png" />
</div>
<script type="text/javascript" src="JS/jquery.cslider.js"></script>
<script type="text/javascript">
	$(function() {
	$('#slider').cslider({
		autoplay	: true,
		bgincrement	: 450,
		interval   	: 8000  
	});
});
</script>	
<script type="text/javascript">
function toggle(elementID){
var target = document.getElementById(elementID)
if (target.style.display == 'none') {
target.style.display = 'block'
} else {
target.style.display = 'none'
}
} 

$(document).ready(function() {
	$(".contact").fancybox({
		maxWidth	: '70%',
		maxHeight	: '90%',
		fitToView	: true,
		width		: '70%',
		height		: '90%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".googlemap").fancybox({
		maxWidth	: '90%',
		maxHeight	: '100%',
		fitToView	: true,
		width		: '90%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model1").fancybox({
		maxWidth	: '60%',
		maxHeight	: '85%',
		fitToView	: true,
		width		: '85%',
		height		: '60%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model2").fancybox({
		maxWidth	: '60%',
		maxHeight	: '95%',
		fitToView	: true,
		width		: '85%',
		height		: '85%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(function(){
      $('form').submit(function(e){
        var thisForm = $(this);
        e.preventDefault();
        $(this).fadeOut(function(){
          $("#loading").fadeIn(function(){
            $.ajax({
              type: 'POST',
              url: thisForm.attr("action"),
              data: thisForm.serialize(),
              success: function(data){
                $("#loading").fadeOut(function(){
                  $("#success").text(data).fadeIn();
                });
              }
            });
          });
        });
      })
    });
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=719517228073996";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>