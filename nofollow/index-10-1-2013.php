<!doctype html>
<html>
<head>
<title>Arizona RV Homes is Valley View @ Sunrise Hills - A Master Planned Community in Fort Mohave Arizona</title>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

<meta name="description" content="Arizona RV Homes are Unique Homes with RV Garages that are located inside of Valley View @ Sunrise Hills. We are part of a master planned community of 500 acres that stretches from Joy Lane to Boundary Cone Rd. and starts at 2530 Nez Perce Rd. Ft. Mohave, AZ 86427 ">
<meta property="og:title" content="Arizona RV Homes">
<meta name="author" content="DonFeightner@Gmail.com">
<meta property="og:description" content="Homes with Large RV Garages on 500 acres that stretches from Joy Lane to Boundary Cone Rd. in Ft. Mohave AZ. Stop by or call (928) 768-2900 to discuss our lots/land and homes available for purchase.">
<meta property="og:url" content="http://arizonarvhomes.com">
<meta property="og:image" content="http://arizonarvhomes.com/images/azrvhomes-thumbnail.jpg">
<link rel='stylesheet' href='css/style.css' media='screen'  type='text/css'/>
<link rel="stylesheet" href="css/headerstyle.css" type="text/css">
<style type="text/css">@import url(css.css);</style>
<script language="JavaScript" src="JS/validate.js" type="text/javascript"></script>
<script src="http://cdn.jquerytools.org/1.2.6/full/jquery.tools.min.js"></script>
<!--[if IE]>
<style type="text/css">
.map {background:transparent;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#333333,endColorstr=#333333);zoom: 1;}
</style>
<![endif]-->
</head>
<body>


<div class="header"> 
  <div id="hero-container" class="hero-anim hero-hoverable"> 
    <div class="arizona-rv-home-title" style="float:left;">Arizona RV Homes</div>
    <div style="top: 120px; padding: 50px 0;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
    <div style="clear:both;"></div>
    <div style="color: rgb(255, 255, 255); height: 30px; font-weight: bold; font-size: 23px; text-align: center; text-shadow: 2px 2px 0px rgb(46, 116, 186), -1px -1px 0px rgb(46, 116, 186), 1px -1px 0px rgb(46, 116, 186), -1px 1px 0px rgb(46, 116, 186), 1px 1px 0px rgb(46, 116, 186); width: 550px; position: relative; margin: 38px 0px -71px 191px;" class="hwriting">CALL OR STOP BY TODAY! <span style="color: #ff0 !important;font-family: sans-serif !important;">928-768-2900</span></div>
    <div class="hwriting" style="color:#fff; height:30px; width: 500px; text-align:center; margin: 6px 0 -70px 592px; font-size: 23px;">Priced 
      <span style="color: #ff0 !important;font-family: sans-serif !important;font-size: .5em !important;">from</span></div>
    <div class="hwriting" style="color:#fff; height:30px; width: 500px; text-align:center; margin:61px 0 -70px 592px; font-size: 1em;">the 
      mid <span style="color: #ff0 !important;font-family: sans-serif !important; font-size: .4em !important;">$100's</span></div>
    <div style="color:#fff; height:30px; width: 500px; text-align:center; margin: 81px 0 -94px 588px; font-size: 1.7em; family: sans-serif !important;">VALLEY 
      VIEW</div>
    <div style="color:#fff; height:30px; width: 500px; text-align:center; margin: 87px 0 -124px 586px; font-size: 2.2em; family: sans-serif !important;">@</div>
    <div class="sunrise-hills-title" style="color:#fff; height:30px; width: 500px; text-align:center; margin: 129px 0 -160px 586px; font-size: 3.4em; family: sans-serif !important;">Sunrise 
      Hills</div>
    <div style="color:#fff; height:30px; width: 500px; text-align:center; margin: 198px 0 -228px 599px; font-size: .7em; family: sans-serif !important;">9,500+ 
      Sq. Ft.</div>
    <div style="color:#fff; height:30px; width: 500px; text-align:center; margin: 209px 0 -233px 582px; font-size: 1em; family: sans-serif !important;">Lots</div>

     <div id="hero-rv" class="hero-image" style="float:left;"></div>
    <div style="font-family: Open Sans,Tahoma,Arial,sans-serif; font-size: 16px; color: #062746; float: right; height: 200px; font-weight: bold; letter-spacing: 0px; margin-top: 180px; margin-right:-222px; width: 246px; line-height: 23px; text-shadow:1px 1px 0 #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;"> 
      <ul style="list-style-type:none !important;">
        <li>* 2 hours south of Las Vegas</li>
        <li>* 9 Laughlin, NV Casinos</li>
        <li>* 7 nearby Golf Courses</li>
        <li>* Colorado River</li>
        <li>* Lake Mohave</li>
        <li>* Off Roading</li>
        <li>* Boating</li>
        <li>* Fishing</li>
      </ul>
    </div>
    <div style="clear:both;"></div>
  </div>
</div>
<!--START-->
<div style="width:100%;background: url(&quot;images/footer-1.jpg&quot;) repeat scroll 0 0 #1A1A1B;color: #fff; margin:-61px 0 0 0; padding:15px 0 0 0"> 
  <div id="aboutColumnText" style="margin:25px auto; width:1152px; font-family:'Open Sans', sans-serif"> 
    <h2 style="font-size: 85px; text-transform: capitalize;padding-bottom: 20px;">What are <span style="color:#fff;">Arizona RV Homes ?</span></h2>
     <div id="open-contact" class="open-contact" style="margin: -210px 368px 0 0;padding-left: 20px;">Contact Us Now</div>
      <div id="open_now" class="open-map">Open Map</div>
    <div style="float:right; margin:-48px 370px 0 0"  class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
    <h4 style="padding:10px;">Arizona RV Homes is Valley View @ Sunrise Hills - A Master Planned Community in Fort Mohave Arizona that stretches 500 acres from Joy Lane to Boundary Cone Rd. and starts at 2530 Nez Perce Rd. Ft. Mohave, AZ 86427 </h4>
     <div class="transparent-steelblue">
    <p style="width:100%;"> We have Unique Homes with large Arizona RV Garages and are currently under construction 
      and have the first phase complete in Valley View. In the first area developed, 
      we will have 130 acres in the Northwest corner of Section 6 that will be 
      subdivided into 375 homes only lots, adjacent to the Sun Valley subdivision. 
      In "Valley View" the lot sizes will be approximately 8500-15,000 square 
      feet. This phase will be an exclusive Homes-Only area and lots will be available 
      to qualified builders and select buyers. <br />
      <br />
      This area as all of Sunrise Hills will have it's own set of CCR'S to insure 
      conformity to community standards. All utilities will be provided to each 
      lot, such as electric, natural gas, water, telephone and sewer. Access will 
      be through Sun Valley subdivision and our newly constructed (during phase 
      2) Sunrise Hills Parkway, a planned 4 lane divided roadway for community 
      access. Sales prices for the first phase of lots are reasonable when compared 
      to similar areas.<br />
      <br />
      The homes are smaller, but the garage is big enough for that 45 footer with 
      all the slide outs extended. And we also include our RV, full hook up outside 
      pad area. Invite a friend or park your own outside and enjoy the views. 
      With each home being designed for its owner, we say our project has unlimited 
      possibilities. Our smallest lot is 9,500 square feet. Yes, you could build 
      that 4000' monster home. Personally I like that 1000 foot home with the 
      monster garage. Either way you like it we can, and will accommodate all 
      serious buyers. <br />
      <br />
      If you are looking for that special area with “something to do” and not 
      a boring area, please come and investigate for yourself what Valley View 
      has to offer. </p>
      </div>
  </div>
  <!--end -->
  <p>&nbsp;</p>
</div>
<div class="section0"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The Models</div>
      <div class="section-text">Call or stop by today and view our model homes 
        and you will see why Arizona RV Homes is a place you can call home!</div>
    </div>
    <div class="section-picture-block"><img src="images/model1.jpg" alt="Model" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/model2.jpg" alt="Model" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/model3.jpg" alt="Model" width="226" height="150"></div>
  </div>
</div>
<div class="section4"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The RV Garages</div>
      <div class="section-text">These Large Garage Homes can also store other 
        vehicles that fit your lifestyle like boats & ATV's.</div>
    </div>
    <div class="section-picture-block"><img src="images/rv-garage-1.jpg" alt="RV Garage" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/rv-garage-2.jpg" alt="RV Garage" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/rv-garage-3.jpg" alt="RV Garage" width="226" height="150"></div>
  </div>
</div>
<div class="section1"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The Bedrooms</div>
      <div class="section-text">Come home and Relax in your new warm and cozy 
        bedrooms that offer plenty of space to suit any of your needs.</div>
    </div>
    <div class="section-picture-block"><img src="images/bedroom1.jpg" alt="Bedroom" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/bedroom2.jpg" alt="Bedroom" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/bedroom3.jpg" alt="Bedroom" width="226" height="150"></div>
  </div>
</div>
<div class="section2"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The Kitchens</div>
      <div class="section-text">Combined with elegance & functionality our kitchens 
        welcome you home with a blend of upscale features and appliances.</div>
    </div>
    <div class="section-picture-block"><img src="images/kitchen1.jpg" alt="Kitchen" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/kitchen2.jpg" alt="Kitchen" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/kitchen3.jpg" alt="Kitchen" width="226" height="150"></div>
  </div>
</div>
<div class="section3"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The Bathrooms</div>
      <div class="section-text">Gleaming with granite counter tops, custom sinks 
        and framed mirrors, these walk in showers have multiple jets & over-sized 
        shower heads.</div>
    </div>
    <div class="section-picture-block"><img src="images/bathroom1.jpg" alt="Bathroom" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/bathroom2.jpg" alt="Bathroom" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/bathroom3.jpg" alt="Bathroom" width="226" height="150"></div>
  </div>
</div>
<div class="section5"> 
  <div class="section-container"> 
    <div class="section-block"> 
      <div class="section-title">The Floorplans</div>
      <div class="section-text">View our pre-designed floorplans for Arizona RV 
        Homes that you can modify to suit your needs or you can design your own!</div>
    </div>
    <div class="section-picture-block"><img src="images/floorplan1.png" alt="Floorplan" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/floorplan2.png" alt="Floorplan" width="226" height="150"></div>
    <div class="section-picture-block"><img src="images/floorplan3.png" alt="Floorplan" width="226" height="150"></div>
  </div>
</div>
<div style="width:100%;background: url(&quot;images/footer-1.jpg&quot;) repeat scroll 0 0 #1A1A1B;color: #fff; margin:-52px 0 0 0; padding:15px 0 0 0;height: 71px;text-align: center;/* text-transform: capitalize !important; */">
  <h2 style="text-transform: capitalize;font-size: 56px; padding-top: 10px;">Like What You See? <span style="color:#fff;">Call or Stop By Today!</span></h2>
</div>
<div id="footer"> 
  <div class="centerContainer"> 
    <div style="width:490px; margin:0 auto;"> 
      <div style="float:left; width:135x;"><img src="images/equal-opp.png" alt="Equal Oppotunity Housing Logo" width="115" height="116"></div>
      <div style="margin:35px 0 0; float:right; width:370px;"> 
        <h1 style="font-size: 66px !important; font-color:#fff; text-align:center;font-family: 'handwriting', Arial, sans-serif !important; text-transform:capitalize; padding-bottom:7px;">Arizona 
          RV Homes </h1>
        <h1>928-768-2900</h1>
      </div>
      <div style="clear:both;"></div>
    </div>
    <p style="font-size:18px;">&#169; 2013 Arizona RV Homes. All rights reserved. 
      2530 Nez Perce Rd | Ft Mohave, AZ 86427 </p>
    <div style="margin: 24px 0;height: 28px; display:none;">
      <?php include("counter.php"); ?>
      unique visitors since 9/27/13</div>
    <div style="margin:20px 0; width:910px; padding-bottom:1px;"> 
      <p> 
      <div style="float:left;"> <a style="color:#d0d0d0 !important;" target="_blank" href="http://www.facebook.com/arizonarvhomes">Like 
        Our Facebok FanPage</a> 
        <div style="margin: -16px 0 0 232px; display: block;" class="fb-like" data-href="http://www.facebook.com/arizonarvhomes" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left; "> Like Our Website 
        <div style="margin: -16px 0 0 184px; display: 
              block;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" 
              data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left;; "> <a style="color:#d0d0d0 !important;" target="_blank" href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US">Like 
        Our Google+ Places Page</a> 
        <div style="margin: -16px 0 0 250px; display: block;" class="fb-like" data-href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
    </div>
  </div>
</div>

<!-- Start Overlay Content -->

<!-- Start Map Overlay -->
	      <div id="map">
		<div style="height:100%;">
		 <iframe src="http://mapsengine.google.com/map/embed?mid=zezFRos3Ea2Y.kSlmKlQJAwz8" width="100%" height="100%"></iframe>
		    <div style="margin: -580px 0 0 0; background:transparent; position:absolute;float: right;right: 482px;cursor: pointer;">
		    <p>
		     <img src="images/close-map-banner.png" width="315" height="181" class="close" alt="Enter Arizona RV Homes" title="Enter Arizona RV Homes"> 
		    </p>
		   </div>
		  </div>
		</div>
<script>
$(document).ready(function() {
  $("#open_now").click(function() {
      $("#map").overlay().load();
  });
  $("#map").overlay({
    top: 0,
    mask: {
    color: '#fff',
    loadSpeed: 200,
    opacity: 0.5
    },
    closeOnClick: false,
    load: false
    });
    });
</script>
<!-- End Map Overlay -->
		
<!-- Start Contact Overlay -->
<div id="facebox">
 <iframe src="http://arizonarvhomes.com/contact-form.html" frameborder="0" height="350" width="260"></iframe>
    <p>
      <img src="images/close-button.png" class="close" style="margin:-368px 0 0 215px; position:absolute;" />
    </p>
  </div>

<script>
$(document).ready(function() {
  $("#open-contact").click(function() {
      $("#facebox").overlay().load();
  });
  $("#facebox").overlay({
    top: 260,
    mask: {
    color: '#262626',
    loadSpeed: 200,
    opacity: 0.9
    },
    closeOnClick: false,
    load: false
    });
    });
</script>
<!-- End Contact Overlay -->
<!-- End Overlay Content -->

<div id="preload"> <img src="images/smiths.jpg" height="100" width="100" alt="Smiths" /> 
  <img src="images/safeway.jpg" height="100" width="100" alt="Safeway" /> <img src="images/target.jpg" height="100" width="100" alt="target" /> 
  <img src="images/home-depot.jpg" height="100" width="100" alt="Home Depot" /> 
  <img src="images/sams-club.jpg" height="100" width="100" alt="Sams Club" /> 
  <img src="images/walmart.jpg" height="100" width="100" alt="Walmart" /> <img src="images/lowes.jpg" height="100" width="100" alt="Lowes" /> 
  <img src="images/kohls.jpg" height="100" width="100" alt="Kohls" /> </div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=719517228073996";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 	 </script>
</body>
</html>