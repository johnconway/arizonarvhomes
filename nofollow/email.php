<?php
if(isset($_POST['email'])) {
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "donfeightner@gmail.com";
    $email_subject = "Please notify us when Sunrise Hills goes live.";
     
     
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
     
    // validation expected data exists
    if(!isset($_POST['email'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');      
    }
     
    $email_from = $_POST['email']; // required
     
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "Form details below.\n\n";
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
     
    $email_message .= "Email: ".clean_string($email_from)."\n";
    
     
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	
    <title>Sunrise Servers</title>
    <link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <meta http-equiv="refresh" content="10;url=http://174.121.134.156/~sunrise/">	
</head>

<body>

    <div class="container">
        
        <div class="prepend-6 span-18">
            <h1 class="span-12" id="logo"><a title="Sunrise Hills" href="#">Sunrise Hills</a></h1>
        </div>

        <div id="post" class="prepend-6 span-12">
            <p><div style="margin:0 auto; width:550px; font-family:arial; font-size:16px; color:#fff; padding-top:20px; text-align:center;">Thank you for contacting us. We will notify you via email once our site goes live. </div>
<div style="margin:0 auto; width:600px; font-family:arial; font-size:14px; color:#fff; padding-top:20px; text-align:center;">Automatically sending you back one page in 10 seconds, if not click here: <a href="http://174.121.134.156/`sunrise/">Sunrise Hills</a></div> 
</p>
        </div>

        <hr class="space" />

        </div>

        <div id="footer" class="prepend-6 span-12"></div>
    </div><!-- end container -->
    <?php echo $cc_fscripts;?>
</body>
</html>

<?php
}
?>