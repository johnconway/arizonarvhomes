<!doctype html>
<html>
<head>
<title>Arizona RV Homes is Valley View @ Sunrise Hills - Custom Built Arizona Houses with Large RV Garges</title>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description" content="Arizona RV Homes are Unique Homes with RV Garages that are located inside of Valley View @ Sunrise Hills. We are part of a master planned community of 500 acres that stretches from Joy Lane to Boundary Cone Rd. and starts at 2530 Nez Perce Rd. Ft. Mohave, AZ 86427 ">
<meta property="og:title" content="Arizona RV Homes">
<meta name="author" content="DonFeightner@Gmail.com">
<meta property="og:description" content="Homes with Large RV Garages on 500 acres that stretches from Joy Lane to Boundary Cone Rd. in Ft. Mohave AZ. Stop by or call (928) 768-2900 to discuss our lots/land and homes available for purchase.">
<meta property="og:url" content="http://arizonarvhomes.com">
<meta property="og:image" content="http://arizonarvhomes.com/images/azrvhomes-thumbnail.jpg">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="gallery/JS/jquery.migrate.1.1.1.min.js"></script>
<script type="text/javascript" src="JS/modernizr.custom.29473.js"></script>
<script type="text/javascript" src="JS/inline.js"></script>
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-buttons.css" />
<link rel="stylesheet" type="text/css" href="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-thumbs.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css"  />
<link rel="stylesheet" type="text/css" href="css/font.min.css">
<link rel="stylesheet" type="text/css" href="css/faq.css">
<link rel="stylesheet" type="text/css" href="font/titillium/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/animation.css">
<script type="text/javascript" src="gallery/Plugins/FancyBox/jquery.fancybox.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="gallery/Plugins/FancyBox/Helpers/jquery.fancybox-easing.js"></script>
<link rel="stylesheet" type="text/css" href="gallery/CSS/jquery.fb-album.iframe.css" />
<link rel='stylesheet' href='css/style.css' media='screen'  type='text/css'/>
<link rel="stylesheet" href="css/headerstyle.css" type="text/css">
<link rel="stylesheet" href="css/clouds.css" type="text/css">
<style type="text/css">@import url(css.css);</style>
<!--[if IE]>
<style type="text/css">
.map {background:transparent;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#333333,endColorstr=#333333);zoom: 1;}
</style>
<![endif]-->
</head>
<body>
<div id="clouds">
	<div class="cloud x1"></div>
	<div class="cloud x2"></div>
	<div class="cloud x3"></div>
	<div class="cloud x4"></div>
	<a class="contact" href="#contact-form" style="width:1px;height:1px;position:absolute;"><div class="cloud x5"></div></a>
</div>
<div class="header2"> 

  <div id="hero-container" class=""> 
    <div class="arizona-rv-home-title azrv-edit" style="float:left;">Arizona RV Homes</div>
    <div style="margin:0 0 0 10px; padding: 50px 0;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
    <div style="clear:both;"></div>
    <div class="hwriting call-to-action1 azrv-edit">CALL OR STOP BY TODAY! <span style="color: #ff0 !important; font-family: sans-serif !important;">928-768-2900</span></div>
    <div class="priced-from hwriting">Priced <span style="color: #ff0 !important;font-family: sans-serif !important;font-size: .5em !important;">from</span></div>
    <div class="the-mid hwriting">the mid <span style="color: #ff0 !important;font-family: sans-serif !important; font-size: .4em !important;" class="azrv-edit">$100's</span></div>
    <div class="valley-view">VALLEY VIEW</div>
    <div class="at">@</div>
    <div class="sunrise-hills-title">Sunrise Hills</div>
    <div class="size"><span class="azrv-edit">9,500+ Sq. Ft.</span></div>
    <div class="lots">Lots</div>
    <div class="hero-anim hero-hoverable">
    <div id="hero-rv" class="hero-image" style="float:left;"></div>
    </div>
    <div class="bullet-points azrv-edit"><ul style="list-style-type: none !important;">
<li>* 2 hours south of Las Vegas</li>
<li>* 9 Laughlin, NV Casinos</li>
<li>* 7 nearby Golf Courses</li>
<li>* Colorado River</li>
<li>* Lake Mohave</li>
<li>* Off Roading</li>
<li>* Boating</li>
<li>* Fishing</li>
</ul></div>
    <div class="contact-button"><a class="contact" href="#contact-form">Contact</a></div>
    <div class="map-button"><a href="http://mapsengine.google.com/map/embed?mid=zezFRos3Ea2Y.kSlmKlQJAwz8" target="_blank">Map</a></div>
    <div id="contact-form" style="display:none;"> 
      <div style="float:left;"> <img src="images/map-outline.png" alt="Valley View @ Sunrise Hills"> 
        <div class="azrv-edit">
        <ul>
          <li>2530 Nez Perce Rd</li>
          <li>Ft. Mohave AZ 86427</li>
          <li>928-768-2900</li>
        </ul>
        </div>
      </div><form method='post' action='test-form.php' style="float:right; width:420px;"> 
      <div class="azrv-edit">If you are viewing our site after hours and would like more information and are not able to call or stop by soon please send us a brief message and we will be in contact with you shortly..</div>
      <p>&nbsp;</p>
      <p><label for='firstname'>Name:</label> 
        <input name='firstname' type='text'>
      </p>
      <br>
      <p><label for='phone'>Phone:</label> 
        <input name='phone' type='text'>
      </p>
      <br>
      <p><label for='email'>Email:</label> 
        <input name='email' type='text'>
      </p>
      <br>
      <label for='message'>Message:</label><br>
      <textarea name='message' rows='15' cols='40'></textarea>
      <br>
      <input type='submit'>
    </form>
    </div>
    <div id="loading"> Sending your message.... </div>
    <div id="success"> </div>
  </div>
</div>
<div style="clear:both;"></div>
<!--START-->
<div class="about-cont"> 
  <div id="aboutColumnText"> 
    <div class="transparent-steelblue"> 
      <div style="margin: 0 auto;width: 100%;"> 
        <div class="azrv-edit">
        <h2 style="font-size:85px;border-radius:10px;padding:20px 0 20px 0;">What are <span style="color:#fff;">Arizona RV Homes ?</span></h2>
        </div>
        <div style="float:right; margin:-50px 50px 0 0"  class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
        <div class="azrv-edit"></div>
      <p style="width:100%;">
      <div id="ads" style="margin-bottom: -115px;width: 282px;float: left;height: 345px;margin-left: -38px;"> 
        <iframe id="ads" src="gallery/advertising.html" style="margin: 0px auto; padding: 0px; height: 300px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
      </div>
      <div class="azrv-edit">Valley View is a new concept for Custom Built Arizona Houses. Our exclusive Homes Only area has unique houses that are built with large RV Garages on lot sizes from approximately 8500-15,000 square feet.
<p>&nbsp;</p>
Our large RV Garages are big enough for your 45+ footer with all the slide outs extended with full hookups inside the garage, and a full hookup pad outside which allows you to also park and enjoy the views or invite a friend over.
<p>&nbsp;</p>
With each home being designed for its owner, we say our project has unlimited possibilities. With our smallest lot at 9,500 square feet you can build that 4000 square foot monster home, however we like that 1000 square foot home with the monster garage. Either way you like it we can, and will accommodate all serious buyers.
<p>&nbsp;</p>
Valley View and other areas of Sunrise Hills will have their own set of CCR'S to insure conformity to community standards. Access to the community is currently through the Sun Valley subdivision and our newly constructed (during phase 2) Sunrise Hills Parkway, a planned 4 lane divided roadway for community access. Sales prices for the first phase of lots are reasonable when compared to similar areas.
<p>&nbsp;</p>
If you are looking for that special area with &ldquo;something to do&rdquo; and not a boring area, please come and see for yourself what Valley View has to offer.
<p>&nbsp;</p></div>
      </div>
    </div>
  </div>
  <!--end -->
  <p>&nbsp;</p>
</div>

<div style="margin: -300px 0 0 0;">
<div> 
  <div style="width: 940px;margin: 0 auto; color:#262626; text-align:center;">
    <h2 class="azrv-edit">View Our Arizona RV Model Homes For Sale</h2>
  </div>
</div>
<div style="width:941px; height:100%; margin: 0 auto;">
<div style="background:url('images/top-round.png') no-repeat; width:941px; height:59px;"></div>
<div style="width:256px;height:328px;float:left;background:url('images/trans-343434.png')">
<h3 class="azrv-edit" style="font-family:inherit;padding:10px;background:#d5b03d;border-radius:10px;width:50%;color:#fff;margin:-10px 0 10px 28px;">$Call For Price</h3>
<div class="azrv-edit">
<ul style="width:205px;margin:0 auto;color:#fff;">
  <li><strong>Size:</strong> 1200 sq ft.</li>
  <li><strong>Lot Size:</strong> 9500 sq ft.</li>
  <li><strong>Bedrooms:</strong> 1</li>
  <li><strong>Bathrooms:</strong> 1</li>
  <li><strong>1 RV Garage</strong></li>
  <li><strong>Garage Size:</strong> 500 sq ft.</li>
  <li><strong>Landscape:</strong> Yes</li>
  <li><strong>Flooring:</strong> Carpet and Tile</li>
  <li><strong>Master Bath:</strong> Double Sink</li>
  <li><strong>Shower:</strong> walk in</li>
  <li><strong>Appliances:</strong> Yes</li>
  <li><strong>Washer/Dryer:</strong> Hookups</li>
  <li><strong>Date Available:</strong> Immediately</li>
  <li><strong>HOA:</strong> Yes</li>
  <li><strong>Year Built:</strong> 2013</li>
</ul>
</div>
<a class="model1" href="#one-bedroom-overlay" style="float:right;padding:7px;background:#d5b03d;margin:-15px 0px 0 0;color:#fff;">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 20px 0 0 298px;">FOR SALE - 1 Bedroom 1 Bathroom 1 RV Garage</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;margin-top: -18px;"><img src="images/1bed-hero.png"></div>
</div>
<div class="cb"></div>
<div style="width:100%; height:100%; margin:0;"> 
  <div style="width:940px; margin:0px auto -10px; padding:0px 10px 25px 10px;"> 
    <!-- Start 1 bedroom content -->
    <div id="one-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - The Large Arizona RV Garage Home Can Also Store Other Vehicles That Fit Your Lifestyle Like Boats & ATV's.</p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedroom</span> - Come Home And Relax In Your New Warm And Cozy Bedroom That Offer Plenty Of Space To Suit Any Of Your Needs. </p>
      <p>&nbsp;</p>
      <p><span tyle="background:#ccc; font-weight:bold;">The Kitchen</span> - Combined With Elegance & Functionality This Kitchen Will Welcome You Home With A Blend Of Upscale Features And Appliances. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathroom</span> - Gleaming With A Granite Counter Top, Custom Sinks And A Framed Mirror, There Is Also A Walk In Shower With Multiple Jets And Over-Sized Shower Head. 
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 1 Bedroom With 1 Bathroom With A Beautiful Walk-In Shower, An RV Garage With RV Hookups Inside And RV Hookups Outside With Extra Parking Room for 2 more. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home! </p>
      <p>&nbsp;</p>
      <p>928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 1 bedroom content -->
  
    <div id="one-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="one-bedroom" src="gallery/one-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:0 auto;"><img src="images/bg-bar.jpg" /></div>
  
  
<div style="width:941px; height:100%; margin: 60px auto 0;">

<div style="width:256px; height:328px; float:left; background:url('images/trans-343434.png')"><h3 class="azrv-edit" style="font-family: inherit;padding: 10PX;background: #d5b03d;border-radius: 10px;width: 50%;color: #fff;margin: -10px 0 10px 28px;">$Call For Price</h3>
<div class="azrv-edit">
<ul style="width: 222px;margin: 0 auto;color: #fff;">
  <li><strong>Size:</strong> 1200 sq ft.</li>
  <li><strong>Lot Size:</strong> 9500 sq ft.</li>
  <li><strong>Bedrooms:</strong> 2</li>
  <li><strong>Bathrooms:</strong> 2</li>
  <li><strong>1 RV Garage</strong></li>
  <li><strong>1 Car Garage</strong></li>
  <li><strong>Garage Size:</strong> 900 sq ft.</li>
  <li><strong>Landscape:</strong> Yes</li>
  <li><strong>Flooring:</strong> Carpet and Tile</li>
  <li><strong>Master Bath:</strong> Double Sink</li>
  <li><strong>Shower:</strong> walk in</li>
  <li><strong>Appliances:</strong> Yes</li>
  <li><strong>Washer/Dryer:</strong> Hookups</li>
  <li><strong>Date Available:</strong> Immediately</li>
  <li><strong>HOA:</strong> Yes</li>
  <li><strong>Year Built:</strong> 2013</li>
</ul>
</div>
<a class="model2" href="#two-bedroom-overlay" style="
    float: right;
    padding: 7px;
    background: #d5b03d;
    margin: -15px 0px 0 0;
    color: #fff;
">Model Info</a>
</div>
<h2 class="azrv-edit" style="color:#777; font-size:48px; position: absolute;text-align: right;margin: 0 0 0 298px;">FOR SALE - 2 Bedroom 2 Bathroom RV Garage+</h2>
<div style="width:674px; height:323px; float: right; background:#fff;text-align: center;margin-top: -18px;"><img src="images/2bed-hero.jpg"></div>
</div>
<div class="cb"></div>

    <!-- Start 2 bedroom content -->
    <div id="two-bedroom-overlay" style="display:none;" class="azrv-edit"> 
      <h2 style="padding: 5px;">Arizona RV Model Home FOR SALE</h2>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Garage</span> - This Large Arizona RV Garage Home comes with a Large Garage for your RV and and extra garage for a vehicle, atv, etc. as these garages can even hold large boats. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bedrooms</span> - There are two nice size bedrooms with the master having it's own master bathroom, the bedrooms are also equiped with built in closet shelving, custom doors and lighting fixtures. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Office</span> - Has plenty of room to handle all of your tasks and is conveniently close to everything else inside the home. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Kitchen</span> - Comes equiped With stainless steel appliances including the refrigerator, range/oven and even a mivrowave. There is also an island-bar with double sink and garbage disposal. There are also custom cabinets with a custom tile splash board. There is also an exit door that leads into the arizona rv garage. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Bathrooms</span> - The house is equiped with 2 full bathrooms that have their own private walk in shower stalls that are custom tiled from the floor to the ceiling with each also having their own dual luxury shower heads. </p>
      <p>&nbsp;</p>
      <p><span style="background:#ccc; font-weight:bold;">The Floorplan</span> - Includes 2 bedrooms with 2 bathrooms with beautiful walk-in showers, an office or a den, an RV Garage with extra vehicle garage, RV Hookups inside and outside with extra room for 2. </p>
      <p>&nbsp;</p>
      <p>Call Or Stop By Today! View Our Models And You Will See Why Arizona RV Homes Is A Place You Can Call Home!</p>
      <p>&nbsp;</p>
      <p> 928-768-2900 - 2530 Nez Perce Rd | Ft Mohave, AZ 86427 - © 2013 Arizona RV Homes. All rights reserved.</p>
    </div>
    <!-- End 2 bedroom content -->
    <div id="two-bedroom" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="two-bedroom" src="gallery/two-bedroom.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 
    </div>
  </div>
  <div style="width:941px; margin:15px auto;"><img src="images/bg-bar.jpg" /></div>
      <div style="width:980px; margin: 0 auto;">
      <h2 class="azrv-edit" style="color:#777; font-size:48px; text-align:center;">View Our Floorplans or Design Your Own</h2>
      <p></p>
    </div>
    <div id="floorplans" style="margin: 0px auto; width: 940px; margin-bottom: -45px;"> 
      <iframe id="floorplans" src="gallery/floorplans.html" style="width: 100%; margin: 0px auto; padding: 0px; height: 225px; overflow: hidden;" frameborder="0" scrolling="no"></iframe> 

</div>
<!-- Start FAQS -->

<div class="nav azrv-edit">
  <div>	 
  <h2 style="margin-left:5px;">General Questions About Arizona RV Homes</h2>
  <input id="label-1" name="faq" type="radio" checked>
  <label for="label-1" id="item1"><i class="icon-question-sign"></i>How big is 
  the RV Garage?</label> 
  <div class="content" id="a1"> 
    <h4>Our standard size is 52 foot deep and 20 foot wide. We can also custom build it to your specifications and make it larger or smaller.</h4>
  </div>
<div id="faqs" style="width:980px; height:100%; display:none;">
<div> 
  <input id="label-2" name="faq" type="radio"/>
  <label for="label-2" id="item2"><i class="icon-question-sign"></i>Can you park on the street?</label> 
  <div class="content" id="a2"> 
    <h4>We provide you with a long driveway and the ability to park along the side of your home (behind the setbacks) or for up to 72 hours in front of your home per CCR’s</h4>
  </div>
</div>
<div> 
  <input id="label-3" name="faq" type="radio"/>
  <label for="label-3" id="item3"><i class="icon-question-sign"></i>Will my truck and boat fit?</label> 
  <div class="content" id="a3"> 
    <h4>It depends on the size of both. We can adjust the driveway and the garage to accommodate both. Flexibility is the rule here.</h4>
  </div>
</div>
<div> 
  <input id="label-4" name="faq" type="radio"/>
  <label for="label-4" id="item4"><i class="icon-question-sign"></i>What size homes will you build?</label> 
  <div class="content" id="a4"> 
    <h4>Our homes can range from 500 square foot to 3500 square foot. You can say that we build what YOU want. Every home will be custom unless you buy 
      an existing model.</h4>
  </div>
</div>
<div> 
  <input id="label-5" name="faq" type="radio"/>
  <label for="label-5" id="item5"><i class="icon-question-sign"></i>Do I own the land?</label> 
  <div class="content" id="a5"> 
    <h4>Our land is “Fee Simple” which means that you own it and it is not leased.</h4>
  </div>
</div>
<div> 
  <input id="label-6" name="faq" type="radio"/>
  <label for="label-6" id="item6"><i class="icon-question-sign"></i>How big are the lots?</label> 
  <div class="content" id="a6"> 
    <h4>Our smallest lots are over 9,500 square foot (74.5’ X 127.6’) in Phase 1 (66 lots)</h4>
  </div>
</div>
<div> 
  <input id="label-7" name="faq" type="radio"/>
  <label for="label-7" id="item7"><i class="icon-question-sign"></i>What are the setbacks?</label> 
  <div class="content" id="a7"> 
    <h4>We have a 20 foot in front, 5 foot on side (10 feet on a corner side lot) and 25 foot in the rear.</h4>
  </div>
</div>
<div> 
  <input id="label-8" name="faq" type="radio"/>
  <label for="label-8" id="item8"><i class="icon-question-sign"></i>Is all of my lot useable?</label> 
  <div class="content" id="a8"> 
    <h4>With front yard utility easements and municipal sewer, your lot is fully useable and you have plenty of room for a pool or larger home and garage.</h4>
  </div>
</div>
<div> 
  <input id="label-9" name="faq" type="radio"/>
  <label for="label-9" id="item9"><i class="icon-question-sign"></i>How many lots are there?</label> 
  <div class="content" id="a9"> 
    <h4>In Phase 1 we have 66 lots. We intend to have a total of 375 lots in 6 phases.</h4>
  </div>
</div>
<div> 
  <input id="label-10" name="faq" type="radio"/>
  <label for="label-10" id="item10"><i class="icon-question-sign"></i>Do you have CCR'S?</label> 
  <div class="content" id="a10"> 
    <h4>Yes we do and they are specifically tailored for RV, Boat and Off Road Vehicle usage.</h4>
  </div>
</div>
<div> 
  <input id="label-11" name="faq" type="radio"/>
  <label for="label-11" id="item11"><i class="icon-question-sign"></i>How much are the association dues?</label> 
  <div class="content" id="a11"> 
    <h4>To start out they are $100 a year. We DO NOT have any common areas, clubhouse or pool to pay for. The money covers the administrative actions and legalities 
      needed for Arizona law.</h4>
  </div>
</div>
<div> 
  <input id="label-12" name="faq" type="radio"/>
  <label for="label-12" id="item12"><i class="icon-question-sign"></i>Are you a gated community?</label> 
  <div class="content" id="a12"> 
    <h4>To keep costs low and not have the expense of road maintenance and other associated costs, we will NOT be gated.</h4>
  </div>
 </div>
</div>
</div>
</div> 
<div style="width:980px; margin:-20px auto 25px;"><p><a href="javascript: toggle('faqs');">Please click here to view more or less questions</a></p></div>
<!-- End FAQS -->
<div style="width:100%; background-color:#fff;"> 
  <div style="padding:20px 0;background-color:#fff;width:980px;height:198px;margin:0 auto;" class="local-shopping-wrapper"> 
    <div class="logo_strip"> 
      <h2 class="azrv-edit">Ft Mohave is a great area with close shopping to:</h2>
      <a href="http://g.co/maps/f5phy" target="_blank" title="Smiths Google Map Directions From Arizona RV Homes"> 
      <div class="smiths"></div>
      </a> <a href="http://g.co/maps/vwevw" target="_blank" title="Safeway Google Map Directions From Arizona RV Homes"> 
      <div class="safeway"></div>
      </a> <a href="http://g.co/maps/eqr6j" target="_blank" title="Target Google Map Directions From Arizona RV Homes"> 
      <div class="target"></div>
      </a> <a href="http://g.co/maps/wjsua" target="_blank" title="The Home Depot Google Map Directions From AriArizona aona RV Homes"> 
      <div class="home-depot"></div>
      </a> <a href="http://g.co/maps/tmf6x" target="_blank" title="Sams Club Google Map Directions From Arizona RV Homes"> 
      <div class="sams-club"></div>
      </a> <a href="http://g.co/maps/shd2w" target="_blank" title="Walmart Google Map Directions From Arizona RV Homes"> 
      <div class="walmart"></div>
      </a> <a href="http://g.co/maps/9ugqc" target="_blank" title="Lowes Google Map Directions From Arizona RV Homes"> 
      <div class="lowes"></div>
      </a> <a href="http://g.co/maps/zf3f3" target="_blank" title="Kohls Google Map Directions From Arizona RV Homes"> 
      <div class="kohls"></div>
      </a> </div>
    <div class="azrv-edit" style="width:900px;">(click logos above for google map directions from arizona rv homes) </div>
  </div>
</div>
</div>
<div style="width:100%;background: url(&quot;images/footer-1.jpg&quot;) repeat scroll 0 0 #1A1A1B;color: #fff;border-top:1px solid #333; margin:25px 0 0 0; padding:15px 0 0 0;height: 71px;text-align: left;"> 
  <div style="width:980px; margin:0 auto;">
  <h2 class="azrv-edit" style="font-size:56px; padding-top:10px; color:#D5B03D;">Like What You See? Call or Stop By Today!</h2>
    <a class="contact" href="#contact-form"><div class="now-selling"></div></a>
</div>
</div>
<div id="footer"> 
  <div class="centerContainer"> 
    <div style="width:490px; margin:0 auto;"> 
      <div style="float:left; width:135x;"><img src="images/equal-opp.png" alt="Equal Oppotunity Housing Logo" width="115" height="116"></div>
      <div style="margin:35px 0 0; float:right; width:370px;"> 
        <h1 class="azrv-edit" style="font-size: 66px !important; font-color:#fff; text-align:center;font-family: 'handwriting', Arial, sans-serif !important; text-transform:capitalize; padding-bottom:7px;">Arizona RV Homes </h1>
        <h1 class="azrv-edit">928-768-2900</h1>
      </div>
      <div style="clear:both;"></div>
    </div>
    <p class="azrv-edit" style="font-size:18px; text-align:center;">&#169; 2013 Arizona RV Homes. All rights reserved. 2530 Nez Perce Rd | Ft Mohave, AZ 86427 </p>
    <div style="margin:20px 0; width:910px; padding-bottom:1px;"> 
      <p> 
      <div style="float:left;"> <a style="color:#d0d0d0 !important; font-family:'titilliumregular', Palatino, Georgia, serif;" target="_blank" href="http://www.facebook.com/arizonarvhomes">Like Our Facebok FanPage</a> 
        <div style="margin: -16px 0 0 232px; display: block;" class="fb-like" data-href="http://www.facebook.com/arizonarvhomes" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left; font-family:'titilliumregular', Palatino, Georgia, serif;"> Like Our Website 
        <div style="margin: -16px 0 0 184px; display: 
              block;" class="fb-like" data-href="http://arizonarvhomes.com" data-width="50" 
              data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
      <p> 
      <div style="float:left;; "> <a style="color:#d0d0d0 !important; font-family:'titilliumregular', Palatino, Georgia, serif;" target="_blank" href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US">Like Our Google+ Places Page</a> 
        <div style="margin: -16px 0 0 250px; display: block;" class="fb-like" data-href="https://plus.google.com/116178801460100886263/about?gl=US&hl=en-US" data-width="50" data-layout="button_count" data-show-faces="false" data-send="false"></div>
      </div>
    </div>
  </div>
</div>
<div id="preload"> <img src="images/yellow-ribbon-small-over.png" /> <img src="images/smiths.jpg" /> <img src="images/safeway.jpg" /> 
  <img src="images/target.jpg" /> <img src="images/home-depot.jpg" /> <img src="images/sams-club.jpg" /> 
  <img src="images/walmart.jpg" /> <img src="images/lowes.jpg" /> <img src="images/kohls.jpg" /> <img src="images/now-selling.png" />
</div>
<script type="text/javascript">
function toggle(elementID){
var target = document.getElementById(elementID)
if (target.style.display == 'none') {
target.style.display = 'block'
} else {
target.style.display = 'none'
}
} 

$(document).ready(function() {
	$(".contact").fancybox({
		maxWidth	: '60%',
		maxHeight	: '90%',
		fitToView	: true,
		width		: '60%',
		height		: '90%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".googlemap").fancybox({
		maxWidth	: '85%',
		maxHeight	: '85%',
		fitToView	: true,
		width		: '85%',
		height		: '85%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model1").fancybox({
		maxWidth	: '60%',
		maxHeight	: '85%',
		fitToView	: true,
		width		: '85%',
		height		: '60%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model2").fancybox({
		maxWidth	: '60%',
		maxHeight	: '95%',
		fitToView	: true,
		width		: '85%',
		height		: '85%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(function(){
      $('form').submit(function(e){
        var thisForm = $(this);
        e.preventDefault();
        $(this).fadeOut(function(){
          $("#loading").fadeIn(function(){
            $.ajax({
              type: 'POST',
              url: thisForm.attr("action"),
              data: thisForm.serialize(),
              success: function(data){
                $("#loading").fadeOut(function(){
                  $("#success").text(data).fadeIn();
                });
              }
            });
          });
        });
      })
    });
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=719517228073996";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>