<?php
//error_reporting(0);
session_start();

include('config.php');
include('php/simple_html_dom.php');

//get requests
$action = $_REQUEST['action'];
$refering_page = $_SERVER['HTTP_REFERER'];
$filename = $_REQUEST['file'];
$content = stripslashes($_REQUEST['elm1']);
$id = $_REQUEST['id'];

//check if logged in
if($action != "login" && $action != "getlogin"){
	if($_REQUEST['u'] !="" && $_REQUEST['p'] !=""){
		echo 'error';
	}
	if($_SESSION['registered'] != $username.$password){
		//log out
		session_destroy();
		//silance is golden
		exit();
	}
}
switch ($action){

	case 'login':
		if($_REQUEST['u'] == $username && $_REQUEST['p'] == $password){
			//register session
			$_SESSION['registered'] = $username.$password;
			//go back to the referring page
			echo 'loggedin';
			exit();
		}else{
			//log out
			session_destroy();
			//go back to the referring page
			echo 'error';
		}
		exit();
	break;
	
	case 'logout':		
		//log out
		session_destroy();
		//go back to the referring page
		echo 'loggedout';
		exit();
	break;
	
	case 'getfilename':
		echo $default_page;
	break;
	
	case 'getstyles':
		echo $default_css;
	break;
	
	case 'getlogin':
		if($_SESSION['registered'] != $username.$password){
			echo 'error';	
		}else{
			echo 'loggedin';	
		}
		exit();
	break;
	
	case'phpinfo':
		phpinfo();
	break;
	
	case "save":
		if($_SESSION['registered']==""){
			echo 'error';
			exit();
		}
		//make sure filename is not a folder
		if(is_dir("../".$filename)){
			$filename = $filename.$default_page;
		}
		//get edits
		$edits = stripslashes($content);
		//get HTML from current file
		$html = file_get_html("../".$filename);
		//replace content of the div with edits
		//foreach($html->find('[class=".azrv-edit"]', $id) as $element) {$element->innertext=$edits;}
		$html->find('[class=azrv-edit]', $id)->innertext = $edits;
		$str = $html->save();

		// Dumps the internal DOM tree back into a file
		$html->save("../".$filename);
		$html->clear(); 
		unset($html);
		//notify user
		echo 'Saved';
		exit();
	break;
	
}
?>