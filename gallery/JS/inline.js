$(document).ready(function() {
	$(".contact").fancybox({
		maxWidth	: '70%',
		maxHeight	: '75%',
		fitToView	: true,
		width		: '70%',
		height		: '75%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".googlemap").fancybox({
		maxWidth	: '85%',
		maxHeight	: '85%',
		fitToView	: true,
		width		: '85%',
		height		: '85%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model1").fancybox({
		maxWidth	: '60%',
		maxHeight	: '85%',
		fitToView	: true,
		width		: '85%',
		height		: '60%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(document).ready(function() {
	$(".model2").fancybox({
		maxWidth	: '60%',
		maxHeight	: '90%',
		fitToView	: true,
		width		: '85%',
		height		: '75%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		openSpeed       : '250',
		closeSpeed	: '250'
	});
});

$(function(){
      $('form').submit(function(e){
        var thisForm = $(this);
        e.preventDefault();
        $(this).fadeOut(function(){
          $("#loading").fadeIn(function(){
            $.ajax({
              type: 'POST',
              url: thisForm.attr("action"),
              data: thisForm.serialize(),
              success: function(data){
                $("#loading").fadeOut(function(){
                  $("#success").text(data).fadeIn();
                });
              }
            });
          });
        });
      })
    });