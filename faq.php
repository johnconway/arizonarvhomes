<?
?>
<link href="css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<script src="JS/jquery-1.9.1.js"></script>
<script src="JS/jquery-ui-1.10.3.custom.js"></script>
<script>
  $(function() {
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#accordion" ).accordion({
      icons: icons,
      collapsible: true,
      heightStyle: "content",
      active: false	
    });
    $( "#toggle" ).button().click(function() {
      if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });
  });
  </script>
</head>
<body>


<div style="width:100%; background:#000;padding: 20px 0;margin: -15px 0;">
<div style="width:950px; margin:0 auto;"> 
<h2 style="padding:15px 0;color:#ddd;">General Questions About Arizona RV Homes</h2>
  <div id="accordion"> 
    <h3>How big is the garage?</h3>
    <div> 
      <p>Our standard size is 52 foot deep and 20 foot wide, which allows you 
        to park your RV and extend the side outs and fully use it as part of the 
        living area. We can also custom build it to your specifications and make 
        it larger or smaller.</p>
    </div>
    <h3>Can you park on the street?</h3>
    <div> 
      <p>We provide you with a long driveway and the ability to park along the 
        side of your home (behind the setbacks) or for up to 72 hours in front 
        of your home per CCR&#146;s</p>
    </div>
    <h3>Will my truck and boat fit?</h3>
    <div> 
      <p>It depends on the size of both. We can adjust the driveway and the garage 
        to accommodate both. Flexibility is the rule here.</p>
    </div>
    <h3>What size homes will you build?</h3>
    <div> 
      <p>Our homes can range from 500 square foot to 3500 square foot. You can 
        say that we build what YOU want. Every home will be custom unless you 
        buy an existing model.</p>
    </div>
    <h3>Do I own the land?</h3>
    <div> 
      <p>Our land is &#147;Fee Simple&#148; which means that you own it and it 
        is not leased.</p>
    </div>
    <h3>How big are the lots?</h3>
    <div> 
      <p>Our smallest lots are over 9,500 square foot (74.5&#146; X 127.6&#146;) 
        in Phase 1 (66 lots)</p>
    </div>
    <h3>What are the setbacks?</h3>
    <div> 
      <p>We have a 20 foot in front, 5 foot on side (10 feet on a corner side 
        lot) and 25 foot in the rear.</p>
    </div>
    <h3>Is all of my lot useable?</h3>
    <div> 
      <p>With front yard utility easements and municipal sewer, your lot is fully 
        useable and you have plenty of room for a pool or larger home and garage.</p>
    </div>
    <h3>How many lots are there?</h3>
    <div> 
      <p>In Phase 1 we have 66 lots. We intend to have a total of 375 lots in 
        6 phases.</p>
    </div>
    <h3>Do you have CCR'S?</h3>
    <div> 
      <p>Yes we do and they are specifically tailored for RV, Boat and Off Road 
        Vehicle usage.</p>
    </div>
    <h3>How much are the association dues?</h3>
    <div> 
      <p>To start out they are $100 a year. We DO NOT have any common areas, clubhouse 
        or pool to pay for. The money covers the administrative actions and legalities 
        needed for Arizona law.</p>
    </div>
    <h3>Are you a gated community?</h3>
    <div> 
      <p>To keep costs low and not have the expense of road maintenance and other 
        associated costs, we will NOT be gated.</p>
    </div>
  </div>
</div>
</div>