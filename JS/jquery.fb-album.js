/**
 * jQuery Facebook Album Plugin
 * @name jquery.fb-album.js
 * @version 3.3
 * @category jQuery Plugin
 */

// Check if Gallery is embedded via iFrame
var isInIFrame 				= (window.location != window.parent.location) ? true : false;

// Other Global Variables
var iFrameWidth 			= 0;
var iFrameHeight 			= 0;
var iFrameAdjust			= 0;
var viewPortWidth 			= 0;
var viewPortHeight			= 0;
var scrollBarWidth			= 0;
var galleryWidth			= 0;
var smartAlbumsPerPage		= 0;
var smartPhotosPerPage		= 0;
var totalItems				= 0;
var galleryContainer 		= "";
var galleryResponsive 		= false;
var controlBarAdjust		= 0;
var buttonWidthText			= 0;
var buttonWidthImage		= 0;
var currentPageList			= "";
var AlbumThumbWidth			= 0;
var AlbumThumbHeight		= 0;
var PhotoThumbWidth			= 0;
var PhotoThumbHeight		= 0;
var TotalThumbs 			= 0;
var TotalPages 				= 0;
var TotalTypes 				= 0;
var SortingOrder 			= "";
var SortingType 			= "";

// Functions to retrieve Screen and iFrame Dimensions
function GetScreenDimensions() {
	if (typeof window.innerWidth != 'undefined') {
		// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
		viewPortWidth = 			parent.window.innerWidth;
		viewPortHeight = 			parent.window.innerHeight;
	} else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
		// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
		viewPortWidth = 			parent.document.documentElement.clientWidth;
		viewPortHeight = 			parent.document.documentElement.clientHeight;
	} else {
		// older versions of IE
		viewPortWidth = 			parent.document.getElementsByTagName('body')[0].clientWidth;
		viewPortHeight = 			parent.document.getElementsByTagName('body')[0].clientHeight;
	};
}
function GetIFrameDimensions() {
	if ((typeof window.innerWidth != 'undefined') && (typeof( window.innerWidth ) == 'number')) {
		//Non-IE
		iFrameWidth = 				window.innerWidth;
		iFrameHeight = 				window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		iFrameWidth = 				document.documentElement.clientWidth;
		iFrameHeight = 				document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		iFrameWidth = 				document.body.clientWidth;
		iFrameHeight = 				document.body.clientHeight;
	}
}

// Extend the jQuery Array Functions
(function ($) {
	$.ajaxSetup({ cache: false });
	Array.prototype.frequencies = function() {
		var l = this.length, result = {all:[]};
		while (l--){
		   result[this[l]] = result[this[l]] ? ++result[this[l]] : 1;
		}
		// all pairs (label, frequencies) to an array of arrays(2)
		for (var l in result){
		   if (result.hasOwnProperty(l) && l !== 'all'){
			  result.all.push([ l,result[l] ]);
		   }
		}
		return result;
	};
	//Extend arrays to have a contains method
	if (!("contains" in Array.prototype)){
		Array.prototype.contains = function ( needle ) {
			var i = 0;
			for (i in this) {
				if (this[i] === needle) {
					return true;
				};
			};
			return false;
		};
	};
})( jQuery );

// Custom plugin for a Slide In/Out Animation with a Fade
(function ($) {
	$.fn.slideFade = function (speed, callback) {
		var slideSpeed;
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == "number") {
				slideSpeed  = arguments[i];
			}
			else {
				var callBack = arguments[i];
			}
		}
		if(!slideSpeed) {
			slideSpeed = 500;
		}
		this.animate({
				opacity: 'toggle',
				height: 'toggle'
			}, slideSpeed,
			function(){
				if( typeof callBack != "function" ) { callBack = function(){}; }
				callBack.call(this);
			}
		);
  };
})( jQuery );

// Additional Plugins and Functions used for the Script
(function ($) {
	//case-insensitive version of :contains
	$.extend($.expr[":"], {"containsNC": function(elem, i, match, array) {return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;}});
	// Plugin to check and compare jQuery versions
	//$.isVersion("1.4.2"); --> returns true, if $().jquery == "1.4.2"
	//$.isVersion("1.3.2", ">"); --> returns true if $().jquery > "1.3.2"
	//$.isVersion("1.3", ">", "1.2.6"); --> returns true
	//$.isVersion("1.3.2", "<", "1.3.1"); --> returns false
	//$.isVersion("1.4.0", ">=", "1.3.2"); --> returns true
	//$.isVersion("1.4.1", "<=", "1.4.1"); --> returns true
	//$.isVersion("1.4.2", "<=", "1.4.2pre"); --> returns false
    /**
     * @param {string} left A string containing the version that will become the left hand operand.
     * @param {string} oper The comparison operator to test against. By default, the "==" operator will be used.
     * @param {string} right A string containing the version that will become the right hand operand. By default, the current jQuery version (jQuery.fn.jquery) will be used.
     * @return {boolean} Returns the evaluation of the expression, either true or false.
    */
	$.isVersion = function(left, oper, right) {
		if (left) {
			var pre = /pre/i,
				replace = /[^\d]+/g,
				oper = oper || "==",
				right = right || $().jquery,
				l = left.replace(replace, ''),
				r = right.replace(replace, ''),
				l_len = l.length, r_len = r.length,
				l_pre = pre.test(left), r_pre = pre.test(right);
			l = (r_len > l_len ? parseInt(l) * Math.pow(10, (r_len - l_len)) : parseInt(l));
			r = (l_len > r_len ? parseInt(r) * Math.pow(10, (l_len - r_len)) : parseInt(r));
			switch(oper) {
				case "==": {return (true === (l == r && (l_pre == r_pre)));};
				case ">=": {return (true === (l >= r && (!l_pre || l_pre == r_pre)));};
				case "<=": {return (true === (l <= r && (!r_pre || r_pre == l_pre)));};
				case ">": {return (true === (l > r || (l == r && r_pre)));};
				case "<": {return (true === (l < r || (l == r && l_pre)));};
			};
		};
		return false;
	};
	// jQuery Plugin for event that happens once after a window resize
    var $event = $.event, $special, resizeTimeout;
    $special = $event.special.debouncedresize = {
        setup: function() {$( this ).on( "resize", $special.handler );},
        teardown: function() {$( this ).off( "resize", $special.handler );},
        handler: function( event, execAsap ) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function() {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply( context, args );
                };
            if ( resizeTimeout ) {clearTimeout( resizeTimeout );}
            execAsap ? dispatch() : resizeTimeout = setTimeout( dispatch, $special.threshold );
        },
        threshold: 150
    };
	// Adjust element width for responsive layout after window resize
	$(window).on("debouncedresize", function( event ) {
		// Reset viewPort Dimensions
		GetScreenDimensions();
		// Adjust Height of iFrame Container (if applicable)
		setTimeout(function(){
			if (isInIFrame) {
				GetIFrameDimensions();
				var galleryContainerHeight = $("#" + galleryContainer).height() + iFrameAdjust;
				var galleryContainerWidth = $("#" + galleryContainer).width() + 4;
				var IFrameID = getIframeID(this);
				if (IFrameID != "N/A") {
					window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
					parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
					/*if (galleryResponsive) {
						window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
						parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
					}*/
				};
			};
		}, 100);
	});
	// Detect if Scrollbar is present
	$.fn.isScrollable = function(){
		var elem = $(this);
		return (
		elem.css('overflow') == 'scroll'
			|| elem.css('overflow') == 'auto'
			|| elem.css('overflow-x') == 'scroll'
			|| elem.css('overflow-x') == 'auto'
			|| elem.css('overflow-y') == 'scroll'
			|| elem.css('overflow-y') == 'auto'
		);
	};
})(jQuery);

// Function to retrieve iFrame ID in which gallery is embedded (if applicable)
function getIframeID(el) {
	var myTop = top;
	var myURL = location.href.split('?')[0];
	var iFs = top.document.getElementsByTagName('iframe');
	var x, i = iFs.length;
	while ( i-- ){
		x = iFs[i];
		if (x.src && x.src == myURL){
			//return 'The iframe ' + ((x.id)? 'has ID=' + x.id : 'is anonymous');
			return ((x.id)? x.id : 'N/A');
		};
	};
	return 'N/A';
};

// jQuery Facebook Gallery
(function($) {
    $.fn.FB_Album = function (opts) {
		// Set Treshold for Window Resizing Watch
		$.event.special.debouncedresize.threshold = 250;

		// Function to check if Logging Console already exists, otherwise Create
		if (!"console" in window || typeof console == "undefined") {
			var methods = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
			var emptyFn = function () {};
			window.console = {};
			for (var i = 0; i < methods.length; ++i) {window.console[methods[i]] = emptyFn;};
		};

		// Function to retrieve Absolute Path for a File
		function getAbsolutePath() {
			var loc = window.location;
			var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
			return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
		};

		// Define Gallery Options
		opts = $.extend({
            facebookID: 					'',											// Define your Facebook ID
				excludeAlbums: 				[],											// ID's of albums that are to be exclude from showing (only applies if "showSelectionOnly" is set to "false")

			singleAlbumOnly:				false,										// Set to "true" if you want to show only one specific Facebook Album
				singleAlbumID:				'',											// Define the ID of the single album you want to show
				useGalleriffic:				true,										// If "true", single album will be shown in alternative Galleriffic layout (requires Galleriffic plugin)

			showSelectionOnly:				true,										// Set to "true" if you want to show only specified albums; otherwise all albums will be pulled (minus the ones marked to be excluded)
				includeAlbums:				[],											// ID's of albums that you want to be shown (only applies if "showSelectionOnly" is set to "true")

			maxNumberGalleries:				200,											// Define how many galleries should be pulled from Facebook (0 = all albums; only applies if "showSelectionOnly" is set to "false")
				excludeTimeLine:			true,										// Define if the "Timeline" album should be excluded automatically

			maxNumberImages:				5000,											// Define how many images per gallery should be pulled from Facebook (set to "0" (zero) if all images should be pulled)
				excludeImages: 				[],											// ID's of images that are to be exclude from showing

			maxGraphLimit:					1000,										// Internal Setting for Graph Request (only used if "maxNumberGalleries" is "0")

			innerImageScaler:				false,										// If set to true, the script will use an internal php function to scale images to size, otherwise a cloud based service (http://src.sencha.io) will be used
				PathInternalPHP:			'PHP/TimThumb.php',							// Define path and name to internal PHP Image Scaler
			imageLazyLoad:					true,										// Define if lazyload for thumbnails should be utilized (images will only be loaded if in view)

			responsiveGallery:				true,										// Define if gallery is supposed to be responsive to window size changes
				responsiveWidth:			90,											// Set percent of window width the responsive frameID container should have; only enter number but no '%' behind number
				fixedWidth:					800,										// Set window width in px for a fixed size frameID container; only enter number but no 'px' behind number
				iFrameHeightAdjust:			24,											// Define an adjustment in px that will be used to offset the height adjustment for the iFrame the gallery is embedded in (if applicable)

			PathNoCoverImage:				'CSS/Images/no_cover.png',					// Set path to image to be used if Facebook doesn't provide a Cover Image for an album
			showAlbumDescription:			true,										// Set to "true" if you want to show the album description in album view

			cacheAlbumContents:				false,										// Set to "true" if you want to keep albums already loaded in DOM and reuse or "false" for reloading every time
			albumSearchControls:			true,										// If "true", a album search feature will be provided

			paginationLayoutAlbums:			true,										// Set to "true" if you want to use pagination for the album thumbnails
				smartAlbumsPerPageAllow:	true,										// If "true", the script will auto paginate the album thumbnails based on screen dimensions
					setAlbumsByPages:		false,										// If "true", you can manually set the total number of pages for album thumbnails
					numberAlbumsPerPage:	9,											// Set the number of albums that should be shown per page (if paginationLayoutAlbums = true)
					numberPagesForAlbums:	3,											// Set the number of pages for album thubmnails that should be shown (if setAlbumsByPages = true)
			paginationLayoutPhotos:			true,										// Set to "true" if you want to use pagination for the photo thumbnails
				smartPhotosPerPageAllow:	true,										// If "true", the script will auto paginate the photo thumbnails based on screen dimensions
					setPhotosByPages:		false,										// If "true", you can manually set the total number of pages for photo thumbnails
					numberPhotosPerPage:	16,											// Set the number of photos that should be shown per page (if paginationLayoutPhotos = true)
					numberPagesForPhotos:	6,											// Set the number of pages for photo thumbnails that should be shown (if setPhotosByPages = true)
				showBottomControlBar:		true,										// Set to "true" if you want to show a control bar at the bottom of the detailed album view (includes a 2nd "Back" and a "Scroll to Top" Button)
			showTopPaginationBar:			true,										// If "true", a pagination control bar (first / prev / next / last page) will be shown above the thumbnails
			showBottomPaginationBar:		true,										// If "true", a pagination control bar (first / prev / next / last page) will be shown below the thumbnails
			showThumbInfoInPageBar:			true,										// If "true", thumbnail count (i.e. Album 1 to 6 out of 20) will be shown in Pagination Bars

			floatingControlBar:				true,										// If set to "true", the control bar will follow the user while scrolling up/down (only if not in iFrame!)
				controlBarTopOffset:		10,											// Allows for an offset in px for the floating controlbar in order to account for menus or other top-fixed elements
				showFloatingReturnButton:	false,										// If "true", a return button will be shown in album detail view
				showFloatingToTopButton:	true,										// If "true", a Go-To-Top button will be shown in the floating control bar

			// Settings for Tooltips
			// ---------------------
			tooltipTipAnchor:				'title',									// Define what anchor or data-key should be used to store tooltips (i.e. "alt", "title", etc.)
			tooltipUseInternal:				true,										// Define if the internal tooltip script (qTip2) should be utilized
				tooltipDesign:				'qtip-jtools',								// Define which design to choose from for the qTip2 Plugin
			createTooltipsAlbums:			true,										// Add Tooltip class "TipGallery" to Album Thumbnails
			createTooltipsPhotos:			true,										// Add Tooltip class "TipPhoto" to Photo Thumbnails
			createTooltipsLightbox:			true,										// Add Tooltip class "TipLightbox" to Description Text in Lightbox

			// General Settings for Filter/Search Feature
			// ------------------------------------------
			albumsFilterAllow:				true,										// If "true", provides a filter to filter albums by either dates created or last updated
				albumsFilterAllEnabled:		false,										// If "true", all album filter selections will be unchecked by default
				useAlbumsUpdated:			true,										// If "true", filter will use date last updated; if "false", filter will use date created
			photosFilterAllow:				true,										// If "true", provides a filter to filter photos by dates last added to the album
				photosFilterAllEnabled:		false,										// If "true", all photo filter selections will be unchecked by default
			sortFilterNewToOld:				true,										// If "true", all filter criteria will be sorted from newest to oldest or reverse when "false"

			// General Settings for Sorting Feature
			// ------------------------------------
			albumSortControls:				true,										// Allow for Sorting of Album Thumbnails
				albumAllowSortName:			true,										// Allow for Sorting by Album Name
				albumAllowSortItems:		true,										// Allow for Sorting by Number of Images per Album
				albumAllowSortCreated:		true,										// Allow for Sorting by Date Album has been created
				albumAllowSortUpdate:		true,										// Allow for Sorting by Date Album has last been updated
				albumAllowSortFacebook:		false,										// Allow for Sorting by order as provided by Facebook
				albumAllowSortID:			false,										// Allow for Sorting by Facebook ID
			photoSortControls:				true,										// Allow for Sorting of Photo Thumbnails
				photoAllowSortAdded:		true,										// Allow for Sorting by Date Photo has been added to Album
				photoAllowSortUpdate:		true,										// Allow for Sorting by Date Photo has last been updated
				photoAllowSortFacebook:		false,										// Allow for Sorting by order as provided by Facebook
				photoAllowSortID:			false,										// Allow for Sorting by Facebook ID

			// Settings for Initial Album Thumbnail Sorting Order
			// --------------------------------------------------
			defaultSortDirectionASC:		true,										// Set to "true" for ascending (oldest to newest) and "false" for descending (newest to oldest) default sort direction for album thumbnails
				defaultSortByAlbumTitle:	true,										// Set to "true" if the default sorting criteria should be the album title
				defaultSortByNumberImages:	false,										// Set to "true" if the default sorting criteria should be the number of images per album
				defaultSortByDateCreated:	false,										// Set to "true" if the default sorting criteria should be the date at which album was created
				defaultSortByDateUpdated:	false,										// Set to "true" if the default sorting criteria should be the date at which album was last updated
				defaultSortByFacebookOrder:	false,										// Set to "true" if the default sorting criteria should be the order at which albums were received from Facebook
				defaultSortByFacebookID:	false,										// Set to "true" if the default sorting criteria should be the album ID as assigned by Facebook

			// Settings for Initial Photo Thumbnail Sorting Order
			// --------------------------------------------------
			defaultPhotoDirectionsASC:		true,										// Set to "true" for ascending (oldest to newest) and "false" (newest to oldest) for descending default sort direction for photo thumbnails
				defaultPhotoSortAdded:		true,										// Set to "true" if the default sorting criteria should be the date at which photo was added to the album
				defaultPhotoSortUpdated:	false,										// Set to "true" if the default sorting criteria should be the date at which photo was last updated
				defaultPhotoSortOrder:		false,										// Set to "true" if the default sorting criteria should be the order at which photos were received from Facebook
				defaultPhotoSortID:			false,										// Set to "true" if the default sorting criteria should be the photo ID assigned by Facebook

			// Settings for Text Items in Sorting Controls
			// -------------------------------------------
			PagesButtonText:				'Change Page',								// Define Text for Page Gallery Button
			SortButtonTextAlbums:			'Sort Albums',								// Define Text for Sorting Button (Albums)
			SortButtonTextPhotos:			'Sort Photos',								// Define Text for Sorting Button (Photos)
				SortNameText:				'Album Name',								// Define Text for Sorting Option (Sort by Album Name)
				SortItemsText:				'Number Images',							// Define Text for Sorting Option (Sort by Number Items)
				SortAddedText:				'Date Added',								// Define Text for Sorting Option (Sort by Date Photo Added to Album)
				SortCreatedText:			'Date Created',								// Define Text for Sorting Option (Sort by Date Created)
				SortUpdatedText:			'Last Update',								// Define Text for Sorting Option (Sort by Date Updated)
				SortFacebookText:			'Facebook Order',							// Define Text for Sorting Option (Sort by Order as provided by Facebook)
				SortIDText:					'Facebook ID',								// Define Text for Sorting Option (Sort by Facebook ID)
			FilterButtonTextAlbums:			'Albums Updated',							// Define Text for Filter Button (Albums)
			FilterButtonTextPhotos:			'Photos Added',								// Define Text for Filter Button (Photos)
			SearchButtonTextAlbums:			'Search Albums',							// Define Text for Search Button (Albums)
			SearchButtonTextPhotos:			'Search Photos',							// Define Text for Search Button (Photos)
			SearchDefaultText:				'Search ...',								// Define Text for Default Search Term

			// Settings for Text Items in Album Preview
			// ----------------------------------------
			AlbumContentPreText:			'Content:',									// Adjust width of CSS classes .albumCount, .albumCreate, .albumUpdate, .albumNumber if necessary
			AlbumCreatedPreText:			'Created:',									// Adjust width of CSS classes .albumCount, .albumCreate, .albumUpdate, .albumNumber if necessary
			AlbumUpdatedPreText:			'Updated:',									// Adjust width of CSS classes .albumCount, .albumCreate, .albumUpdate, .albumNumber if necessary
			AlbumShareMePreText:			'Share Album:',								// Define text shown before "Share Album" Links
			AlbumNumericIDPreText:			'Album ID:',								// Adjust width of CSS classes .albumCount, .albumCreate, .albumUpdate, .albumNumber if necessary
			OutOfTotalImagesPreText:		'out of',									// Define pre text when there are more images in album that the script is allowed to pull
			SingleImageWord:				'Image',									// Define word for a single Image
			MultiImagesWord:				'Images',									// Define word for multiple Images

			// Settings for Text Items in Photo Preview
			// ----------------------------------------
			AlbumBackButtonText:			'Back',										// Define text for back button in album preview
			AlbumTitlePreText:				'',								// Define text shown before album name
			AlbumLinkButtonText:			'Click here to view Album on Facebook',		// Define text shown for text link to original Facebook Album
			AlbumNoDescription:				'No Album Description available.',			// Define text to be shown if there is no album description available
			ImageLocationPreText:			'Picture(s) taken at',						// Define text shown before image location text (actual loaction pulled from Facebook; if available)
			ImageNumberPreText:				'Image ID:',								// Define text shown before Image ID Number
			ImageShareMePreText:			'Share Image:',								// Define text shown before "Share Image" Links
			colorBoxNoDescription:			'No Image Description available.',			// Define text to be shown in colorBox if no image description available

			// Settings for Album Thumbnails
			// -----------------------------
			albumNameTitle:					true,										// Add Name / Title of Album to each Album Thumbnail
				albumNameAbove:				true,										// If "true", the album name will be shown above the thumbnail, otherwise below
				albumNameShorten:			true,										// If "true", the album name shown will automatically be shortened to avoid unnecessary linebreaks
			albumImageCount:				true,										// Add Image Count per Album Below Album Thumbnail
			albumDateCreate:				true,										// Add Date Created below Album Thumbnail
				albumCreateFromNow:			true,										// Define if date created should be converted into a "from now" period (i.e. 2 days ago)
			albumDateUpdate:				true,										// Add Date Last Updated below Album Thumbnail
				albumUpdateFromNow:			true,										// Define if date last updated should be converted into a "from now" period (i.e. 2 days ago)
			albumFacebookID:				false,										// Add Album ID below Album Thumbnail; ID can be used to exclude album from showing

			matchAlbumPhotoThumbs:			false,										// Set to true if you want to make the album thumbnails look like the photo thumbnails (photo thumbnail settings will be used)
				albumWrapperWidth:			290,										// Define width for each Album Wrapper (should equal albumThumWidth + 2x albumFrameOffset!)
				albumThumbWidth: 			280,										// Define width for each Album Thumbnail (deduct at least 2x albumFrameOffset from albumWrapperWidth to allow for frame offset)
				albumThumbHeight: 			200,										// Define Height for each Album Thumbnail
				albumFrameOffset:			5,											// Define offset for 2nd Album Thumbnail border to create stacked effect (set to "0" (zero) to disable stack effect)
				albumThumbPadding:			0,											// This is just a placeholder variable; no need to change since it will be automatically filled!
			albumWrapperMargin:				10,											// Define margin for each Album Wrapper
			albumShadowOffset:				12,											// Define additional offset (top) for album shadow to fine-tune shadow position
			albumInfoOffset:				0,											// Define additional offset (top) for album information section (name, content, dates)
			albumThumbOverlay:				true,										// Add Magnifier Overlay to Thumbnail
			albumThumbRotate:				true,										// Add Hover Rotate / Rumble Effect to Album Thumbnail (rotate does not work in IE 8 or less; rumble effect compensates)
				albumRumbleX:				3,											// Define Rumble Movement on X-Scale for Album Thumbnails
				albumRumbleY:				3,											// Define Rumble Movement on Y-Scale for Album Thumbnails
				albumRotate:				3,											// Define Rotation Angle on X+Y-Scale for Album Thumbnails
				albumRumbleSpeed:			150,										// Define Speed for Rumble / Rotate Effect for Album Thumbnails
			albumShowPaperClipL:			true,										// PaperClip on the left
			albumShowPaperClipR:			false,										// PaperClip on the Right
			albumShowPushPin:				false,										// Centered Pushin
			albumShowShadow:				true,										// Show Shadow below Album Thumbnail (use only one shadow type below)
				albumShadowA:				true,										// Images Show Shadow Type 1 (default if none selected and "albumShowShadow" = "true")
				albumShadowB:				false,										// Images Show Shadow Type 2
				albumShadowC:				false,										// Images Show Shadow Type 3
			albumCCS3Shadow:				false,										// CSS3 Show Shadow Type (adds class "ShadowCSS3" to elements; independent from image shadow types)
			albumShowSocialShare:			true,										// Add Section to share album via Facebook, Twitter and Google
				albumShowOrder:				false,										// Show Number of album (derived from order as provided by Facebook)

			// Settings for Photo Thumbnails
			// -----------------------------
			photoThumbWidth: 				210,										// Define Width for each Photo Thumbnail
			photoThumbHeight: 				155,										// Define Height for each Photo Thumbnail
			photoThumbMargin: 				10,											// Define Margin (top-left-bottom-right) for each Photo Thumbnail for space between each thumbnails
			photoThumbPadding:				5,											// Define Padding (top-left-bottom-right) for each Photo Thumbnail for photo frame
			photoThumbOverlay:				true,										// Add Magnifier Overlay to Photo Thumbnail
			photoThumbRotate:				true,										// Add Hover Rotate / Rumble Effect to Photo Thumbnail (rotate does not work in IE 8 or less; rumble effect compensates)
				photoRumbleX:				5,											// Define Rumble Movement on X-Scale for Photo Thumbnails
				photoRumbleY:				5,											// Define Rumble Movement on Y-Scale for Photo Thumbnails
				photoRotate:				5,											// Define Rotation Angle on X+Y-Scale for Photo Thumbnails
				photoRumbleSpeed:			150,										// Define Speed for Rumble / Rotate Effect for Photo Thumbnails
			photoShowClearTape:				true,										// Add Clear Tape on Top of Photo Thumbnail
			photoShowYellowTape:			false,										// Add Yellow Tape on Top of Photo Thumbnail
			photoShowPushPin:				false,										// Add Centered Pushin on Top of Photo Thumbnail
			photoShowIconFBLink:			true,										// Set to "true" if you want to show a icon link to the original Facebook Album
			photoShowTextFBLink:			true,										// Set to "true" if you want to show a text link to the original Facebook Album
			photoShowNumber:				false,										// Add Facebook Image ID Number below Thumbnail
			photoShowSocialShare:			true,										// Add Section to share photo via Facebook, Twitter and Google
				photoShowOrder:				false,										// Show Number of photo (derived from order as provided by Facebook)

			// Settings for Optional Lightbox (Colorbox)
			// -----------------------------------------
			fancyBoxAllow:					true,										// Add fancyBox (Lightbox) to Photo Thumbnails; if not, images will open up in new tab / window
			fancyBoxOptions: 				{},											// Options for fancyBox Lightbox Plugin (currently not active yet; preparation for future update!)
			colorBoxAllow:					false,										// Add colorBox (Lightbox) to Photo Thumbnails; if not, images will open up in new tab / window
			colorBoxOptions: 				{},											// Options for colorBox Lightbox Plugin (currently not active yet; preparation for future update!)
			prettyPhotoAllow:				false,										// Add prettyPhoto (Lightbox) to Photo Thumbnails; if not, images will open up in new tab / window
			prettyPhotoOptions:				{},											// Options for prettyPhoto Lightbox Plugin (currently not active yet; preparation for future update!)

			// Debug Settings (Experimental)
			// -----------------------------
			outputCountAlbumID:				false,										// Shows a popup with album counter and album ID for each album found and not excluded while looping
			consoleLogging:					true,										// Define if error/success messages and notices should be logged into the browser developer console

			// Don't change any ID's unless you are also updating the corresponding CSS file
			// -----------------------------------------------------------------------------
			frameID: 						$(this).attr("id"),							// ID of element in which overall gallery script is to be shown
			loaderID: 						'FB_Album_Loader',							// ID of element in which gallery loader animation is to be shown ... ensure ID matches the one used in CSS settings!
			galleryID: 						'FB_Album_Display'							// ID of element in which gallery thumbnails are to be shown ... ensure ID matches the one used in CSS settings!
		}, opts);

		// Check if Selection Only Mode with no Albums and Switch to Standard Mode
		if ((opts.showSelectionOnly) && (opts.includeAlbums.length == 0)) {
			opts.showSelectionOnly = false;
		} else if ((opts.showSelectionOnly) && (opts.includeAlbums.length > 0)) {
			opts.maxNumberGalleries = opts.includeAlbums.length;
		};

		// Check if Selection Only Mode with 1 Album Only and Switch to Single Mode
		if ((opts.showSelectionOnly) && (opts.includeAlbums.length == 1)) {
			opts.singleAlbumOnly = true;
			opts.showSelectionOnly = false;
			opts.singleAlbumID = opts.includeAlbums;
			if (opts.consoleLogging) {
				console.log('User set script to "album-selection" mode with only one album (' + opts.includeAlbums + ') specified. Script has been reset to "single-album" mode.');
			};
		};

		// Define Some Script Variables
		var counterA = 					0;
		var counterB = 					0;
		var images = 					0;
		var albumCount =				0;
		var albumId = 					opts.singleAlbumID;
		var headerArray = 				new Array();
		var footerArray = 				new Array();
		var graphLimitA =				opts.maxNumberGalleries;
		var graphLimitB =				opts.maxNumberImages;
		var defaultSortTypeAlbums =		'';
		var defaultSortTypePhotos =		'';
		var defaultSortArrayAlbums =	new Array();
		var defaultSortArrayPhotos =	new Array();
		galleryContainer =				opts.frameID;
		galleryResponsive = 			opts.responsiveGallery;
		controlBarAdjust = 				opts.controlBarTopOffset;
		iFrameAdjust =					opts.iFrameHeightAdjust;

		// Check for Contradicting Default Sort Settings and Auto Correct
		defaultSortArrayAlbums.push(opts.defaultSortByAlbumTitle);
		defaultSortArrayAlbums.push(opts.defaultSortByNumberImages);
		defaultSortArrayAlbums.push(opts.defaultSortByDateCreated);
		defaultSortArrayAlbums.push(opts.defaultSortByDateUpdated);
		defaultSortArrayAlbums.push(opts.defaultSortByFacebookOrder);
		defaultSortArrayPhotos.push(opts.defaultPhotoSortAdded);
		defaultSortArrayPhotos.push(opts.defaultPhotoSortUpdated);
		defaultSortArrayPhotos.push(opts.defaultPhotoSortOrder);
		defaultSortArrayPhotos.push(opts.defaultPhotoSortID);
		var checkSortSettingsAlbums = 	defaultSortArrayAlbums.frequencies();
		var checkSortSettingsPhotos = 	defaultSortArrayPhotos.frequencies();
		if (checkSortSettingsAlbums[true] != 1) {
			opts.defaultSortByAlbumTitle = true;
			opts.defaultSortByNumberImages = false;
			opts.defaultSortByDateCreated = false;
			opts.defaultSortByDateUpdated = false;
			opts.defaultSortByFacebookOrder = false;
		};
		if (checkSortSettingsPhotos[true] != 1) {
			opts.defaultPhotoSortAdded = true;
			opts.defaultPhotoSortUpdated = false;
			opts.defaultPhotoSortOrder = false;
			opts.defaultPhotoSortID = false;
		};

		// Determine Screen, Scrollbar & Gallery Size
		GetScreenDimensions();
		scrollBarWidth = 				scrollBarWidth();
		if (isInIFrame) {
			opts.floatingControlBar = 	false;
			GetIFrameDimensions();
			galleryWidth = 				iFrameWidth;
		} else {
			galleryWidth =				Math.round((opts.responsiveGallery == true ? ((viewPortWidth - scrollBarWidth) * opts.responsiveWidth / 100) : opts.fixedWidth));
		}
		if (opts.consoleLogging) {
			console.log("Usable Screen Size Detection: Width = " + viewPortWidth + "px / Height = " + viewPortHeight + "px / Width of Scrollbar: " + scrollBarWidth + "px / Width of Gallery: " + galleryWidth + "px");
		};

		// Determine Default Sort Settings for Albums & Photos
		if (opts.defaultSortByAlbumTitle) {
			if ((!opts.albumAllowSortName)) {opts.albumAllowSortName = true;};
			defaultSortTypeAlbums = 'albumTitle';
		} else if (opts.defaultSortByNumberImages) {
			if (!opts.albumAllowSortItems) {opts.albumAllowSortItems = true;};
			defaultSortTypeAlbums = 'numberItems';
		} else if (opts.defaultSortByDateCreated) {
			if	(!opts.albumAllowSortCreated) {opts.albumAllowSortCreated = true;};
			defaultSortTypeAlbums = 'createDate';
		} else if (opts.defaultSortByDateUpdated) {
			if	(!opts.albumAllowSortUpdate) {opts.albumAllowSortUpdate = true;};
			defaultSortTypeAlbums = 'updateDate';
		} else if (opts.defaultSortByFacebookOrder) {
			if	(!opts.albumAllowSortFacebook) {opts.albumAllowSortFacebook = true;};
			defaultSortTypeAlbums = 'orderFacebook';
		} else if (opts.defaultSortByFacebookID) {
			if	(!opts.albumAllowSortID) {opts.albumAllowSortID = true;};
			defaultSortTypeAlbums = 'FacebookID';
		};
		if (opts.defaultPhotoSortAdded) {
			if (!opts.photoAllowSortAdded) {opts.photoAllowSortAdded = true;};
			defaultSortTypePhotos = 'addedDate';
		} else if (opts.defaultPhotoSortUpdated) {
			if (!opts.photoAllowSortUpdate) {opts.photoAllowSortUpdate = true;};
			defaultSortTypePhotos = 'updateDate';
		} else if (opts.defaultPhotoSortOrder) {
			if (!opts.photoAllowSortFacebook) {opts.photoAllowSortFacebook = true;};
			defaultSortTypePhotos = 'orderFacebook';
		} else if (opts.defaultPhotoSortID) {
			if (!opts.photoAllowSortID) {opts.photoAllowSortID = true;};
			defaultSortTypePhotos = 'FacebookID';
		};

		if ((!opts.showAlbumDescription) && (!opts.showBottomControlBar)) {
			opts.showFloatingReturnButton = true;
		}

		if ((opts.innerImageScaler) && (opts.PathInternalPHP.length == 0)) {opts.innerImageScaler = false;};

		// Check if Album Thumbnails should Match Photo Thumbnails and Adjust
		if (opts.matchAlbumPhotoThumbs) {
			opts.albumWrapperWidth = opts.photoThumbWidth;
			opts.albumThumbWidth = opts.photoThumbWidth;
			opts.albumThumbHeight = opts.photoThumbHeight;
			opts.albumWrapperMargin = opts.photoThumbMargin;
			opts.albumFrameOffset = 0;
			opts.albumThumbPadding = opts.photoThumbPadding;
			opts.albumShadowOffset = 0;
		} else {
			opts.albumThumbPadding = 0;
		};

		// Check which Lightbox Plugin should be Used
		if ((opts.colorBoxAllow) && (opts.fancyBoxAllow) && (opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		} else if ((!opts.colorBoxAllow) && (opts.fancyBoxAllow) && (opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		} else if ((opts.colorBoxAllow) && (!opts.fancyBoxAllow) && (opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		} else if ((opts.colorBoxAllow) && (opts.fancyBoxAllow) && (!opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		} else if ((opts.colorBoxAllow) && (!opts.fancyBoxAllow) && (opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		} else if ((!opts.colorBoxAllow) && (!opts.fancyBoxAllow) && (!opts.prettyPhotoAllow)) {
			opts.fancyBoxAllow = true;
			opts.colorBoxAllow = false;
			opts.prettyPhotoAllow = false;
		};

		if (opts.AlbumShareMePreText.substr(opts.AlbumShareMePreText.length - 1) == ":") {opts.AlbumShareMePreText = opts.AlbumShareMePreText.slice(0, -1);};
		if (opts.ImageShareMePreText.substr(opts.ImageShareMePreText.length - 1) == ":") {opts.ImageShareMePreText = opts.ImageShareMePreText.slice(0, -1);};

		// Initialize Album Gallery
		function galleryAlbumsInit() {
			$('#fb-album-header').html("");
			$('#fb-album-footer').html("");
			if ($('#fb-albums-all-paged').length != 0) {
				//alert("Restored from 'galleryAlbumsInit'");
				if ($("#paginationControls-" + opts.facebookID).length != 0) {
					if ((opts.floatingControlBar) && (!isInIFrame)) {
						$("#paginationControls-" + opts.facebookID).unbind('stickyScroll');
						$("#paginationControls-" + opts.facebookID).stickyScroll('reset');
					};
				};
				$("#" + opts.loaderID).slideFade(700);
				$('#fb-albums-all-paged').slideFade(700);
				var $container = $('#fb-albums-all');
				$container.isotope('reloadItems');
				$container.isotope('reLayout');
				if ((opts.floatingControlBar) && (!isInIFrame)) {
					isotopeHeightContainer = $container.height();
					if (!opts.paginationLayoutAlbums) {
						$("#paginationControls-" + opts.facebookID).stickyScroll({ container: $("#fb-albums-all-paged") })
					}
				}
				$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
			} else {
				$("<div>", {
					id : "fb-albums-all"
				}).appendTo("#fb-album-content");
				if (!opts.albumSortControls) {
					$("#fb-albums-all").css("padding-top", "10px");
				};
				galleryAlbumsShow();
			};
		};

		// Load and Show Album Gallery Thumbnails and Data
		function galleryAlbumsShow() {
			if (!opts.singleAlbumOnly) {
				if ((opts.maxNumberGalleries == 0) || (opts.showSelectionOnly)) {
					var graph = "https://graph.facebook.com/" + opts.facebookID + "/albums?fields=id,name,cover_photo,count,created_time,updated_time,description,link,type,location,place,from,privacy&limit=" + opts.maxGraphLimit;
				} else {
					var graphLimit = opts.excludeAlbums.length + opts.maxNumberGalleries + (opts.excludeTimeLine == true ? 1 : 0);
					var graph = "https://graph.facebook.com/" + opts.facebookID + "/albums?fields=id,name,cover_photo,count,created_time,updated_time,description,link,type,location,place,from,privacy&limit=" + graphLimit;
				}
				$.ajax({
					url: 			graph,
					cache: 			false,
					dataType: 		"jsonp",
					success: function(json) {
						$.each(json.data, function(k, albums){
							if (typeof albums.cover_photo !== "undefined") {
								if (typeof(albums.count) != "undefined") {
									if (opts.outputCountAlbumID) {
										alert(i + ": " + albums.id + " / " + albums.count);
									}
									if (((opts.showSelectionOnly) && ($.inArray(albums.id, opts.includeAlbums) > -1)) || ((!opts.showSelectionOnly) && ($.inArray(albums.id, opts.excludeAlbums) == -1))) {
										counterA = counterA + 1;
										if ((counterA <= opts.maxNumberGalleries) || (opts.maxNumberGalleries === 0) || ((opts.showSelectionOnly) && (counterA <= opts.includeAlbums.length))) {
											if ((albums.count > opts.maxNumberImages) && (opts.maxNumberImages != 0)) {
												var countTxt = opts.maxNumberImages + " ";
											} else {
												var countTxt = albums.count + " ";
											}
											// Convert ISO-8601 Dates into readable Format
											if (opts.albumDateCreate) {
												var timeStampA = new XDate((albums.created_time.length == 10 ? albums.created_time * 1000 : albums.created_time));
												if (opts.albumCreateFromNow) {
													timeStampA = moment(timeStampA).fromNow();
												} else {
													var timeStampA_Zone = Math.abs(timeStampA.clone().toString("z"));
													timeStampA = timeStampA.clone().addHours(timeStampA_Zone).toString("MM/dd/yyyy - hh:mm TT");
												}
											}
											if (opts.albumDateUpdate) {
												var timeStampB = new XDate((albums.updated_time.length == 10 ? albums.updated_time * 1000 : albums.updated_time));
												if (opts.albumUpdateFromNow) {
													timeStampB = moment(timeStampB).fromNow()
												} else {
													var timeStampB_Zone = Math.abs(timeStampB.clone().toString("z"));
													timeStampB = timeStampB.clone().addHours(timeStampB_Zone).toString("MM/dd/yyyy - hh:mm TT");
												}
											}
											if (this.count > 1) {
												countTxt += opts.MultiImagesWord;
											} else {
												countTxt += opts.SingleImageWord;
											}
											if ((this.count > opts.maxNumberImages) && (opts.maxNumberImages != 0)) {
												countTxt += " (" + opts.OutOfTotalImagesPreText + " " + albums.count + " " + opts.MultiImagesWord + ")";
											}
											if (!opts.matchAlbumPhotoThumbs) {
												var clear = 'width: ' + (opts.albumWrapperWidth + opts.albumFrameOffset * 2) + 'px; margin: ' + opts.albumWrapperMargin + 'px; display: none;';
											} else {
												var clear = 'width: ' + (opts.albumWrapperWidth + 10) + 'px; margin: ' + opts.albumWrapperMargin + 'px; display: none;';
											}
											if (opts.createTooltipsAlbums) {var tooltips = " TipGallery";} else {var tooltips = "";};

											var html = "";

											if ((opts.albumNameTitle) && (opts.albumNameAbove)) {html += '<div class="albumHead fbLink"><span class="albumNameHead" data-albumid="' + albums.id + '" ' + opts.tooltipTipAnchor + '="' + albums.name + '">' + albums.name + '</span></div>';}

											if (!opts.matchAlbumPhotoThumbs) {
												html += '<div id="' + albums.id + '" class="albumThumb fbLink' + tooltips + (opts.albumCCS3Shadow == true ? " ShadowCSS3" : "") + '" ' + opts.tooltipTipAnchor + '="' + albums.name + '" data-link="' + albums.link + '" style="width:' + (opts.albumThumbWidth) + 'px; height:' + (opts.albumThumbHeight) + 'px; padding: ' + opts.albumFrameOffset + 'px;" data-href="#album-' + albums.id + '">';
											} else {
												html += '<div id="' + albums.id + '" class="albumThumb fbLink' + tooltips + (opts.albumCCS3Shadow == true ? " ShadowCSS3" : "") + '" ' + opts.tooltipTipAnchor + '="' + albums.name + '" data-link="' + albums.link + '" style="width:' + (opts.albumThumbWidth) + 'px; height:' + (opts.albumThumbHeight) + 'px; padding: 5px;" data-href="#album-' + albums.id + '">';
											}
												if (!opts.matchAlbumPhotoThumbs) {
													if (opts.albumShowPaperClipL) 	{html += '<span class="PaperClipLeft"></span>';}
													if (opts.albumShowPaperClipR) 	{html += '<span class="PaperClipRight" style="left: ' + (opts.albumWrapperWidth - 30) + 'px;"></span>';}
													if (opts.albumShowPushPin) 		{html += '<span class="PushPin" style="left: ' + (Math.ceil(opts.albumWrapperWidth / 2)) + 'px;"></span>';}
													if (opts.albumShowShadow) {
														if ((!opts.albumShadowA) && (!opts.albumShadowB) && (!opts.albumShadowC)) {
															html += '<div class="fb-album-shadow1" style="top: ' + (opts.albumThumbHeight + opts.albumShadowOffset) + 'px;"></div>';
														} else if (opts.albumShadowA){
															html += '<div class="fb-album-shadow1" style="top: ' + (opts.albumThumbHeight + opts.albumShadowOffset) + 'px;"></div>';
														} else if (opts.albumShadowB){
															html += '<div class="fb-album-shadow2" style="top: ' + (opts.albumThumbHeight + opts.albumShadowOffset) + 'px;"></div>';
														} else if (opts.albumShadowC){
															html += '<div class="fb-album-shadow3" style="top: ' + (opts.albumThumbHeight + opts.albumShadowOffset) + 'px;"></div>';
														}
													}
												} else {
													if (opts.photoShowClearTape) 	{html += '<span class="ClearTape" style="left: ' + (Math.ceil((opts.albumThumbWidth + opts.albumWrapperMargin + opts.albumThumbPadding - 77) / 2)) + 'px;"></span>';}
													if (opts.photoShowYellowTape) 	{html += '<span class="YellowTape" style="left: ' + (Math.ceil((opts.albumThumbWidth + opts.albumWrapperMargin + opts.albumThumbPadding - 115) / 2)) + 'px;"></span>';}
													if (opts.photoShowPushPin) 		{html += '<span class="PushPin" style="left: ' + (Math.ceil((opts.albumThumbWidth + opts.albumWrapperMargin + opts.albumThumbPadding) / 2)) + 'px;"></span>';}
												}
												if (opts.albumFrameOffset == 0) 	{html += '<span id="Wrap_' + albums.id + '" style="border: none;" class="albumThumbWrap" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; padding: ' + opts.albumFrameOffset + 'px; left: ' + opts.albumFrameOffset + 'px; top: ' + opts.albumFrameOffset + 'px;">';
												} else 								{html += '<span id="Wrap_' + albums.id + '" class="albumThumbWrap" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; padding: ' + opts.albumFrameOffset + 'px; left: ' + opts.albumFrameOffset + 'px; top: ' + opts.albumFrameOffset + 'px;">';}
												//html += '<i class="fb-album-loading" id="fb-album-loading-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; padding: ' + opts.albumFrameOffset + 'px;"></i>';
												if (!opts.matchAlbumPhotoThumbs) 	{html += '<i class="fb-album-spinner" id="fb-album-spinner-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px;"></i>';
												} else 								{html += '<i class="fb-album-spinner" id="fb-album-spinner-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; top: 0px; left: 0px;"></i>';}
												html += '<i class="fb-album-thumb" id="fb-album-thumb-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px;"></i>';
												if (!opts.matchAlbumPhotoThumbs) 	{html += '<i class="fb-album-overlay" id="fb-album-overlay-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; padding: ' + opts.albumFrameOffset + 'px;"></i>';
												} else 								{html += '<i class="fb-album-overlay" id="fb-album-overlay-' + albums.cover_photo + '" style="width:' + opts.albumThumbWidth + 'px; height:' + opts.albumThumbHeight + 'px; padding: ' + opts.albumThumbPadding + 'px; left: -' + opts.albumThumbPadding + 'px; top: -' + opts.albumThumbPadding + 'px; "></i>';}
												html += '</span>';
											html += '</div>';

											html += '<div class="albumDetails" ' + opts.tooltipTipAnchor + '="' + albums.id + '" style="width:' + (opts.albumWrapperWidth + 2 * opts.albumThumbPadding) + 'px; padding-top: ' + ((opts.albumShowShadow == true ? opts.albumShadowOffset : 0) + opts.albumInfoOffset) + 'px;">';
												if (opts.albumShowSocialShare) {
													html += '<div class="albumShare clearfix" style="width: ' + (opts.albumWrapperWidth + opts.albumFrameOffset + 2 * opts.albumThumbPadding) + 'px;">';
														if (opts.albumShowOrder) {
															html += '<span class="albumSocial">' + opts.AlbumShareMePreText + ' (' + counterA + '):</span>';
														} else {
															html += '<span class="albumSocial">' + opts.AlbumShareMePreText + ':</span>';
														}
														html += '<ul style="float: right;" class="socialcount" data-url="' + this.link + '" data-share-text="Share this Album ..." data-counts="true">';
															html += '<li class="googleplus"><a class="TipGallery" target="_blank" href="https://plus.google.com/share?url=' + albums.link + '" ' + opts.tooltipTipAnchor + '="Share Album ' + albums.name + ' on Google Plus"><span class="social-icon icon-googleplus"></span></a></li>';
															html += '<li class="twitter"><a class="TipGallery" target="_blank" href="https://twitter.com/intent/tweet?text=' + albums.link + '" ' + opts.tooltipTipAnchor + '="Share Album ' + albums.name + ' on Twitter"><span class="social-icon icon-twitter"></span></a></li>';
															html += '<li class="facebook"><a class="TipGallery" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' + albums.link + '" ' + opts.tooltipTipAnchor + '="Share Album ' + albums.name + ' on Facebook"><span class="social-icon icon-facebook"></span></a></li>';
														html += '</ul>';
													html += '</div>';
												}
												html += '<div class="albumText">';
													if ((opts.albumNameTitle) && (!opts.albumNameAbove)) 	{html += '<div class="fbLink" style="width: ' + opts.albumWrapperWidth + 'px;"><span class="albumName" data-albumid="' + albums.id + '" ' + opts.tooltipTipAnchor + '="' + albums.name + '">' + albums.name + '</span></div>';}
													if (opts.albumImageCount) 	{html += '<div class="clearfix" style="width: ' + opts.albumWrapperWidth + 'px; display: block;"><div class="albumCount">' + opts.AlbumContentPreText + '</div><div class="albumInfo clearfix">' + countTxt + '</div></div>';}
													if (opts.albumDateCreate) 	{html += '<div class="clearfix" style="width: ' + opts.albumWrapperWidth + 'px; display: block;"><div class="albumCreate">' + opts.AlbumCreatedPreText + '</div><div class="albumInfo clearfix">' + timeStampA + '</div></div>';}
													if (opts.albumDateUpdate) 	{html += '<div class="clearfix" style="width: ' + opts.albumWrapperWidth + 'px; display: block;"><div class="albumUpdate">' + opts.AlbumUpdatedPreText + '</div><div class="albumInfo clearfix">' + timeStampB + '</div></div>';}
													if (opts.albumFacebookID) 	{html += '<div class="clearfix" style="width: ' + opts.albumWrapperWidth + 'px; display: block;"><div class="albumUpdate">' + opts.AlbumNumericIDPreText + '</div><div class="albumInfo clearfix">' + this.id + '</div></div>';}
												html += '</div>';
											html += '</div>';

											if (((counterA <= opts.maxNumberGalleries) && (opts.maxNumberGalleries > 0)) || (opts.maxNumberGalleries === 0)) {
												var coverType = albums.type;
												if (opts.useAlbumsUpdated) {
													var albumTime = moment(albums.updated_time).fromNow().replace(/ /g,"_");
													var albumUTC = (albums.updated_time.length == 10 ? albums.updated_time * 1000 : albums.updated_time);
												} else {
													var albumTime = moment(albums.created_time).fromNow().replace(/ /g,"_");
													var albumUTC = (albums.created_time.length == 10 ? albums.created_time * 1000 : albums.created_time);
												}
												if (((!opts.excludeTimeLine) && (coverType == "wall")) || (coverType != "wall")) {
													$("<div>", {
														"class": 		"albumWrapper " + albumTime,
														"id": 			"coverWrapper-" + albums.cover_photo,
														"data-title":	albums.name,
														"data-cover":	albums.cover_photo,
														"data-create":	(albums.created_time.length == 10 ? albums.created_time * 1000 : albums.created_time),
														"data-update":	(albums.updated_time.length == 10 ? albums.updated_time * 1000 : albums.updated_time),
														"data-count":	albums.count,
														"data-number":	albums.id,
														"data-order":	counterA,
														"data-time":	albumTime,
														"data-UTC":		albumUTC,
														"data-id":		albums.id,
														"data-type":	coverType,
														style: 			clear,
														html : 			html
													}).appendTo("#fb-albums-all").fadeIn(500, function(){});
													$('body').on('click', '#' + albums.id, function(e){
														$(this).find(".fb-album-overlay").stop().animate({opacity: 0}, "slow");
														//$(this).find(".fb-album-loading").stop().animate({opacity: .5}, "slow");
														checkExisting($(this).attr('data-href'));
													});
													var coverCount = counterA - 1;
													var coverAlbum = "";
													var coverID = "";
													if (opts.consoleLogging) {
														if (albums.count > 1) {
															if ((opts.maxNumberImages > 0) && (albums.count > opts.maxNumberImages)) {
																var coverContent = opts.maxNumberImages + " out of " + albums.count + " Images";
															} else {
																var coverContent = albums.count + " Images";
															}
														} else {
															if ((opts.maxNumberImages > 0) && (albums.count > opts.maxNumberImages)) {
																var coverContent = opts.maxNumberImages + " out of " + albums.count + " Image";
															} else {
																var coverContent = albums.count + " Image";
															}
														}
													};
													if (coverType == "wall") {
														var cover = "https://graph.facebook.com/" + opts.facebookID + "?fields=cover";
														$.ajax({
															url: 			cover,
															cache: 			false,
															dataType: 		"jsonp",
															success: function(data){
																$.each([data], function(i, item){
																	coverAlbum = "Timeline";
																	coverID = item.cover.cover_id;
																	coverCount++;
																	if (opts.innerImageScaler) {
																		var imgcover = '' + opts.PathInternalPHP + '?src=' + (item.cover.source) + '&w=' + (opts.albumThumbWidth) + '&zc=1';
																	} else {
																		var imgcover = 'http://src.sencha.io/' + (opts.albumThumbWidth) + '/' + (item.cover.source);
																	}
																	var fileExtension = item.cover.source.substring(item.cover.source.lastIndexOf('.') + 1).toUpperCase();
																	$("#coverWrapper-" + albums.cover_photo).attr("data-type", fileExtension).addClass(fileExtension);
																	if (opts.imageLazyLoad) {
																		$("#fb-album-thumb-" + albums.cover_photo).attr("data-original", imgcover).attr("data-album", albums.cover_photo).attr("data-loaded", "FALSE");
																	} else {
																		$("#fb-album-thumb-" + albums.cover_photo).attr("data-album", albums.cover_photo).attr("data-loaded", "FALSE");
																		$("#fb-album-thumb-" + albums.cover_photo).hide().css("background-image", "url(" + (imgcover) + ")");
																		$("#fb-album-thumb-" + albums.cover_photo).waitForImages({
																			waitForAll: 	true,
																			finished: 		function() {},
																			each: 			function() {
																				var $album 	= $(this).attr('data-album');
																				if ($("#fb-album-thumb-" + $album).attr("data-loaded") == "FALSE") {
																					$("#fb-album-spinner-" + $album).hide();
																					$("#fb-album-thumb-" + $album).fadeIn(500);
																					$("#fb-album-thumb-" + $album).attr("data-loaded", "TRUE");
																				};
																			},
																		});
																	}
																});
																if (opts.consoleLogging) {
																	console.log('Update: Cover Photo (' + coverID + ') link for album #' + coverCount + '(' + coverAlbum + ' / ' + coverContent + ') could be successfully retrieved!');
																}
															},
															error: function(jqXHR, textStatus, errorThrown){
																console.log('Error: \njqXHR:' + jqXHR + '\ntextStatus: ' + textStatus + '\nerrorThrown: '  + errorThrown);
															}
														});
													} else {
														var cover = "https://graph.facebook.com/" + albums.cover_photo + "?fields=album,from,created_time,height,width,source,name,link,picture";
														$.ajax({
															url: 			cover,
															cache: 			false,
															dataType: 		"jsonp",
															success: function(data){
																$.each([data], function(i, item){
																	coverAlbum = albums.id;
																	coverID = item.id;
																	coverCount++;
																	if (typeof(coverID)  === "undefined") {
																		var pathname = getAbsolutePath();
																		pathname += opts.PathNoCoverImage;
																		if (opts.innerImageScaler) {
																			var imgcover = '' + opts.PathInternalPHP + '?src=' + (pathname) + '&w=' + (opts.albumThumbWidth) + '&zc=1';
																		} else {
																			var imgcover = 'http://src.sencha.io/' + (opts.albumThumbWidth) + '/' + (pathname);
																		}
																	} else {
																		if (opts.innerImageScaler) {
																			var imgcover = '' + opts.PathInternalPHP + '?src=' + (item.source) + '&w=' + (opts.albumThumbWidth) + '&zc=1';
																		} else {
																			var imgcover = 'http://src.sencha.io/' + (opts.albumThumbWidth) + '/' + (item.source);
																		}
																	}
																	if (typeof(item.source)  === "undefined") {
																		var fileExtension = "N/A";
																	} else {
																		var fileExtension = item.source.substring(item.source.lastIndexOf('.') + 1).toUpperCase();
																	}
																	$("#coverWrapper-" + albums.cover_photo).attr("data-type", fileExtension).addClass(fileExtension);
																	if (opts.imageLazyLoad) {
																		$("#fb-album-thumb-" + albums.cover_photo).attr("data-original", imgcover).attr("data-album", albums.cover_photo).attr("data-loaded", "FALSE");
																	} else {
																		$("#fb-album-thumb-" + albums.cover_photo).attr("data-album", albums.cover_photo).attr("data-loaded", "FALSE");
																		$("#fb-album-thumb-" + albums.cover_photo).hide().css("background-image", "url(" + (imgcover) + ")");
																		$("#fb-album-thumb-" + albums.cover_photo).waitForImages({
																			waitForAll: 	true,
																			finished: 		function() {},
																			each: 			function() {
																				var $album 	= $(this).attr('data-album');
																				if ($("#fb-album-thumb-" + $album).attr("data-loaded") == "FALSE") {
																					$("#fb-album-spinner-" + $album).hide();
																					$("#fb-album-thumb-" + $album).fadeIn(500);
																					$("#fb-album-thumb-" + $album).attr("data-loaded", "TRUE");
																				};
																			},
																		});
																	}
																});
																if (opts.consoleLogging) {
																	console.log('Update: Cover Photo (' + coverID + ') link for album #' + coverCount + '(' + coverAlbum + ' / ' + coverContent + ') could be successfully retrieved!');
																}
															},
															error: function(jqXHR, textStatus, errorThrown){
																console.log('Error: \njqXHR:' + jqXHR + '\ntextStatus: ' + textStatus + '\nerrorThrown: '  + errorThrown);
															}
														});
													}
												} else {
													counterA = counterA - 1;
												}
											}
										}
									} else {}
								}
							}
						});
						if (opts.consoleLogging) {
							console.log('Update: Data for ' + counterA + ' album(s) for Facebook ID "' + opts.facebookID + '" could be successfully retrieved!');
						}
						var $container = $('#fb-albums-all');
						$("#" + opts.loaderID).slideFade(700);
						$("#FB_Album_Display").slideFade(700);
						// Initialize Paging Feature
						equalHeightFloat(true, opts.facebookID);
						currentPageList = $container;
						setTimeout(function(){
							if (!opts.paginationLayoutAlbums) {
								var albumItemsPerPage = counterA;
							} else if (opts.smartAlbumsPerPageAllow) {
								var albumItemsPerPage = smartAlbumsPerPage;
							} else {
								if (opts.setAlbumsByPages) {
									var albumItemsPerPage = ((opts.paginationLayoutAlbums = true && opts.numberPagesForAlbums > 0) ? (Math.ceil(counterA / opts.numberPagesForAlbums)) : counterA);
								} else {
									var albumItemsPerPage = ((opts.paginationLayoutAlbums = true && opts.numberAlbumsPerPage > 0) ? opts.numberAlbumsPerPage : counterA);
								}
							}
							var AlbumSettings = {
								'searchBoxDefault' 		: 	opts.SearchDefaultText,
								'itemsPerPageDefault' 	: 	albumItemsPerPage,
								'hideToTop'				:	(opts.showFloatingToTopButton == true ? false : true),
								'hideFilter' 			: 	(opts.albumsFilterAllow == true ? false : true),
								'hideSort' 				: 	(opts.albumSortControls == true ? false : true),
								'hideSearch' 			: 	(opts.albumSearchControls == true ? false : true),
								'hidePager'				:	false
							};
							new CallPagination(currentPageList, AlbumSettings, "fb-albums-all-paged", true, true, opts.facebookID, totalItems);
						}, 500);
						// Initialize LazyLoad for Thumbnails
						if ((opts.imageLazyLoad) && ($.isFunction($.fn.lazyloadanything))) {
							$('.fb-album-thumb').lazyloadanything({
								'auto': 			true,
								'repeatLoad':		true,
								'onLoadingStart': 	function(e, LLobjs, indexes) {
									return true
								},
								'onLoad': 			function(e, LLobj) {
									var $img 	= LLobj.$element;
									var $src 	= $img.attr('data-original');
									var $album 	= $img.attr('data-album');
									if (($('#fb-albums-all-paged').is(':visible')) && $("#fb-album-thumb-" + $album).is(':visible')) {
										if ($("#fb-album-thumb-" + $album).attr("data-loaded") == "FALSE") {
											$img.hide().css('background-image', 'url("' + $src + '")');
											$img.waitForImages({
												waitForAll: 	true,
												finished: 		function() {},
												each: 			function() {
													if ($("#coverWrapper-" + $album).css('display') != 'none') {
														$("#fb-album-spinner-" + $album).hide();
														$("#fb-album-thumb-" + $album).fadeIn(500);
														$("#fb-album-thumb-" + $album).attr("data-loaded", "TRUE");
													};
												},
											});
										};
									};
								},
								'onLoadComplete':	function(e, LLobjs, indexes) {
									return true
								}
							});
							$.fn.lazyloadanything('load');
						} else {
							$('.albumWrapper .fb-album-thumb').waitForImages({
								waitForAll: true,
								finished: function() {},
								each: function() {
									var $album 	= $(this).attr("data-album");
									if ($("#fb-album-thumb-" + $album).attr("data-loaded") == "FALSE") {
										$("#fb-album-spinner-" + $album).hide();
										$("#fb-album-thumb-" + $album).hide().fadeIn(500);
										$("#fb-album-thumb-" + $album).attr("data-loaded", "TRUE");
									};
								},
							});
						}
						// Adjust Height of iFrame Container (if applicable)
						setTimeout(function(){
							if (isInIFrame) {
								var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
								var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
								var IFrameID = getIframeID(this);
								if (IFrameID != "N/A") {
									window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
									parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
									/*if (galleryResponsive) {
										window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
										parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
									}*/
								}
							}
						}, 100);
					},
					error: function(jqXHR, textStatus, errorThrown){
						if (opts.consoleLogging) {
							console.log('Error: \njqXHR:' + jqXHR + '\ntextStatus: ' + textStatus + '\nerrorThrown: '  + errorThrown);
						}
					}
				});
			} else {
				$("#" + opts.loaderID).show();
				singleAlbumInit();
			}
		};

		// Initialize Single Album Preview
		function singleAlbumInit() {
			$("#" + opts.loaderID).slideFade(700);
			if (opts.cacheAlbumContents) {
				if (($('#fb-album-paged-' + albumId).length != 0) || ($('#fb-album-' + albumId).length != 0)) {
					//alert("Restore from 'singleAlbumInit'");
					if ($("#paginationControls-" + albumId).length != 0) {
						if ((opts.floatingControlBar) && (!isInIFrame)) {
							$("#paginationControls-" + albumId).unbind('stickyScroll');
							$("#paginationControls-" + albumId).stickyScroll('reset');
						};
					};
					$('#fb-album-header').html(headerArray[albumId]);
					if (opts.showBottomControlBar) {
						$('#fb-album-footer').html(footerArray[albumId]);
					};
					$('#Back-' + albumId + '_1').unbind("click").bind('click', function(e){
						checkExisting($(this).attr('data-href'));
					});
					if (opts.showBottomControlBar) {
						$('#Back-' + albumId + '_2').unbind("click").bind('click', function(e){
							checkExisting($(this).attr('data-href'));
						});
						$('#Back_To_Top-' + albumId).click(function(e){
							$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
						});
					};
					if (opts.showFloatingReturnButton) {
						$('#Back-' + albumId + '_3').unbind("click").bind('click', function(e){
							checkExisting($(this).attr('data-href'));
						});
					};
					$('.paginationMain').hide();
					$("#" + opts.loaderID).slideFade(700);
					setTimeout(function(){
						$('#fb-album-paged-' + albumId).show();
						$('#fb-album-' + albumId).show();
						var $albumContainer = $('#fb-album-' + albumId);
						$albumContainer.isotope('reloadItems');
						$albumContainer.isotope('reLayout');
						if ((opts.floatingControlBar) && (!isInIFrame) && ($("#paginationControls-" + albumId).length != 0)) {
							isotopeHeightContainer = $albumContainer.height();
							$("#paginationControls-" + albumId).stickyScroll({ container: $("#fb-album-paged-" + albumId) });
						};
						$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
						if (opts.consoleLogging) {
							console.log('Update: All data for Album ' + albumId + ' has been restored from cache and set to visible!');
						}
					}, 800);
					return;
				}
			} else {
				removeAlbumDOM(albumId);
			}
			counterB = 0;
			var album = "https://graph.facebook.com/" + albumId + "?fields=description,count,cover_photo,id,link,location,name,place,from,created_time,updated_time,type";
			$.ajax({
				url: 			album,
				cache: 			false,
				dataType: 		"jsonp",
				success: function(data){
					$.each([data], function(i, item){
						var albname = item.name;
						var desc = "";
						if (item.description){desc += item.description;};
						if (item.location){
							if(desc != ""){desc += ' ';};
							desc += '[' + opts.ImageLocationPreText + ' ' + item.location + ']';
						}
						if ((desc!='') && (desc!=' ')){
							desc = '<p>' + desc + '</p>';
						} else {
							desc='<p>' + opts.AlbumNoDescription + '</p>';
						};
						if (!opts.singleAlbumOnly) {
							var headerID = 	'<div data-href="#" id="Back-' + albumId + '_1" class="BackButton fbLink clearfix">' + opts.AlbumBackButtonText + '</div>';
						} else {
							var headerID = 	'';
						};
						if (!opts.singleAlbumOnly) {
							var footerID =	'<div class="seperator clearfix" style="width: 100%; margin-top: 0px;"></div>';
							footerID += 		'<div data-href="#" id="Back-' + albumId + '_2" class="BackButton fbLink clearfix">' + opts.AlbumBackButtonText + '</div>';
							footerID += 		'<ul id="Top-' + albumId + '" class="TopButton fbLink clearfix"><li><a style="width: 40px;" id="Back_To_Top-' + albumId + '"><div id="To_Top_' + albumId + '" class="Album_To_Top"></div></a></li></ul>';
						} else {
							var footerID =	'<div class="seperator clearfix" style="width: 100%; margin-top: 0px;"></div>';
							footerID += 		'<ul id="Top-' + albumId + '" class="TopButton fbLink clearfix"><li><a style="width: 40px;" id="Back_To_Top-' + albumId + '"><div id="To_Top_' + albumId + '" class="Album_To_Top"></div></a></li></ul>';
						};
						if ((opts.photoShowIconFBLink) && (!opts.singleAlbumOnly)) {
							headerID +=		'<div id="Link-' + albumId + '" class="albumFacebook"><a href="' + item.link + '" target="_blank" style="text-decoration: none; border: 0px;"><div class="albumLinkSimple TipGallery" style="top: 25px;" ' + opts.tooltipTipAnchor + '="Click here to view the full Album on Facebook!"></div></a></div>';
						} else if ((opts.photoShowIconFBLink) && (opts.singleAlbumOnly)) {
							headerID +=		'<div id="Link-' + albumId + '" class="albumFacebook"><a href="' + item.link + '" target="_blank" style="text-decoration: none; border: 0px;"><div class="albumLinkSimple TipGallery" ' + opts.tooltipTipAnchor + '="Click here to view the full Album on Facebook!"></div></a></div>';
						};
						headerID += 			'<div class="albumTitle clearfix" style="' + (((opts.photoShowIconFBLink) && (opts.singleAlbumOnly)) ? "margin-top: -20px;" : "") + '">' + opts.AlbumTitlePreText + ' ' + albname + '</div>';
						if (opts.showAlbumDescription) {
							headerID += 		'<div class="albumDesc clearfix">' + desc + '</div>';
							if (opts.photoShowTextFBLink) {
								headerID += 		'<div class="albumLinkText clearfix"><a class="AlbumLinkButtonText" href="' + item.link + '" target="_blank">' + opts.AlbumLinkButtonText + '</a></div>';
							}
						};
						headerID +=			'<div class="seperator clearfix' + ((opts.floatingControlBar == true && !isInIFrame) ? " Floater" : "") + '" style="width: 100%; ' + (opts.floatingControlBar == false ? "margin-bottom: 0px;" : "") + '"></div>';
						headerArray[albumId] = headerID;
						footerArray[albumId] = footerID;
						$('#fb-album-header').html(headerID).hide();
						if (opts.showBottomControlBar) {
							$('#fb-album-footer').html(footerID).hide();
						};
						$("<div>", {
							id: 		'fb-album-' + albumId,
							"class": 	'album'
						}).appendTo("#fb-album-content").hide();
						albumCount = item.count;
					});
					singleAlbumShow(albumCount);
					if (!opts.singleAlbumOnly) {
						$('#Back-' + albumId + '_1').unbind("click").bind('click', function(e){
							if (!opts.cacheAlbumContents) {
								removeAlbumDOM(albumId);
							}
							checkExisting($(this).attr('data-href'));
						});
						$('#Back-' + albumId + '_2').unbind("click").bind('click', function(e){
							if (!opts.cacheAlbumContents) {
								removeAlbumDOM(albumId);
							}
							checkExisting($(this).attr('data-href'));
						});
					}
					$('#Back_To_Top-' + albumId).click(function(e){
						$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
					});
				},
				error: function(jqXHR, textStatus, errorThrown){
					if (opts.consoleLogging) {
						console.log('Error: \njqXHR:' + jqXHR + '\ntextStatus: ' + textStatus + '\nerrorThrown: '  + errorThrown);
					}
				}
			});
		}

		// Load and Show Single Album Thumbnails and Data
		function singleAlbumShow(albumCount) {
			var graphLimit = opts.excludeImages.length + opts.maxNumberImages;
			var pictures = "https://graph.facebook.com/" + albumId + "/photos?fields=id,name,picture,created_time,updated_time,source,height,width,album,link,images&limit=" + albumCount;
			$.ajax({
				url: 			pictures,
				cache: 			false,
				dataType: 		"jsonp",
				success: function(json) {
					$.each(json.data, function(j, photos){
						if (typeof photos.picture !== "undefined") {
							if($.inArray(photos.id, opts.excludeImages) == -1) {
								//alert(photos.id);
								counterB = counterB + 1;
								if ((counterB <= opts.maxNumberImages) || (opts.maxNumberImages == 0)) {
									var name = "";
									if (photos.name) {name = photos.name;}
									if (opts.createTooltipsPhotos) {
										var tooltips = " TipPhoto";
									} else {
										var tooltips = "";
									};
									if (opts.prettyPhotoAllow) {
										if (opts.tooltipTipAnchor != "title") {
											var html = '<a id="' + albumId + '_' + counterB + '" class="photoThumb prettyPhoto ' + albumId + tooltips + '" data-key="' + counterB + '" rel="prettyPhoto[' + albumId + ']" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px; padding:' + opts.photoThumbPadding + 'px;" ' + opts.tooltipTipAnchor + '="' + name + '" title="' + name + '" href="' + photos.source + '" target="_blank">';
										} else {
											var html = '<a id="' + albumId + '_' + counterB + '" class="photoThumb prettyPhoto ' + albumId + tooltips + '" data-key="' + counterB + '" rel="prettyPhoto[' + albumId + ']" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px; padding:' + opts.photoThumbPadding + 'px;" ' + opts.tooltipTipAnchor + '="' + name + '" href="' + photos.source + '" target="_blank">';
										};
									} else {
										var html = '<a id="' + albumId + '_' + counterB + '" class="photoThumb ' + albumId + tooltips + '" data-key="' + counterB + '" rel=' + albumId + ' style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px; padding:' + opts.photoThumbPadding + 'px;" ' + opts.tooltipTipAnchor + '="' + name + '" href="' + photos.source + '" target="_blank">';
									};
										if (opts.photoShowClearTape) {
											html += '<span class="ClearTape" style="left: ' + (Math.ceil((opts.photoThumbWidth + opts.photoThumbMargin + opts.photoThumbPadding - 77) / 2)) + 'px;"></span>';
										};
										if (opts.photoShowYellowTape) {
											html += '<span class="YellowTape" style="left: ' + (Math.ceil((opts.photoThumbWidth + opts.photoThumbMargin + opts.photoThumbPadding - 115) / 2)) + 'px;"></span>';
										};
										if (opts.photoShowPushPin) {
											html += '<span class="PushPin" style="left: ' + (Math.ceil((opts.photoThumbWidth + opts.photoThumbMargin + opts.photoThumbPadding) / 2)) + 'px;"></span>';
										};
									html += '<span class="photoThumbWrap">';

									if (opts.innerImageScaler) {
										var imgthumb = '' + opts.PathInternalPHP + '?src=' + (photos.source) + '&w=' + opts.photoThumbWidth + '&zc=1';
									} else {
										var imgthumb = 'http://src.sencha.io/' + opts.photoThumbWidth + '/' + (photos.source);
									};

									html += '<i class="fb-photo-spinner fb-photo-spinner-' + albumId + '" id="fb-photo-spinner-' + photos.id + '" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px;  padding:' + opts.photoThumbPadding + 'px;"></i>';

									if (opts.imageLazyLoad) {
										html += '<i class="fb-photo-thumb fb-photo-thumb-' + albumId + '" id="fb-photo-thumb-' + photos.id + '" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px;" data-loaded="FALSE" data-original="' + imgthumb + '" data-album="' + albumId + '" data-photo="' + photos.id + '"></i>';
									} else {
										html += '<i class="fb-photo-thumb fb-photo-thumb-' + albumId + '" id="fb-photo-thumb-' + photos.id + '" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px; background-image:url(' + (imgthumb) + ')" data-loaded="FALSE" data-album="' + albumId + '" data-photo="' + photos.id + '"></i>';
									};

									html += '<i class="fb-photo-overlay fb-photo-overlay-' + albumId + '" id="fb-photo-overlay-' + photos.id + '" style="width:' + opts.photoThumbWidth + 'px; height:' + opts.photoThumbHeight + 'px;  padding:' + opts.photoThumbPadding + 'px;"></i>';
									html += '</span>';
									html += '</a>';

									html += '<div class="photoDetails" ' + opts.tooltipTipAnchor + '="' + photos.id + '" style="width: ' + (opts.photoThumbWidth + opts.photoThumbMargin) + 'px;">';
										if (opts.photoShowSocialShare) {
											html += '<div class="photoShare clearfix" style="width: ' + (opts.photoThumbWidth + opts.photoThumbMargin) + 'px; ' + (!opts.photoShowNumber ? "border-bottom: none;" : "") + '">';
												if (opts.photoShowOrder) {
													html += '<span class="photoSocial">' + opts.ImageShareMePreText + ' (' + counterB + '):</span>';
												} else {
													html += '<span class="photoSocial">' + opts.ImageShareMePreText + ':</span>';
												};
												html += '<ul style="float: right;" class="socialcount" data-url="' + photos.source + '" data-share-text="Share this Album ...">';
													html += '<li class="googleplus"><a class="Share_Google TipGallery" target="_blank" href="https://plus.google.com/share?url=' + photos.source + '" ' + opts.tooltipTipAnchor + '="Share Image ' + photos.id + ' on Google Plus"><span class="social-icon icon-googleplus"></span></a></li>';
													html += '<li class="twitter"><a class="Share_Twitter TipGallery" target="_blank" href="https://twitter.com/intent/tweet?text=' + photos.source + '" ' + opts.tooltipTipAnchor + '="Share Image ' + photos.id + ' on Twitter"><span class="social-icon icon-twitter"></span></a></li>';
													html += '<li class="facebook"><a class="Share_Facebook TipGallery" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' + photos.source + '" ' + opts.tooltipTipAnchor + '="Share Image ' + photos.id + ' on Facebook"><span class="social-icon icon-facebook"></span></a></li>';
													html += '<li class="savetodisk"><a class="Share_Save TipGallery" target="_blank" href="' + photos.source + '" ' + opts.tooltipTipAnchor + '="Save Image ' + photos.id + ' to disk" data-original="' + photos.source + '"><span class="social-icon icon-disk"></span></a></li>';
												html += '</ul>';
											html += '</div>';
										}
										if (opts.photoShowNumber) {
											html += '<div class="photoID" style="width:' + opts.photoThumbWidth + 'px; margin-bottom:' + opts.photoThumbMargin + 'px;">' + opts.ImageNumberPreText + ' ' + photos.id + '</div>';
										}
									html += '</div>';

									var clear = 'width: ' + (opts.photoThumbWidth + opts.photoThumbPadding * 2) + 'px; margin: ' + opts.photoThumbMargin + 'px; display: none;';
									if (((counterB <= albumCount) && (counterB <= opts.maxNumberImages)) || (opts.maxNumberImages == 0)) {
										if (typeof(photos.source)  === "undefined") {
											var fileExtension = "N/A";
										} else {
											var fileExtension = photos.source.substring(photos.source.lastIndexOf('.') + 1).toUpperCase();
										}
										var photoTime = moment(photos.created_time).fromNow().replace(/ /g,"_");
										$("<div>", {
											"class": 			"photoWrapper " + fileExtension + " " + photoTime,
											"id": 				'fb-photo-' + photos.id,
											"data-title":		'',
											"data-cover":		'',
											"data-added":		photos.created_time,
											"data-update":		photos.updated_time,
											"data-height":		photos.height,
											"data-width":		photos.width,
											"data-number":		photos.id,
											"data-order":		counterB,
											"data-type":		fileExtension,
											"data-time":		photoTime,
											"data-ID":			photos.id,
											"data-location":	'',
											"style":			clear,
											html: 				html
										}).appendTo('#fb-album-' + albumId).show();
									};
								};
							};
						};
					});
					if (opts.consoleLogging) {
						console.log('Update: ' + counterB + ' Photo(s) for album ID "' + albumId + '" could be successfully retrieved!');
					}
					if ($('#fb-album-' + albumId + ' > .photoWrapper').length == 0) {
						$('#fb-album-' + albumId).wrap('<div id="fb-album-paged-' + albumId + '" class="paginationMain" />');
						$("<div>", {
							"id": 				'no-fb-photos',
							html: 				"Sorry, there are either no public images in that album or all of the images have been exluded from being shown ... Please check your settings!"
						}).appendTo('#fb-album-' + albumId).fadeIn(500);
						// Remove Loader Animation and Show Album Content
						$("#" + opts.loaderID).hide();
						if (opts.showAlbumDescription) {
							$('#fb-album-header').show();
						} else {
							$('#fb-album-header').hide();
						};
						if (opts.showBottomControlBar) {
							$('#fb-album-footer').show();
						};
						//$('#fb-album-' + albumId).show();
						$("#fb-album-" + albumId).slideFade(700);
					} else {
						var $albumContainer = $('#fb-album-' + albumId);
						$("#" + opts.loaderID).hide();
						//$("#fb-album-paged" + albumId).slideFade(700);
						// Initialize Paging Feature
						equalHeightFloat(false, albumId);
						currentPageList = $albumContainer;
						setTimeout(function(){
							if (!opts.paginationLayoutPhotos) {
								var photoItemsPerPage = counterB;
							} else if (opts.smartPhotosPerPageAllow) {
								var photoItemsPerPage = smartPhotosPerPage;
							} else {
								if (opts.setPhotosByPages) {
									var photoItemsPerPage = ((opts.paginationLayoutPhotos = true && opts.numberPagesForPhotos > 0) ? (Math.ceil(counterB / opts.numberPagesForPhotos)) : counterB);
								} else {
									var photoItemsPerPage = ((opts.paginationLayoutPhotos = true && opts.numberPhotosPerPage > 0) ? opts.numberPhotosPerPage : counterB);
								};
							};
							var PhotoSettings = {
								'searchBoxDefault' 		: 	opts.SearchDefaultText,
								'itemsPerPageDefault' 	: 	photoItemsPerPage,
								'hideToTop'				:	(opts.showFloatingToTopButton == true ? false : true),
								'hideFilter' 			: 	(opts.photosFilterAllow == true ? false : true),
								'hideSort' 				: 	(opts.photoSortControls == true ? false : true),
								'hideSearch' 			: 	true,
								'hidePager'				:	false
							};
							new CallPagination($albumContainer, PhotoSettings, "fb-album-paged-" + albumId, false, false, albumId);
						}, 500);
						// Initialize LazyLoad for Thumbnails
						if ((opts.imageLazyLoad) && ($.isFunction($.fn.lazyloadanything))) {
							$('.fb-photo-thumb-' + albumId).lazyloadanything({
								'auto': 			true,
								'repeatLoad':		true,
								'onLoadingStart': 	function(e, LLobjs, indexes) {
									return true
								},
								'onLoad': 			function(e, LLobj) {
									var $img = LLobj.$element;
									var $src = $img.attr('data-original');
									var $lazy = $img.attr('data-photo');
									var $frame = $img.attr('data-album');
									if (($('#fb-album-paged-' + $frame).is(':visible')) && $('#fb-photo-' + $lazy).hasClass('Showing')) {
										if ($("#fb-photo-thumb-" + $lazy).attr("data-loaded") == "FALSE") {
											$img.hide().css('background-image', 'url("' + $src + '")');
											$img.waitForImages({
												waitForAll: 	true,
												finished: 		function() {},
												each: 			function() {
													if ($("#fb-photo-" + $lazy).css('display') != 'none') {
														$("#fb-photo-spinner-" + $lazy).hide();
														$("#fb-photo-thumb-" + $lazy).fadeIn(500);
														$("#fb-photo-thumb-" + $lazy).attr("data-loaded", "TRUE");
													};
												},
											});
										};
									};
								},
								'onLoadComplete':	function(e, LLobjs, indexes) {
									return true
								}
							});
							$.fn.lazyloadanything('load');
						} else {
							$('#fb-album-' + albumId + ' .fb-photo-thumb').waitForImages({
								waitForAll: true,
								finished: function() {},
								each: function() {
									var $picture 	= $(this).attr("data-photo");
									if ($("#fb-photo-thumb-" + $picture).attr("data-loaded") == "FALSE") {
										$("#fb-photo-spinner-" + $picture).hide();
										$("#fb-photo-thumb-" + $picture).hide().fadeIn(500);
										$("#fb-photo-thumb-" + $picture).attr("data-loaded", "TRUE");
									};
								},
							});
						}
						// Initialize fancyBox Plugin for Photo Thumbnails
						if ((opts.fancyBoxAllow) && ($.isFunction($.fn.fancybox))) {
							if (isInIFrame) {
								$('a.' + albumId).click(function(e) {
									e.preventDefault();
									$(this).trigger('stopRumble');
									$(this).stop().animate({opacity: 1}, "slow");
									var currentImage = $(this).attr('data-key') - 1;
									$(".fb-photo-overlay").css("display", "none");
									var galleryArray = [];
									$('a.' + albumId).each(function(index){
										if ($(this).attr('href') != "undefined") {
											galleryArray.push({
												href: 	$(this).attr('href'),
												title:  $(this).attr(opts.tooltipTipAnchor),
												id:		$(this).attr('id'),
												key:	$(this).attr('data-key'),
												rel:	$(this).attr('rel')
											});
										}
									});
									parent.$.fancybox(galleryArray, {
										//href:				this.href,
										padding: 			15,
										scrolling: 			'auto',
										autosize: 			true,
										fitToView: 			true,
										openEffect: 		'elastic',
										openEasing: 		'easeInBack',
										openSpeed: 			500,
										//closeBtn:			false,
										closeEffect: 		'elastic',
										closeEasing: 		'easeOutBack',
										closeSpeed : 		500,
										closeClick: 		true,
										arrows: 			true,
										nextClick: 			false,
										playSpeed:			8000,
										index:				currentImage,
										afterLoad: function(){
											this.title = (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										},
										beforeShow: function(){
											//this.title = (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										},
										afterShow: function(){
											//this.title = (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										},
										onUpdate: function(){
											//this.title = (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										},
										helpers:  {
											overlay : {
												speedIn  : 		300,
												speedOut : 		300,
												opacity  : 		0.8,
												css      : {
													cursor : 	'pointer'
												},
												closeClick: 	true
											},
											title : {
													type : 		'inside'
											},
											buttons	: {
												position: 		'top'
											},
											thumbs	: {
												width	: 		50,
												height	: 		50
											}
										}
									});
								});
							} else {
								$('a.' + albumId).fancybox({
									padding: 			15,
									scrolling: 			'auto',
									autosize: 			true,
									fitToView: 			true,
									openEffect: 		'elastic',
									openEasing: 		'easeInBack',
									openSpeed: 			500,
									//closeBtn:			false,
									closeEffect: 		'elastic',
									closeEasing: 		'easeOutBack',
									closeSpeed : 		500,
									closeClick: 		true,
									arrows: 			true,
									nextClick: 			false,
									playSpeed:			8000,
									afterLoad: function(){
										this.title = $(this.element).attr(opts.tooltipTipAnchor);
									},
									beforeShow: function(){
										this.title = $(this.element).attr(opts.tooltipTipAnchor);
									},
									afterShow: function(){
										this.title = $(this.element).attr(opts.tooltipTipAnchor);
									},
									onUpdate: function(){
										this.title = $(this.element).attr(opts.tooltipTipAnchor);
									},
									helpers:  {
										overlay : {
											speedIn  : 		300,
											speedOut : 		300,
											opacity  : 		0.8,
											css      : {
												cursor : 	'pointer'
											},
											closeClick: 	true
										},
										title : {
												type : 		'inside'
										},
										buttons	: {
											position: 		'top'
										},
										thumbs	: {
											width	: 		50,
											height	: 		50
										}
									}
								});
							}
						}
						// Initialize colorBox Plugin for Photo Thumbnails
						if ((opts.colorBoxAllow) && ($.isFunction($.fn.colorbox))) {
							if (isInIFrame) {
								$('a.' + albumId).click(function(e) {
									e.preventDefault();
									$(this).trigger('stopRumble');
									$(this).stop().animate({opacity: 1}, "slow");
									var currentImage = $(this).attr('data-key') - 1;
									$(".fb-photo-overlay").css("display", "none");
									var galleryArray = [];
									$('a.' + albumId).each(function(index){
										if ($(this).attr('href') != "undefined") {
											galleryArray.push({
												href: 	$(this).attr('href'),
												title:  $(this).attr(opts.tooltipTipAnchor),
												id:		$(this).attr('id'),
												key:	$(this).attr('data-key'),
												rel:	$(this).attr('rel')
											});
										}
									});
									if (parent.$("#FaceBookGalleryColorBox").length > 0) {
										parent.$("#FaceBookGalleryColorBox").empty();
										var $gallery = parent.$("#FaceBookGalleryColorBox");
									} else {
										var $gallery = parent.$('<div id="FaceBookGalleryColorBox">').hide().appendTo('body');
									}
									$.each(galleryArray, function(i){
										$('<a id="' + galleryArray[i].id + '" class="ColorBoxOutSource" rel="' + galleryArray[i].rel + '" data-key="' + galleryArray[i].key + '" href="' + galleryArray[i].href + '" title="' + galleryArray[i].title + '"></a>').appendTo($gallery);
									});
									$gallery.find('a.ColorBoxOutSource').colorbox({
										rel:			albumId,
										scalePhotos: 	true,
										returnFocus:	false,
										slideshow:		true,
										slideshowSpeed:	6000,
										slideshowAuto:	false,
										slideshowStart:	'<span id="cboxPlay"></span>',
										slideshowStop:	'<span id="cboxStop"></span>',
										current: 		'Image {current} of {total}',
										title: function(){
											var imageText = $(this).attr("title");
											var imageLink = $(this).href;
											var imageInfo = "";
											if (imageText.length != 0) {
												if (opts.createTooltipsLightbox) {
													imageInfo = '<div class="TipLightbox" style="cursor: pointer;" ' + opts.tooltipTipAnchor + '="' + imageText + '">' + imageText + '</div>';
													return imageInfo;
												} else {
													imageInfo = '<span>' + imageText + '</span>';
													return imageInfo;
												}
											} else {
												imageInfo = '<span>' + opts.colorBoxNoDescription + '</span>';
												return imageInfo;
											}
										}
									});
									parent.$('a#' + $(this).attr('id')).click();
								});
							} else {
								$('a.' + albumId).colorbox({
									rel: 			albumId,
									scalePhotos: 	true,
									returnFocus:	false,
									slideshow:		true,
									slideshowSpeed:	6000,
									slideshowAuto:	false,
									slideshowStart:	'<span id="cboxPlay"></span>',
									slideshowStop:	'<span id="cboxStop"></span>',
									current: 		'Image {current} of {total}',
									title: function(){
										var imageText = $(this).attr(opts.tooltipTipAnchor);
										var imageLink = $(this).attr('href');
										var imageInfo = "";
										if (imageText.length != 0) {
											if (opts.createTooltipsLightbox) {
												imageInfo = '<div class="TipLightbox" style="cursor: pointer;" ' + opts.tooltipTipAnchor + '="' + imageText + '">' + imageText + '</div>';
												return imageInfo;
											} else {
												imageInfo = '<span>' + imageText + '</span>';
												return imageInfo;
											}
										} else {
											imageInfo = '<span>' + opts.colorBoxNoDescription + '</span>';
											return imageInfo;
										}
									}
								});
							}
						}
						// Initialize prettyPhoto Plugin for Photo Thumbnails
						if ((opts.prettyPhotoAllow) && ($.isFunction($.fn.prettyPhoto))) {
							if (isInIFrame) {
								$('a.' + albumId).click(function(e) {
									e.preventDefault();
									$(this).trigger('stopRumble');
									$(this).stop().animate({opacity: 1}, "slow");
									var currentImage = $(this).attr('data-key') - 1;
									$(".fb-photo-overlay").css("display", "none");
									var galleryArray = [];
									$('a.' + albumId).each(function(index){
										if ($(this).attr('href') != "undefined") {
											galleryArray.push({
												href: 	$(this).attr('href'),
												title:  $(this).attr(opts.tooltipTipAnchor),
												id:		$(this).attr('id'),
												key:	$(this).attr('data-key'),
												rel:	$(this).attr('rel')
											});
										}
									});
									if (parent.$("#FaceBookGalleryPrettyPhoto").length > 0) {
										parent.$("#FaceBookGalleryPrettyPhoto").empty();
										var $gallery = parent.$("#FaceBookGalleryPrettyPhoto");
									} else {
										var $gallery = parent.$('<div id="FaceBookGalleryPrettyPhoto">').hide().appendTo('body');
									}
									$.each(galleryArray, function(i){
										$('<a id="' + galleryArray[i].id + '" class="prettyPhotoOutSource" rel="prettyPhoto[' + albumId + ']" data-key="' + galleryArray[i].key + '" href="' + galleryArray[i].href + '" title="' + galleryArray[i].title + '"></a>').appendTo($gallery);
									});
									$gallery.find('a.prettyPhotoOutSource[rel^="prettyPhoto"]').prettyPhoto({
										animation_speed: 			'fast', 			/* fast/slow/normal */
										slideshow: 					8000, 				/* false OR interval time in ms */
										autoplay_slideshow: 		false, 				/* true/false */
										opacity: 					0.80, 				/* Value between 0 and 1 */
										show_title: 				true, 				/* true/false */
										allow_resize: 				true, 				/* Resize the photos bigger than viewport. true/false */
										counter_separator_label: 	'/', 				/* The separator for the gallery counter 1 "of" 2 */
										theme: 						'facebook', 		/* light_rounded / dark_rounded / light_square / dark_square / facebook */
										horizontal_padding: 		20, 				/* The padding on each side of the picture */
										hideflash: 					false, 				/* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
										wmode: 						'opaque', 			/* Set the flash wmode attribute */
										autoplay: 					true, 				/* Automatically start videos: True/False */
										modal: 						false, 				/* If set to true, only the close button will close the window */
										deeplinking: 				false, 				/* Allow prettyPhoto to update the url to enable deeplinking. */
										overlay_gallery: 			true, 				/* If set to true, a gallery will overlay the fullscreen image on mouse over */
										keyboard_shortcuts: 		true, 				/* Set to false if you open forms inside prettyPhoto */
										changepicturecallback: 		function(){}, 		/* Called everytime an photos is shown/changed */
										callback: 					function(){}, 		/* Called when prettyPhoto is closed */
										ie6_fallback: 				false,
										custom_markup: 				'',
										social_tools: 				false
									});
									parent.$('a#' + $(this).attr('id')).click();
								});
							} else {
								$('a.' + albumId + '[rel^="prettyPhoto"]').prettyPhoto({
									animation_speed: 			'fast', 			/* fast/slow/normal */
									slideshow: 					8000, 				/* false OR interval time in ms */
									autoplay_slideshow: 		false, 				/* true/false */
									opacity: 					0.80, 				/* Value between 0 and 1 */
									show_title: 				true, 				/* true/false */
									allow_resize: 				true, 				/* Resize the photos bigger than viewport. true/false */
									counter_separator_label: 	'/', 				/* The separator for the gallery counter 1 "of" 2 */
									theme: 						'facebook', 		/* light_rounded / dark_rounded / light_square / dark_square / facebook */
									horizontal_padding: 		20, 				/* The padding on each side of the picture */
									hideflash: 					false, 				/* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
									wmode: 						'opaque', 			/* Set the flash wmode attribute */
									autoplay: 					true, 				/* Automatically start videos: True/False */
									modal: 						false, 				/* If set to true, only the close button will close the window */
									deeplinking: 				false, 				/* Allow prettyPhoto to update the url to enable deeplinking. */
									overlay_gallery: 			true, 				/* If set to true, a gallery will overlay the fullscreen image on mouse over */
									keyboard_shortcuts: 		true, 				/* Set to false if you open forms inside prettyPhoto */
									changepicturecallback: 		function(){}, 		/* Called everytime an photos is shown/changed */
									callback: 					function(){}, 		/* Called when prettyPhoto is closed */
									ie6_fallback: 				false,
									custom_markup: 				'',
									social_tools: 				false
								});
							}
						}
						// Remove Loader Animation and Show Album Content
						if (opts.singleAlbumOnly) {
							$("#" + opts.loaderID).hide();
							$("#" + opts.galleryID).show();
							$('#fb-album-header').show();
							$('#fb-album-footer').hide();
							$('#fb-album-' + albumId).show();
						} else {
							$("#" + opts.loaderID).hide();
							$('#fb-album-header').show();
							$('#fb-album-footer').show();
							$('#fb-album-' + albumId).show();
						}
					}
					// Adjust Height of iFrame Container (if applicable)
					setTimeout(function(){
						if (isInIFrame) {
							var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
							var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
							var IFrameID = getIframeID(this);
							if (IFrameID != "N/A") {
								window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
								parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
								/*if (galleryResponsive) {
									window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
									parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
								}*/
							}
						}
					}, 100);
				},
				error: function(jqXHR, textStatus, errorThrown){
					if (opts.consoleLogging) {
						console.log('Error: \njqXHR:' + jqXHR + '\ntextStatus: ' + textStatus + '\nerrorThrown: '  + errorThrown);
					}
				}
			});
		}

		// Check if alredy created Gallery or Album Preview can be restored
		function checkExisting(href) {
			if ((typeof href != 'undefined') && (href.length != 0)) {
				var anchor = href.split('-');
				if (anchor[0] == '#album') {
					// Hide Album Thumbnail Gallery
					if($('#fb-albums-all-paged').length != 0) {
						$('#fb-albums-all-paged').slideFade(700);
						if ((opts.floatingControlBar) && (!isInIFrame)) {
							$("#paginationControls-" + opts.facebookID).unbind('stickyScroll');
							$("#paginationControls-" + opts.facebookID).stickyScroll('reset');
						}
					}
					// Check if selected album has just been viewed or not
					if(albumId != anchor[1]){
						albumId = anchor[1];
						singleAlbumInit();
					} else {
						if (opts.cacheAlbumContents) {
							//alert("Restore from 'checkExisting'");
							$("#" + opts.loaderID).slideFade(700);
							if ($("#paginationControls-" + albumId).length != 0) {
								if ((opts.floatingControlBar) && (!isInIFrame)) {
									$("#paginationControls-" + albumId).unbind('stickyScroll');
									$("#paginationControls-" + albumId).stickyScroll('reset');
								};
							};
							$('#fb-album-header').html(headerArray[albumId]);
							if (opts.showBottomControlBar) {
								$('#fb-album-footer').html(footerArray[albumId]);
							};
							$('#Back-' + albumId + '_1').unbind("click").bind('click', function(e){
								checkExisting($(this).attr('data-href'));
							});
							if (opts.showBottomControlBar) {
								$('#Back-' + albumId + '_2').unbind("click").bind('click', function(e){
									checkExisting($(this).attr('data-href'));
								});
								$('#Back_To_Top-' + albumId).click(function(e){
									$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
								});
							};
							if (opts.showFloatingReturnButton) {
								$('#Back-' + albumId + '_3').unbind("click").bind('click', function(e){
									checkExisting($(this).attr('data-href'));
								});
							}
							$('.paginationMain').hide();
							$("#" + opts.loaderID).slideFade(700);
							setTimeout(function(){
								$('#fb-album-paged-' + albumId).show();
								$('#fb-album-' + albumId).show();
								var $albumContainer = $('#fb-album-' + albumId);
								$albumContainer.isotope('reloadItems');
								$albumContainer.isotope('reLayout');
								if ((opts.floatingControlBar) && (!isInIFrame) && ($("#paginationControls-" + albumId).length != 0)) {
									isotopeHeightContainer = $albumContainer.height();
									$("#paginationControls-" + albumId).stickyScroll({ container: $("#fb-album-paged-" + albumId) });
								};
								$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
								if (opts.consoleLogging) {
									console.log('Update: All data for Album ' + albumId + ' has been restored from cache and set to visible!');
								}
							}, 700);
						} else {
							singleAlbumInit();
						}
					}
				} else {
					$(".albumThumb").find(".fb-album-overlay").stop().animate({opacity: 0}, "slow");
					$(".albumThumb").find(".fb-album-loading").stop().animate({opacity: 0}, "slow");
					$('.paginationMain').hide();
					$("#" + opts.loaderID).slideFade(700);
					if ((opts.consoleLogging) && (opts.cacheAlbumContents)) {
						console.log('Update: All data for Album ' + albumId + ' has been cached and set to temporarily hidden!');
					}
					if ($("#paginationControls-" + opts.facebookID).length != 0) {
						if ((opts.floatingControlBar) && (!isInIFrame)) {
							$("#paginationControls-" + opts.facebookID).unbind('stickyScroll');
							$("#paginationControls-" + opts.facebookID).stickyScroll('reset');
						};
					};
					galleryAlbumsInit();
				}
				// Adjust Height of iFrame Container (if applicable)
				setTimeout(function(){
					if (isInIFrame) {
						var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
						var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
						var IFrameID = getIframeID(this);
						if (IFrameID != "N/A") {
							window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							/*if (galleryResponsive) {
								window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
								parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
							}*/
						}
					}
				}, 100);
			}
		}

		// Function to Check if Key Node exists in JSAON Feed
		function returnIfExist(property) {
			try {
				return property;
			} catch (err) {
				return null
			}
		}

		// Ensure that all Thumbnails have the same Height
		function equalHeightFloat(Gallery, Identity){
			var heightArray = new Array();
			var widthArray = new Array();
			if (opts.albumNameShorten) {
				if ((opts.albumNameTitle) && (!opts.albumNameAbove)) 	{$('.albumName').shorten({tail: '&nbsp;&nbsp;...'})};
				if ((opts.albumNameTitle) && (opts.albumNameAbove)) 	{$('.albumNameHead').shorten({tail: '&nbsp;&nbsp;...'})};
			}
			if (Gallery) {
				$("#fb-albums-all .albumWrapper").each( function(){
					heightArray.push($(this).find(".albumHead").outerHeight(true) + $(this).find(".albumThumb").outerHeight(true) + $(this).find(".albumDetails").outerHeight(true));
					widthArray.push($(this).outerWidth(true));
				});
			} else {
				$("#fb-album-" + Identity + " .photoWrapper").each( function(){
					heightArray.push(2 * opts.photoThumbMargin + $(this).outerHeight(true) + $(this).find(".photoThumb").outerHeight(true) + $(this).find(".photoDetails").outerHeight(true) + $(this).find(".photoShare").outerHeight(true));
					widthArray.push($(this).outerWidth(true));
				});
			};
			var maxHeight = Math.max.apply(Math, heightArray);
			var maxWidth = Math.max.apply(Math, widthArray);
			totalItems = 0;
			if (Gallery) {
				AlbumThumbHeight = maxHeight;
				AlbumThumbWidth = maxWidth;
				smartAlbumsPerPage = Math.floor(galleryWidth / maxWidth) * (Math.floor(viewPortHeight / maxHeight));
				$("#fb-albums-all .albumWrapper").each( function(){$(this).height(maxHeight + "px"); totalItems++});
			} else {
				PhotoThumbHeight = maxHeight;
				PhotoThumbWidth = maxWidth;
				smartPhotosPerPage = Math.floor(galleryWidth / maxWidth) * ((Math.floor(viewPortHeight / maxHeight) - ((opts.showTopPaginationBar == true && opts.showAlbumDescription == true) ? 1 : 0)));
				$("#fb-album-" + Identity + " .photoWrapper").each( function(){$(this).height(PhotoThumbHeight + "px"); totalItems++});
			};
		};

		// Determine Width for Scrollbar based on Browser Settings
		function scrollBarWidth() {
			var parent, child, width;
			if (width === undefined) {
				parent = $('<div style="width:50px; height:50px; overflow:auto; position:absolute; top:-200px; left:-200px;"><div/>').appendTo('body');
				child = parent.children();
				width = child.innerWidth() - child.height(99).innerWidth();
				parent.remove();
			}
			return width;
		};

		// Remove Album Data from DOM
		function removeAlbumDOM(albumId) {
			if (($('#fb-album-paged-' + albumId).length != 0) || ($('#fb-album-' + albumId).length != 0)) {
				var $albumContainer = $('#fb-album-' + albumId);
				$albumContainer.isotope('destroy');
				$('#Back-' + albumId + '_1').unbind("click");
				if (opts.showBottomControlBar) {
					$('#Back-' + albumId + '_2').unbind("click");
				};
				if (opts.showFloatingReturnButton) {
					$('#Back-' + albumId + '_3').unbind("click");
				};
				if (opts.showFloatingToTopButton) {
					$('#Scroll_To_Top-' + albumId).unbind("click");
				};
				if ((opts.floatingControlBar) && (!isInIFrame)) {
					$("#paginationControls-" + albumId).unbind('stickyScroll');
					$("#paginationControls-" + albumId).stickyScroll('reset');
				};
				$('#fb-album-' + albumId).remove();
				$('#fb-album-paged-' + albumId).remove();
				if (opts.consoleLogging) {
					console.log('Update: All data for Album ' + albumId + ' has been removed from DOM!');
				};
			};
		};

		// Initialize Pagination of Thumbnails
		function CallPagination(currentPageList, options, ID, Gallery, firstRun, Identifier, totalItems){
			var thisFileList 				= this;
			this.timeClassArray 			= new Array();
			TotalThumbs 					= 0;
			TotalPages 						= 0;
			TotalTypes 						= 0;
			TotalTimes						= 0;
			this.settings = {
				'searchBoxDefault' 		: 	"Search ...",
				'itemsPerPageDefault' 	: 	9,
				'hideToTop'				:	false,
				'hideFilter' 			: 	false,
				'hideSort' 				: 	false,
				'hideSearch' 			: 	false,
				'hidePager'				:	false
			};
			if ( options ) {
				$.extend( this.settings, options );
			};

			$('.' + (Gallery == true ? 'albumWrapper' : 'photoWrapper'), currentPageList).each(function(){
				var linkItem = $(this);
				var fileTime = linkItem.attr('data-time');
				var fileUTC = linkItem.attr('data-UTC');
				var added = false;
				TotalThumbs = TotalThumbs + 1;
				$.map(thisFileList.timeClassArray, function(elementOfArray, indexInArray) {
					if (thisFileList.timeClassArray[indexInArray].Time == fileTime) {
						added = true;
						//return false;
					};
				});
				if (!added) {
					TotalTimes = TotalTimes + 1;
					thisFileList.timeClassArray.push({
						Time: fileTime,
						UTC: fileUTC
					});
				}
				linkItem.addClass('paginationItem');
			});
			thisFileList.timeClassArray.sort(sortFilters('UTC'));

			currentPageList.wrap('<div id="' + ID + '" class="paginationMain" />');
			currentPageList.addClass('paginationFrame');
			this.hideToTopDef 				= this.settings.hideToTop;
			this.hideFilterDef 				= this.settings.hideFilter;
			this.hideSearchDef 				= this.settings.hideSearch;
			this.hideSortDef 				= this.settings.hideSort;
			this.hidePagerDef 				= this.settings.hidePager;
			this.PerPageItems 				= this.settings.itemsPerPageDefault;
			this.SearchBox 					= this.settings.searchBoxDefault;

			if ((opts.floatingControlBar == false) && (Gallery == true)) {
				var CSSAdjust = "padding-bottom: 0px;"
			} else if ((opts.floatingControlBar == false) || (Gallery == false)) {
				var CSSAdjust = "padding-bottom: 5px;"
			} else {
				var CSSAdjust = "";
			}

			this.paginationControls 		= $('<div id="paginationControls-' + Identifier +'" class="paginationControls' + ((opts.floatingControlBar == true && !isInIFrame) ? " Floater" : "") + '" style="' + CSSAdjust + '"></div>');
			this.paginationControls.append((this.hideToTopDef === false ? '<a id="Scroll_To_Top-' + Identifier +'" class="Scroll_To_Top float_right defaultPaginationStyle btn" style="display:none" ' + opts.tooltipTipAnchor + '="Click here to go back to the top of the Facebook gallery."><div id="To_Top-' + Identifier +'" class="To_Top"></div></a>' : ''));
			this.paginationControls.append((this.hideSortDef === false ? '<a href="#" id="showSortingBtn-' + Identifier +'" class="showSortingBtn float_right defaultPaginationStyle btn"><div class="AdjustSort">' + (Gallery == true ? opts.SortButtonTextAlbums : opts.SortButtonTextPhotos) + '</div></a>' : ''));
			this.paginationControls.append((((this.hideFilterDef === false) && (TotalTimes > 1)) ? '<a href="#" id="showFilterBtn-' + Identifier +'" class="showFilterBtn float_right defaultPaginationStyle btn"><div class="AdjustType">' + (Gallery == true ? opts.FilterButtonTextAlbums : opts.FilterButtonTextPhotos) + '</div></a>' : ''));
			this.paginationControls.append((this.hidePagerDef === false ? '<a href="#" id="showPagerBtn-' + Identifier +'" class="showPagerBtn float_right defaultPaginationStyle btn"><div class="AdjustPage">' + opts.PagesButtonText + '</div></a>' : ''));
			this.paginationControls.append((this.hideSearchDef === false ? '<div id="paginationSearch-' + Identifier +'" class="paginationSearch"><label style="display:none;">Search</label><input type="text" value="' + this.settings.searchBoxDefault + '" class="paginationSearchValue"><a class="paginationSearchGo btn defaultPaginationStyle"><div class="AdjustSearch">' + (Gallery == true ? opts.SearchButtonTextAlbums : opts.SearchButtonTextPhotos) + '</div></a><a class="clearSearch btn defaultPaginationStyle hidden"><div class="AdjustClear">Clear</div></a></div>' : '<div class="paginationSearch" style="height: 20px;"></div>'));
			this.paginationControls.append((((Gallery === false) && (!opts.singleAlbumOnly) && (opts.showFloatingReturnButton)) ? '<div id="Back-' + Identifier + '_3" class="BackButton fbLink clearfix" style="margin-top: 10px;" data-href="#">Back</div>' : ""));
			this.paginationControls.append(((opts.showThumbInfoInPageBar == true && (Gallery == true && opts.paginationLayoutAlbums == false) || (Gallery == false && opts.paginationLayoutPhotos == false)) ? '<span id="thumbnailStage-' + Identifier +'" class="thumbnailStage" style="' + (Gallery == false ? "margin: -25px 0px 0px 0px" : "") + '"> (Showing ' + (Gallery == true ? "Albums" : "Photos") + ' <span class="thumbnailStageA"></span> to <span class="thumbnailStageB"></span> out of <span class="thumbnailStageC"></span>)</span>' : ''));

			// Create and Define Filter List
			var filterList = '<div id="paginationFilters-' + Identifier +'" class="paginationFilters"><ul id="Filter-Selections-' + Identifier +'" class="Selections unstyled">';
			if ((Gallery == true && opts.albumsFilterAllEnabled == true) || (!Gallery == true && opts.photosFilterAllEnabled == true)) {
				var enableAllFilters = true;
			} else {
				var enableAllFilters = false;
			}
			for(var i = 0; i < thisFileList.timeClassArray.length; i++){
				if (thisFileList.timeClassArray[i].Time != "") {
					filterList += '<li data-UTC="' + thisFileList.timeClassArray[i].UTC + '"><a href="" data-filter-type="' + thisFileList.timeClassArray[i].Time + '" class="'+ thisFileList.timeClassArray[i].Time +'Filter ' +  thisFileList.timeClassArray[i].Time + ' ' + (enableAllFilters == true ? "showing" : "")  + ' FilterA">' + thisFileList.timeClassArray[i].Time.replace(/_/g, ' ') + '</a></li>';
				}
			};
			filterList += '</ul><p class="bar"><a href="#" class="Closer">Close</a></p></div>';
			this.paginationControls.append((((this.hideFilterDef === false) && ((TotalTypes > 1) || (TotalTimes > 1))) ? filterList : ''));

			// Create and Define Paging List
			TotalPages = Math.ceil(TotalThumbs / this.PerPageItems);
			if (this.hidePagerDef === false) {
				this.paginationContainer = $('<div id="paginationPagers-' + Identifier +'" class="paginationPagers"></div>');
				this.paginationControls.append(this.paginationContainer);
				this.paginationListing = $('<ul id="Pager-Selections-' + Identifier +'" class="Selections unstyled"></ul>');
				this.paginationContainer.append(this.paginationListing);
				for (var i = 1; i < TotalPages+1; i++){
					this.paginationListing.append('<li><a id="Page_' + i + '_' + Identifier + '" class="" href="" data-filter-type="Page ' + i + '">' + i +'</a></li>');
				};
				this.paginationContainer.append('<p class="bar"><a href="#" class="Closer">Close</a></p>');
			}

			// Create and Define Sorting List
			if (Gallery) {
				var initialSort = (opts.defaultSortDirectionASC == true ? "asc" : "dec");
				if (defaultSortTypeAlbums == 'albumTitle') {SortByName = initialSort} else {SortByName = ""};
				if (defaultSortTypeAlbums == 'numberItems') {SortBySize = initialSort} else {SortBySize = ""};
				if (defaultSortTypeAlbums == 'createDate') {SortByCreated = initialSort} else {SortByCreated = ""};
				if (defaultSortTypeAlbums == 'updateDate') {SortByUpdated = initialSort} else {SortByUpdated = ""};
				if (defaultSortTypeAlbums == 'orderFacebook') {SortByOrder = initialSort} else {SortByOrder = ""};
				if (defaultSortTypeAlbums == 'FacebookID') {SortByID = initialSort} else {SortByID = ""};
			} else {
				var initialSort = (opts.defaultPhotoDirectionsASC == true ? "asc" : "dec");
				if (defaultSortTypePhotos == 'addedDate') {SortByAdded = initialSort} else {SortByAdded = ""};
				if (defaultSortTypePhotos == 'updateDate') {SortByUpdated = initialSort} else {SortByUpdated = ""};
				if (defaultSortTypePhotos == 'orderFacebook') {SortByOrder = initialSort} else {SortByOrder = ""};
				if (defaultSortTypePhotos == 'FacebookID') {SortByID = initialSort} else {SortByID = ""};
			}

			if (this.hideSortDef === false) {
				this.paginationSorting = $('<div id="paginationSorting-' + Identifier +'" class="paginationSorting" style="' + (this.hideFilterDef === true ? 'right:0;' : '') + '">');
				this.paginationCriteria = $('<ul id="Sort-Selections-' + Identifier +'" class="Selections unstyled"></ul>');
				this.paginationCriteria.append(((TotalThumbs > 1 && Gallery && opts.albumAllowSortName) ? '<li><a id="SortByName" class="' + SortByName + '" href="" data-sort-type="bytitle" data-sort-direction="' + SortByName + '">' + opts.SortNameText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && Gallery && opts.albumAllowSortItems) ? '<li><a id="SortBySize" class="' + SortBySize + '" href="" data-sort-type="bysize" data-sort-direction="' + SortBySize + '">' + opts.SortItemsText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && Gallery && opts.albumAllowSortCreated) ? '<li><a id="SortByCreated" class="' + SortByCreated + '" href="" data-sort-type="bycreate" data-sort-direction="' + SortByCreated + '">' + opts.SortCreatedText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && !Gallery && opts.photoAllowSortAdded) ? '<li><a id="SortByAdded" class="' + SortByAdded + '" href="" data-sort-type="byadded" data-sort-direction="' + SortByAdded + '">' + opts.SortAddedText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && ((Gallery && opts.albumAllowSortUpdate) || (!Gallery && opts.photoAllowSortUpdate))) ? '<li><a id="SortByUpdated" class="' + SortByUpdated + '" href="" data-sort-type="byupdate" data-sort-direction="' + SortByUpdated + '">' + opts.SortUpdatedText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && ((Gallery && opts.albumAllowSortID) || (!Gallery && opts.photoAllowSortID))) ? '<li><a id="SortByID" class="' + SortByID + '" href="" data-sort-type="byID" data-sort-direction="' + SortByID + '">' + opts.SortIDText + '</a></li>' : ''));
				this.paginationCriteria.append(((TotalThumbs > 1 && ((Gallery && opts.albumAllowSortFacebook) || (!Gallery && opts.photoAllowSortFacebook))) ? '<li><a id="SortByOrder" class="' + SortByOrder + '" href="" data-sort-type="byorder" data-sort-direction="' + SortByOrder + '">' + opts.SortFacebookText + '</a></li>' : ''));
				this.paginationSorting.append(this.paginationCriteria);
				this.paginationSorting.append('<p class="bar"><a href="#" class="Closer">Close</a></p>');
				this.paginationControls.append(this.paginationSorting);
			}

			currentPageList.before('<div id="paginationBar-' + Identifier +'" class="paginationBar"></div>');
			if (opts.showTopPaginationBar) {
				$(".paginationBar", $("#" + ID)).append('<div id="paginationButtonsTop-' + Identifier +'" class="paginationButtons ControlsTop" style="' + (opts.floatingControlBar == false ? "padding-top: 15px;" : "") + '">'
														+ '<a href="#" class="pfl_first btn defaultPaginationStyle"><div id="FirstPage"></div></a>'
														+ '<a href="#" class="pfl_prev btn disabled defaultPaginationStyle"><div id="PrevPage"></div></a>'
														+ '<a href="#" class="pfl_last btn defaultPaginationStyle"><div id="LastPage"></div></a>'
														+ '<a href="#" class="pfl_next btn defaultPaginationStyle"><div id="NextPage"></div></a>'
														+ '<span class="pagingInfo" style="' + (opts.showThumbInfoInPageBar == false ? "margin-top: 5px;" : "") + '">Page <span class="currentPage"></span> of <span class="totalPages"></span></span>'
														+ (opts.showThumbInfoInPageBar == true ? '<span class="thumbnailStage"> (Showing ' + (Gallery == true ? "Albums" : "Photos") + ' <span class="thumbnailStageA"></span> to <span class="thumbnailStageB"></span> out of <span class="thumbnailStageC"></span>)</span>' : '')
														+ '</div>');
			}
			$(".paginationBar", $("#" + ID)).append(this.paginationControls);
			$(".paginationBar", $("#" + ID)).append('<div style="display:none" class="paginationMessage"><div id="ErrorMessage"></div><a class="btn defaultPaginationStyle" href="#">Show All Albums</a></div>');
			$(".paginationBar", $("#" + ID)).append('<div style="display:none" class="paginationEmpty"><span></span></div>');
			if (opts.showBottomPaginationBar) {
				currentPageList.after('<div id="paginationButtonsBottom-' + Identifier +'" class="paginationButtons ControlsBottom" style="' + (opts.floatingControlBar == false ? "padding-top: 15px;" : "") + '">'
														+ '<a href="#" class="pfl_first btn defaultPaginationStyle"><div id="FirstPage"></div></a>'
														+ '<a href="#" class="pfl_prev btn disabled defaultPaginationStyle"><div id="PrevPage"></div></a>'
														+ '<a href="#" class="pfl_last btn defaultPaginationStyle"><div id="LastPage"></div></a>'
														+ '<a href="#" class="pfl_next btn defaultPaginationStyle"><div id="NextPage"></div></a>'
														+ '<span class="pagingInfo" style="' + (opts.showThumbInfoInPageBar == false ? "margin-top: 5px;" : "") + '">Page <span class="currentPage"></span> of <span class="totalPages"></span></span>'
														+ (opts.showThumbInfoInPageBar == true ? '<span class="thumbnailStage"> (Showing ' + (Gallery == true ? "Albums" : "Photos") + ' <span class="thumbnailStageA"></span> to <span class="thumbnailStageB"></span> out of <span class="thumbnailStageC"></span>)</span>' : '')
														+ '</div>');
			}

			// Assign and Define Variables
			this.allThumbsContainer = 		$("#" + ID);
			this.messageBox = 				$('.paginationMessage', this.allThumbsContainer);
			this.messageText = 				$('span', this.messageBox);
			this.emptyThumbsBox = 			$('.paginationEmpty', this.allThumbsContainer);
			this.emptyThumbsText = 			$('span', this.emptyThumbsBox);
			this.filteredThumbs = 			$('.paginationItem', this.allThumbsContainer);
			this.allThumbs = 				$('.paginationItem', this.allThumbsContainer);
			this.fileContainer = 			$('.paginationFrame', this.allThumbsContainer);
			this.firstButton = 				$('.pfl_first', this.allThumbsContainer);
			this.lastButton = 				$('.pfl_last', this.allThumbsContainer);
			this.nextButton = 				$('.pfl_next', this.allThumbsContainer);
			this.prevButton = 				$('.pfl_prev', this.allThumbsContainer);
			this.pageAt = 					currentPageList.data('itemsperpage') !== undefined ? currentPageList.data('itemsperpage') : this.settings.itemsPerPageDefault;
			this.paginationContainer = 		$('.paginationButtons', this.allThumbsContainer);
			this.currentPageCounter = 		$('.currentPage', this.allThumbsContainer);
			this.totalPageCounter = 		$('.totalPages', this.allThumbsContainer);
			this.searchAndFilterContainer = this.paginationControls;
			this.showPagerBtn = 			$('.showPagerBtn', this.allThumbsContainer);
			this.pagerLinksContainer = 		$('.paginationPagers',this.allThumbsContainer);
			this.pagerLinks = 				$('li a', this.pagerLinksContainer);
			this.showFilterBtn = 			$('.showFilterBtn', this.allThumbsContainer);
			this.filterLinksContainer = 	$('.paginationFilters', this.allThumbsContainer);
			this.filterLinks = 				$('li a', this.filterLinksContainer);
			this.showSortingBtn = 			$('.showSortingBtn', this.allThumbsContainer);
			this.sortingLinksContainer = 	$('.paginationSorting', this.allThumbsContainer);
			this.sortingLinks = 			$('.paginationSorting li a', this.allThumbsContainer);
			this.searchBoxContainer = 		$('.paginationSearch', this.allThumbsContainer);
			this.searchBox = 				$('.paginationSearchValue', this.searchBoxContainer);
			this.searchButton = 			$('.paginationSearchGo', this.searchBoxContainer);
			this.clearSearchButton = 		$('.clearSearch', this.searchBoxContainer);
			this.currentPage = 				0;
			this.totalFiles = 				this.filteredThumbs.length;
			this.totalPages = 				(Math.ceil(this.totalFiles / this.pageAt));
			this.itemsLastPage = 			this.totalFiles - ((this.totalPages - 1) * this.PerPageItems);

			// Scroll To Top Click
			$('#Scroll_To_Top-' + Identifier).click(function(e){
				$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
			});
			// Paging Click Events
			if(this.pageAt < this.totalFiles){
				this.nextButton.click(function(event) {
					event.preventDefault();
					if($(this).hasClass('disabled')){return false;}
					//currentPageList.isotope('destroy');
					thisFileList.currentPage++;
					showHidePages(thisFileList, Gallery, Identifier);
					isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
					return false;
				});
				this.prevButton.click(function(event) {
					event.preventDefault();
					if($(this).hasClass('disabled')){return false;}
					//currentPageList.isotope('destroy');
					thisFileList.currentPage--;
					showHidePages(thisFileList, Gallery, Identifier);
					isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
					return false;
				});
				this.firstButton.click(function(event) {
					event.preventDefault();
					if($(this).hasClass('disabled')){return false;}
					//currentPageList.isotope('destroy');
					thisFileList.currentPage = 0;
					showHidePages(thisFileList, Gallery, Identifier);
					isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
					return false;
				});
				this.lastButton.click(function(event) {
					event.preventDefault();
					if($(this).hasClass('disabled')){return false;}
					//currentPageList.isotope('destroy');
					thisFileList.currentPage = thisFileList.totalPages - 1;
					showHidePages(thisFileList, Gallery, Identifier);
					isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
					return false;
				});
			};
			$('.paginationPagers li a').click(function(event){
				event.preventDefault();
				//currentPageList.isotope('destroy');
				thisFileList.currentPage = $(this).text() - 1;
				showHidePages(thisFileList, Gallery, Identifier);
				thisFileList.pagerLinksContainer.toggle();
				isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
				return false;
			});
			// Filter Click Events
			this.showFilterBtn.click(function(event){
				event.preventDefault();
				$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				thisFileList.sortingLinksContainer.hide();
				thisFileList.pagerLinksContainer.hide();
				thisFileList.filterLinksContainer.toggle();
				return false;
			});
			$('.Closer', this.filterLinksContainer).click(function(event){
				event.preventDefault();
				thisFileList.sortingLinksContainer.hide();
				thisFileList.pagerLinksContainer.hide();
				thisFileList.filterLinksContainer.toggle();
				return false;
			});
			this.filterLinks.click(function(event){
				event.preventDefault();
				//currentPageList.isotope('destroy');
				$(this).toggleClass('showing');
				var typeToShowA = $('.paginationFilters a.showing', thisFileList.allThumbsContainer);
				var typesString = "";
				if (typeToShowA.length > 0){
					thisFileList.fileContainer.show();
					$.each(typeToShowA, function(){
						typesString += "." + $(this).data('filter-type') + ',';
					});
					thisFileList.filteredThumbs.remove();
					thisFileList.filteredThumbs = thisFileList.allThumbs.filter(typesString.slice(0,-1));
					if(thisFileList.filteredThumbs.length === 0){
						showHideMessage("Sorry, no images of selected type(s) could be found.", thisFileList, Gallery, Identifier);
					} else {
						thisFileList.fileContainer.append(thisFileList.filteredThumbs);
						sortGallery(SortingOrder, SortingType, thisFileList, Gallery);
						thisFileList.currentPage = 0;
						resetPaging(thisFileList);
						showHidePages(thisFileList, Gallery, Identifier);
						showHideMessage("", thisFileList, Gallery, Identifier);
					};
					$(".Selections").css("max-height", 300);
				} else {
					if (((Gallery) && (!opts.albumsFilterAllEnabled)) || ((!Gallery) && (!opts.photosFilterAllEnabled))) {
						thisFileList.filteredThumbs.remove();
						thisFileList.filteredThumbs = thisFileList.allThumbs;
						thisFileList.fileContainer.append(thisFileList.filteredThumbs);
						sortGallery(SortingOrder, SortingType, thisFileList, Gallery);
						thisFileList.currentPage = 0;
						resetPaging(thisFileList);
						showHidePages(thisFileList, Gallery, Identifier);
						showHideMessage("", thisFileList, Gallery, Identifier);
						$(".Selections").css("max-height", 300);
					} else {
						showHideMessage('No types selected.', thisFileList, Gallery, Identifier);
						$(".Selections").css("max-height", 75);
						thisFileList.fileContainer.hide();
					}
				};
				if ((opts.showThumbInfoInPageBar == true && opts.paginationLayoutAlbums == false)) {
					$('.thumbnailStageA', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage)) * parseInt(thisFileList.PerPageItems) + 1));
					$('.thumbnailStageB', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems < parseInt(thisFileList.filteredThumbs.length) ? ((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems) : parseInt(thisFileList.filteredThumbs.length)));
					$('.thumbnailStageC', thisFileList.allThumbsContainer).text(parseInt(thisFileList.filteredThumbs.length));
				}
				isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
				if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
					$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				setTimeout(function(){
					if (isInIFrame) {
						var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
						var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
						var IFrameID = getIframeID(this);
						if (IFrameID != "N/A") {
							window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							/*if (galleryResponsive) {
								window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
								parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
							}*/
						}
					}
				}, 100);
				return false;
			});
			// Search Click Events
			this.searchBox.focus(function(e){
				$(this).addClass("active");
				if($(this).val() === thisFileList.settings.searchBoxDefault){
					$(this).val("");
				};
			});
			this.searchBox.blur(function(e){
				$(this).removeClass("active");
				if($(this).val() === "") {
					$(this).val(thisFileList.settings.searchBoxDefault);
				};
				$.fn.lazyloadanything('load');
			});
			this.searchBox.keydown(function (e){
				if(e.keyCode === 13){
					thisFileList.searchButton.click();
				};
			});
			this.searchButton.click(function(e){
				e.preventDefault();
				if (thisFileList.searchBox.val() !== "" && thisFileList.searchBox.val() !== thisFileList.settings.searchBoxDefault){
					thisFileList.searchBox.removeClass("error");
					thisFileList.filteredThumbs.remove();
					thisFileList.filteredThumbs = thisFileList.allThumbs.filter(':containsNC(' + thisFileList.searchBox.val() + ')');
					if(thisFileList.filteredThumbs.length > 0){
						//currentPageList.isotope('destroy');
						thisFileList.fileContainer.append(thisFileList.filteredThumbs);
						thisFileList.currentPage = 0;
						resetPaging(thisFileList);
						showHidePages(thisFileList, Gallery, Identifier);
						showHideMessage("", thisFileList, Gallery, Identifier);
						thisFileList.clearSearchButton.removeClass('hidden');
						thisFileList.fileContainer.show();
						isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
					} else {
						thisFileList.clearSearchButton.removeClass('hidden');
						showHideMessage("No albums matching your keyword(s) could be found.", thisFileList, Gallery, Identifier);
						thisFileList.fileContainer.hide();
					};
				} else {
					thisFileList.searchBox.addClass("error");
				};
				if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
					$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				setTimeout(function(){
					if (isInIFrame) {
						var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
						var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
						var IFrameID = getIframeID(this);
						if (IFrameID != "N/A") {
							window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
							/*if (galleryResponsive) {
								window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
								parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
							}*/
						}
					}
				}, 100);
			});
			this.clearSearchButton.click(function(e){
				e.preventDefault();
				//currentPageList.isotope('destroy');
				thisFileList.searchBox.val(thisFileList.SearchBox);
				resetWholeList(thisFileList, currentPageList, Identifier, Gallery, firstRun);
				isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
				return false;
			});
			// Show Options - Sorting Button
			this.showSortingBtn.click(function(event){
				event.preventDefault();
				$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				thisFileList.filterLinksContainer.hide();
				thisFileList.pagerLinksContainer.hide();
				thisFileList.sortingLinksContainer.toggle();
				return false;
			});
			// Show Options - Paging Button
			this.showPagerBtn.click(function(event){
				event.preventDefault();
				$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				thisFileList.filterLinksContainer.hide();
				thisFileList.sortingLinksContainer.hide();
				thisFileList.pagerLinksContainer.toggle();
				return false;
			});
			$('.Closer', this.sortingLinksContainer).click(function(){
				thisFileList.filterLinksContainer.hide();
				thisFileList.pagerLinksContainer.hide();
				thisFileList.sortingLinksContainer.toggle();
				return false;
			});
			$('.Closer', this.pagerLinksContainer).click(function(){
				thisFileList.filterLinksContainer.hide();
				thisFileList.sortingLinksContainer.hide();
				thisFileList.pagerLinksContainer.toggle();
				return false;
			});
			// Sorting Links Click Events
			this.sortingLinks.click(function(event){
				event.preventDefault();
				//currentPageList.isotope('destroy');
				var clicked = $(this);
				var sortType = clicked.attr("data-sort-type");
				var sortDirection = "";
				var sortLinks = thisFileList.sortingLinks;
				sortDirection = clicked.attr("data-sort-direction");
				sortLinks.attr('class', '');
				if (sortDirection === "" || sortDirection === undefined){
					sortLinks.attr('data-sort-direction', '');
					sortDirection = 'asc';
				} else {
					if (clicked.attr('data-sort-direction') === 'asc'){
						sortDirection = 'dec';
					} else{
						sortDirection = 'asc';
					};
				};
				clicked.attr('data-sort-direction', sortDirection);
				clicked.addClass(sortDirection);
				SortingOrder = sortDirection;
				SortingType = sortType;
				sortGallery(sortDirection, sortType, thisFileList, Gallery);
				thisFileList.currentPage = 0;
				resetPaging(thisFileList);
				showHidePages(thisFileList, Gallery, Identifier);
				showHideMessage("", thisFileList, Gallery, Identifier);
				if (thisFileList.sortingLinksContainer.is(":hidden") == false){
					thisFileList.sortingLinksContainer.toggle();
				};
				isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
				return false;
			});
			// Show All Files Button
			$('.btn', thisFileList.messageBox).click(function(event){
				event.preventDefault();
				//currentPageList.isotope('destroy');
				thisFileList.searchBox.val(thisFileList.SearchBox);
				resetWholeList(thisFileList, currentPageList, Identifier, Gallery, firstRun);
				isotopeGallery(thisFileList, currentPageList, Gallery, false, Identifier);
				if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
					$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
					$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
				}
				return false;
			});
			initialSorting(thisFileList, currentPageList, Gallery);
			showHidePages(thisFileList, Gallery, Identifier);
			isotopeGallery(thisFileList, currentPageList, Gallery, true, Identifier);
		}

		// Other Functions used for Pagination & Layout
		function showHideMessage (message, thisFileList, Gallery, Identifier){
			$("#ErrorMessage", thisFileList.messageBox).html(message);
			if (message.length > 0){
				thisFileList.filteredThumbs.remove();
				thisFileList.currentPage = 0;
				$('.thumbnailStageA', thisFileList.allThumbsContainer).text(0);
				$('.thumbnailStageB', thisFileList.allThumbsContainer).text(0);
				thisFileList.totalPages = 1;
				showHidePages(thisFileList, Gallery, Identifier);
				thisFileList.messageBox.show();
			} else {
				thisFileList.messageBox.hide();
			};
		};
		function showHidePages(thisFileList, Gallery, Identifier){
			thisFileList.filteredThumbs.hide();
			thisFileList.filteredThumbs.filter(':lt(' + ((parseInt(thisFileList.currentPage) + 1) * parseInt(thisFileList.pageAt)) + ')').addClass("Showing").removeClass("Hiding").show();
			thisFileList.filteredThumbs.filter(':lt(' + ((parseInt(thisFileList.currentPage) + 0) * parseInt(thisFileList.pageAt)) + ')').addClass("Hiding").removeClass("Showing").removeClass("isotope-item").hide();
			thisFileList.fileContainer.removeClass('loading');
			if (thisFileList.currentPage === 0){
				thisFileList.prevButton.addClass('disabled');
				thisFileList.firstButton.addClass('disabled');
			} else {
				thisFileList.prevButton.removeClass('disabled');
				thisFileList.firstButton.removeClass('disabled');
			};
			if (thisFileList.currentPage === (thisFileList.totalPages - 1)){
				thisFileList.nextButton.addClass('disabled');
				thisFileList.lastButton.addClass('disabled');
			} else {
				thisFileList.nextButton.removeClass('disabled');
				thisFileList.lastButton.removeClass('disabled');
			};
			if (thisFileList.totalPages > 1){
				thisFileList.paginationContainer.show();
				$('.currentPage', thisFileList.allThumbsContainer).text(parseInt(thisFileList.currentPage) + 1);
				$('.thumbnailStageA', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage)) * parseInt(thisFileList.PerPageItems) + 1));
				$('.thumbnailStageB', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems < parseInt(thisFileList.filteredThumbs.length) ? ((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems) : parseInt(thisFileList.filteredThumbs.length)));
				$('.thumbnailStageC', thisFileList.allThumbsContainer).text(parseInt(thisFileList.filteredThumbs.length));
				$('.paginationPagers > ul > li > a').each(function() {
					if (this.innerHTML == (thisFileList.currentPage + 1)) {
						$(this).addClass('Active');
						$(this).removeClass('InActive');
						$(this).removeClass('Disabled');
					} else if (this.innerHTML > thisFileList.totalPages) {
						$(this).removeClass('Active');
						$(this).removeClass('InActive');
						$(this).addClass('Disabled');
					} else {
						$(this).removeClass('Active');
						$(this).removeClass('Disabled');
						$(this).addClass('InActive');
					};
				});
				thisFileList.showPagerBtn.show();
				resetPaging(thisFileList);
			} else {
				thisFileList.paginationContainer.hide();
				thisFileList.showPagerBtn.hide();
				if (TotalThumbs == 1) {
					thisFileList.showSortingBtn.hide();
				} else {
					thisFileList.showSortingBtn.show();
				};
			};
			var CurrentFiles = 0;
			$('.paginationItem', this.allThumbsContainer).each(function(index) {
				if ($(this).is(":visible")) {
					CurrentFiles = CurrentFiles + 1;
					$(this).addClass("Showing");
				} else {
					$(this).removeClass("Showing");
				}
			});
			thisFileList.paginationControls.show();
			thisFileList.fileContainer.show();
			$('span', thisFileList.emptyThumbsBox).html("");
			$('span', thisFileList.messageBox).html("");
			thisFileList.emptyThumbsBox.hide();
			if (Gallery) {
				// Add Rotate Effect to Album Thumbnails
				if ((opts.albumThumbRotate) && ($.isFunction($.fn.jrumble))) {
					$('.albumThumb').jrumble({
						x: 				opts.albumRumbleX,
						y: 				opts.albumRumbleY,
						rotation: 		opts.albumRotate,
						speed: 			opts.albumRumbleSpeed,
						opacity:		false,
						opacityMin:		0.6
					});
					$('.albumThumb').hover(function(){
						$(this).trigger('startRumble');
					}, function(){
						$(this).trigger('stopRumble');
					});
				};
				// Add Overlay Effect for Album Thumbnails
				if (opts.albumThumbOverlay) {
					$(".fb-album-overlay").css("opacity", "0");
					$(".fb-album-overlay").hover(function () {
						$(this).stop().animate({opacity: .5}, "slow");
					}, function () {
						$(this).stop().animate({opacity: 0}, "slow");
					});
				} else {
					$(".fb-album-overlay").css("display", "none");
				}
			} else {
				// Add Rotate Effect to Photo Thumbnails
				if ((opts.photoThumbRotate) && ($.isFunction($.fn.jrumble))) {
					$('.photoThumb').jrumble({
						x: 				opts.photoRumbleX,
						y: 				opts.photoRumbleY,
						rotation: 		opts.photoRotate,
						speed: 			opts.photoRumbleSpeed,
						opacity:		false,
						opacityMin:		0.6
					});
					$('.photoThumb').hover(function(){
						$(this).trigger('startRumble');
					}, function(){
						$(this).trigger('stopRumble');
					});
				}
				// Add Overlay Effect for Photo Thumbnails
				if (opts.photoThumbOverlay) {
					$(".fb-photo-overlay").css("opacity", "0");
					$(".fb-photo-overlay").hover(function () {
						$(this).stop().animate({opacity: .5}, "slow");
					}, function () {
						$(this).stop().animate({opacity: 0}, "slow");
					});
				} else {
					$(".fb-photo-overlay").css("display", "none");
				}
			}
			if ((opts.showThumbInfoInPageBar == true && (Gallery == true && opts.paginationLayoutAlbums == false) || (Gallery == false && opts.paginationLayoutPhotos == false))) {
				$('.thumbnailStageA', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage)) * parseInt(thisFileList.PerPageItems) + 1));
				$('.thumbnailStageB', thisFileList.allThumbsContainer).text(((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems < parseInt(thisFileList.filteredThumbs.length) ? ((parseInt(thisFileList.currentPage) + 1) * thisFileList.PerPageItems) : parseInt(thisFileList.filteredThumbs.length)));
				$('.thumbnailStageC', thisFileList.allThumbsContainer).text(parseInt(thisFileList.filteredThumbs.length));
			}
			// Restart LazyLoad
			$.fn.lazyloadanything('load');
			// Adjust Height of iFrame if applicable
			setTimeout(function(){
				if (isInIFrame) {
					var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
					var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
					var IFrameID = getIframeID(this);
					if (IFrameID != "N/A") {
						window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
						parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
						/*if (galleryResponsive) {
							window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
							parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
						}*/
					}
				}
			}, 100);
		};
		function resetWholeList(thisFileList, currentPageList, Identifier, Gallery, firstRun){
			thisFileList.filteredThumbs.remove();
			thisFileList.filteredThumbs = thisFileList.allThumbs;
			thisFileList.fileContainer.append(thisFileList.filteredThumbs);
			if ((Gallery) && (opts.albumsFilterAllEnabled)) {
				thisFileList.filterLinks.addClass('showing');
			} else if ((!Gallery) && (opts.photosFilterAllEnabled)) {
				thisFileList.filterLinks.addClass('showing');
			}
			thisFileList.currentPage = 0;
			resetPaging(thisFileList);
			//showHidePages(thisFileList, Gallery, Identifier);
			showHideMessage("", thisFileList, Gallery, Identifier);
			thisFileList.clearSearchButton.addClass('hidden');
			if (typeof $("#Sort-Selections-" + Identifier).find(".asc").attr("data-sort-type") != 'undefined') {
				sortGallery('asc', $("#Sort-Selections-" + Identifier).find(".asc").attr("data-sort-type"), thisFileList, Gallery);
			} else {
				sortGallery('dec', $("#Sort-Selections-" + Identifier).find(".dec").attr("data-sort-type"), thisFileList, Gallery);
			}
			$(".Selections").css("max-height", 300);
			//currentPageList.isotope('flush');
			currentPageList.isotope('reloadItems');
			currentPageList.isotope({
				itemSelector: 				'.albumWrapper',
				animationEngine:			'best-available',
				itemPositionDataEnabled: 	false,
				transformsEnabled:			true,
				resizesContainer: 			true,
				masonry: {
					columnOffset: 			0
				},
				layoutMode: 				'masonry',
				filter:						'.Showing',
				onLayout: function( $elems, instance ) {
					if (!firstRun) {
						$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
					}
				}
			}, function($elems){});
			showHidePages(thisFileList, Gallery, Identifier);
			if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
				$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
			}
			if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
				$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
			}
			if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
				$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
			}
			setTimeout(function(){
				if (isInIFrame) {
					var galleryContainerHeight = $("#" + opts.frameID).height() + opts.iFrameHeightAdjust;
					var galleryContainerWidth = $("#" + opts.frameID).width() + 4;
					var IFrameID = getIframeID(this);
					if (IFrameID != "N/A") {
						window.top.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
						parent.document.getElementById("" + IFrameID + "").style.height = "" + galleryContainerHeight + "px";
						/*if (galleryResponsive) {
							window.top.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
							parent.document.getElementById("" + IFrameID + "").style.width = "" + galleryContainerWidth + "px";
						}*/
					}
				}
			}, 100);
		};
		function resetPaging(thisFileList){
			thisFileList.totalPages = Math.ceil(thisFileList.filteredThumbs.length / thisFileList.pageAt);
			$('.totalPages', thisFileList.allThumbsContainer).text(thisFileList.totalPages);
		};
		function isotopeGallery(thisFileList, currentPageList, Gallery, firstRun, Identifier) {
			if (Gallery) {
				if (firstRun) {
					setTimeout(function(){
						currentPageList.isotope({
							itemSelector: 				'.albumWrapper',
							animationEngine:			'best-available',
							itemPositionDataEnabled: 	false,
							transformsEnabled:			true,
							resizesContainer: 			true,
							masonry: {
								columnOffset: 			0
							},
							layoutMode: 				'masonry',
							filter:						'.Showing',
							onLayout: function( $elems, instance ) {
								isotopeHeightContainer = currentPageList.height();
								if ((opts.floatingControlBar) && (!isInIFrame)) {
									$("#paginationControls-" + Identifier).stickyScroll({ container: $("#fb-albums-all-paged") })
								};
								if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
									$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
								if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
									$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
								if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
									$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
							}
						}, function($elems){});
					}, 100);
				} else {
					//currentPageList.isotope('flush');
					currentPageList.isotope('reloadItems');
					currentPageList.isotope({
						itemSelector: 				'.albumWrapper',
						animationEngine:			'best-available',
						itemPositionDataEnabled: 	false,
						transformsEnabled:			true,
						resizesContainer: 			true,
						masonry: {
							columnOffset: 			0
						},
						layoutMode: 				'masonry',
						filter:						'.Showing',
						onLayout: function( $elems, instance ) {
							isotopeHeightContainer = currentPageList.height();
							$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
							if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
								$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							}
							if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
								$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							}
							if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
								$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							}
						}
					}, function($elems){});
				}
			} else {
				if (firstRun) {
					setTimeout(function(){
						currentPageList.isotope({
							itemSelector: 				'.photoWrapper',
							animationEngine:			'best-available',
							itemPositionDataEnabled: 	false,
							transformsEnabled:			true,
							resizesContainer: 			true,
							masonry: {
								columnOffset: 			0
							},
							layoutMode: 				'masonry',
							filter:						'.Showing',
							onLayout: function( $elems, instance ) {
								isotopeHeightContainer = currentPageList.height();
								if ((opts.floatingControlBar) && (!isInIFrame)) {
									$("#paginationControls-" + Identifier).stickyScroll({ container: $("#fb-album-paged-" + Identifier) });
								};
								if (opts.showFloatingReturnButton) {
									$('#Back-' + albumId + '_3').unbind("click").bind('click', function(e){
										if (!opts.cacheAlbumContents) {
											removeAlbumDOM(Identifier);
										}
										checkExisting($(this).attr('data-href'));
									});
								};
								if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
									$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
								if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
									$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
								if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
									$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
								};
							}
						}, function($elems){});
					}, 100);
				} else {
					//currentPageList.isotope('flush');
					currentPageList.isotope('reloadItems');
					currentPageList.isotope({
						itemSelector: 				'.photoWrapper',
						animationEngine:			'best-available',
						itemPositionDataEnabled: 	false,
						transformsEnabled:			true,
						resizesContainer: 			true,
						masonry: {
							columnOffset: 			0
						},
						layoutMode: 				'masonry',
						filter:						'.Showing',
						onLayout: function( $elems, instance ) {
							isotopeHeightContainer = currentPageList.height();
							$('html, body').animate({scrollTop:$("#" + opts.frameID).offset().top - 20}, 'slow');
							if ($('#paginationFilters-' + Identifier, this.allThumbsContainer).length != 0) {
								$('#paginationFilters-' + Identifier, this.allThumbsContainer).css("left", $('#showFilterBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							};
							if ($('#paginationSorting-' + Identifier, this.allThumbsContainer).length !=0) {
								$('#paginationSorting-' + Identifier, this.allThumbsContainer).css("left", $('#showSortingBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							};
							if ($('#paginationPagers-' + Identifier, this.allThumbsContainer).length !=0) {
								$('#paginationPagers-' + Identifier, this.allThumbsContainer).css("left", $('#showPagerBtn-' + Identifier, this.allThumbsContainer).position().left + 0);
							};
						}
					}, function($elems){});
				}
			}
			return false;
		}
		function initialSorting(thisFileList, currentPageList, Gallery) {
			if (Gallery) {
				if (defaultSortTypeAlbums == 'albumTitle') 				{sortType = "bytitle"
				} else if (defaultSortTypeAlbums == 'numberItems') 		{sortType = "bysize"
				} else if (defaultSortTypeAlbums == 'createDate') 		{sortType = "bycreate"
				} else if (defaultSortTypeAlbums == 'updateDate') 		{sortType = "byupdate"
				} else if (defaultSortTypeAlbums == 'orderFacebook') 	{sortType = "byorder"
				} else if (defaultSortTypeAlbums == 'FacebookID') 		{sortType = "byID"
				}
				sortGallery((opts.defaultSortDirectionASC == true ? "asc" : "dec"), sortType, thisFileList, Gallery);
			} else {
				if (defaultSortTypePhotos == 'addedDate') 				{sortType = "byadded"
				} else if (defaultSortTypePhotos == 'updateDate') 		{sortType = "byupdate"
				} else if (defaultSortTypePhotos == 'orderFacebook') 	{sortType = "byorder"
				} else if (defaultSortTypePhotos == 'FacebookID') 		{sortType = "byID"
				}
				sortGallery((opts.defaultPhotoDirectionsASC == true ? "asc" : "dec"), sortType, thisFileList, Gallery);
			}
		}
		function sortGallery(direction, sortType, thisFileList, Gallery){
			thisFileList.filteredThumbs.remove();
			var sortAlg = direction + "_" + sortType;
			switch (sortAlg){
				case 'asc_byorder':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_byorder);
					break;
				case 'dec_byorder':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_byorder);
					break;
				case 'asc_bysize':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_bysize);
					break;
				case 'dec_bysize':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_bysize);
					break;
				case 'asc_bycreate':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_bycreate);
					break;
				case 'dec_bycreate':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_bycreate);
					break;
				case 'asc_byupdate':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_byupdate);
					break;
				case 'dec_byupdate':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_byupdate);
					break;
				case 'asc_bytitle':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_bytitle);
					break;
				case 'dec_bytitle':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_bytitle);
					break;
				case 'asc_byadded':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_byadded);
					break;
				case 'dec_byadded':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_byadded);
					break;
				case 'asc_byID':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_byID);
					break;
				case 'dec_byID':
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(dec_byID);
					break;
				default:
					thisFileList.filteredThumbs = thisFileList.filteredThumbs.sort(asc_bytitle);
					break;
			};
			thisFileList.fileContainer.hide().append(thisFileList.filteredThumbs).fadeIn('slow');;
		};
		function sortFilters(propName) {
			return function (a,b) {
				// return a[propName] - b[propName];
				var aVal = a[propName], bVal = b[propName];
				if (opts.sortFilterNewToOld) {
					return aVal < bVal ? 1 : (aVal > bVal ?  - 1 : 0);
				} else {
					return aVal > bVal ? 1 : (aVal < bVal ?  - 1 : 0);
				}

			};
		}
		function asc_byorder(a, b){
			var compA = parseInt($(a).attr("data-order"));
			var compB = parseInt($(b).attr("data-order"));
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_byorder(a, b){
			var compA = parseInt($(a).attr("data-order"));
			var compB = parseInt($(b).attr("data-order"));
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_bysize(a, b){
			var compA = parseInt($(a).attr("data-count"));
			var compB = parseInt($(b).attr("data-count"));
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_bysize(a, b){
			var compA = parseInt($(a).attr("data-count"));
			var compB = parseInt($(b).attr("data-count"));
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_bycreate(a, b){
			var compA = $(a).attr("data-create").toUpperCase();
			var compB = $(b).attr("data-create").toUpperCase();
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_bycreate(a, b){
			var compA = $(a).attr("data-create").toUpperCase();
			var compB = $(b).attr("data-create").toUpperCase();
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_byupdate(a, b){
			var compA = $(a).attr("data-update").toUpperCase();
			var compB = $(b).attr("data-update").toUpperCase();
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_byupdate(a, b){
			var compA = $(a).attr("data-update").toUpperCase();
			var compB = $(b).attr("data-update").toUpperCase();
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_byadded(a, b){
			var compA = $(a).attr("data-added").toUpperCase();
			var compB = $(b).attr("data-added").toUpperCase();
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_byadded(a, b){
			var compA = $(a).attr("data-added").toUpperCase();
			var compB = $(b).attr("data-added").toUpperCase();
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_bytitle(a, b){
			var compA = $(a).attr("data-title").toUpperCase();
			var compB = $(b).attr("data-title").toUpperCase();
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_bytitle(a, b){
			var compA = $(a).attr("data-title").toUpperCase();
			var compB = $(b).attr("data-title").toUpperCase();
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};
		function asc_byID(a, b){
			var compA = $(a).attr("data-id").toUpperCase();
			var compB = $(b).attr("data-id").toUpperCase();
			return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
		};
		function dec_byID(a, b){
			var compA = $(a).attr("data-id").toUpperCase();
			var compB = $(b).attr("data-id").toUpperCase();
			return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
		};

		// Create Necessary HTML Markup for Gallery
		$("#" + opts.frameID).append("<div id='" + opts.loaderID + "' class='clearfix'></div>");
		$("#" + opts.frameID).append("<div id='" + opts.galleryID + "' class='clearfix'></div>");

		if (opts.responsiveGallery) {
			$("#" + opts.frameID).css("width", opts.responsiveWidth + "%").css("padding", (opts.floatingControlBar == true ? "0px 0px 5px 0px" : "5px 0px"));
		} else {
			$("#" + opts.frameID).css("width", opts.fixedWidth + "px").css("padding", (opts.floatingControlBar == true ? "0px 0px 5px 0px" : "5px 0px"));
		}

		$("<div>", {id : "fb-album-header"}).appendTo("#" + opts.galleryID);
		$("<div>", {id : "fb-album-content"}).appendTo("#" + opts.galleryID);

		if (opts.showBottomControlBar) {$("<div>", {id : "fb-album-footer"}).appendTo("#" + opts.galleryID);}

		//if (!opts.paginationLayoutAlbums) {$('.album').hide();} else {$('.paginationMain').hide();}
		$('.paginationMain').hide();

		// Initialize qTip2 Tooltips as live mouseover event
		if ((opts.tooltipUseInternal) && ($.isFunction($.fn.qtip))) {
			var qTipShared = {
				prerender: 			false,
				overwrite: 			true,
				hide: {
					target: 		false,
					event: 			'mouseleave mousedown unfocus click',
					effect: 		true,
					delay: 			0,
					fixed: 			true,
					inactive: 		5000,
					leave: 			"window"
				},
				events: {
					render: 		null,
					move: 			null,
					show: 			null,
					hide: 			null,
					toggle: 		null,
					focus: 			null,
					blur: 			null
				},
				show: {
					target: 		false,
					event: 			'mouseenter',
					effect: 		true,
					delay: 			90,
					solo: 			false,
					ready: 			false,
					modal: 			false
				}
			};
			// Initialize Tooltips for Album Thumbnail Section
			$('body').on('mouseover', '.TipGallery', function() {
				// Make sure to only apply one tooltip per element!
				if( typeof( $(this).data('qtip') ) == 'object' ) {
					return;
				};
				$(this).qtip( $.extend({}, qTipShared, {
					style: {
						classes: 		opts.tooltipDesign,
						def: 			true,
						widget: 		false,
						width: 			220,
						tip: {
							corner: 	false,
							mimic: 		false,
							width: 		8,
							height: 	8,
							border: 	true,
							offset: 	0
						}
					},
					content: {
						text: 			true,
						attr: 			opts.tooltipTipAnchor,
						title: {
							text: 		false,
							button: 	false
						}
					},
					position: {
						my: 			'top center',
						at: 			'bottom center',
						target: 		'mouse',
						container: 		false,
						viewport: 		$(window),
						adjust: {
							x: 			0,
							y: 			30,
							mouse: 		true,
							resize: 	true,
							method: 	'shift none'
						},
						effect: 		true
					}
				}));
				$(this).qtip('show');
			});
			// Initialize Tooltips for Photo Thumbnail Section
			$('body').on('mouseover', '.TipPhoto', function() {
				// Make sure to only apply one tooltip per element!
				if( typeof( $(this).data('qtip') ) == 'object' ) {
					return;
				};
				$(this).qtip( $.extend({}, qTipShared, {
					style: {
						classes: 		opts.tooltipDesign,
						def: 			true,
						widget: 		false,
						width: 			400,
						tip: {
							corner: 	false,
							mimic: 		false,
							width: 		8,
							height: 	8,
							border: 	true,
							offset: 	0
						}
					},
					content: {
						text: 			true,
						attr: 			opts.tooltipTipAnchor,
						title: {
							text: 		false,
							button: 	false
						}
					},
					position: {
						my: 			'top center',
						at: 			'bottom center',
						target: 		'mouse',
						container: 		false,
						viewport: 		$(window),
						adjust: {
							x: 			0,
							y: 			30,
							mouse: 		true,
							resize: 	true,
							method: 	'shift none'
						},
						effect: 		true
					}
				}));
				$(this).qtip('show');
			});
			// Initialize Tooltips for Lightbox Section
			$('body').on('mouseover', '.TipLightbox', function() {
				// Make sure to only apply one tooltip per element!
				if( typeof( $(this).data('qtip') ) == 'object' ) {
					return;
				};
				$(this).qtip( $.extend({}, qTipShared, {
					style: {
						classes: 		opts.tooltipDesign,
						def: 			true,
						widget: 		false,
						width: 			400,
						tip: {
							corner: 	false,
							mimic: 		false,
							width: 		8,
							height: 	8,
							border: 	true,
							offset: 	0
						}
					},
					content: {
						text: 			true,
						attr: 			opts.tooltipTipAnchor,
						title: {
							text: 		false,
							button: 	false
						}
					},
					position: {
						my: 			'bottom center',
						at: 			'top center',
						target: 		'mouse',
						container: 		false,
						viewport: 		$(window),
						adjust: {
							x: 			0,
							y: 			0,
							mouse: 		true,
							resize: 	true,
							method: 	'shift none'
						},
						effect: 		true
					}
				}));
				$(this).qtip('show');
			});
		}

		galleryAlbumsInit();
    };
})(jQuery);


// -------------------------------
// Other External REQUIRED Plugins
// -------------------------------

// jQuery Shorten
(function(a){function s(g,c){return c.measureText(g).width}function t(g,c){c.text(g);return c.width()}var q=false,o,j,k;a.fn.shorten=function(){var g={},c=arguments,r=c.callee;if(c.length)if(c[0].constructor==Object)g=c[0];else if(c[0]=="options")return a(this).eq(0).data("shorten-options");else g={width:parseInt(c[0]),tail:c[1]};this.css("visibility","hidden");var h=a.extend({},r.defaults,g);return this.each(function(){var e=a(this),d=e.text(),p=d.length,i,f=a("<span/>").html(h.tail).text(),l={shortened:false, textOverflow:false};i=e.css("float")!="none"?h.width||e.width():h.width||e.parent().width();if(i<0)return true;e.data("shorten-options",h);this.style.display="block";this.style.whiteSpace="nowrap";if(o){var b=a(this),n=document.createElement("canvas");ctx=n.getContext("2d");b.html(n);ctx.font=b.css("font-style")+" "+b.css("font-variant")+" "+b.css("font-weight")+" "+Math.ceil(parseFloat(b.css("font-size")))+"px "+b.css("font-family");j=ctx;k=s}else{b=a('<table style="padding:0; margin:0; border:none; font:inherit;width:auto;zoom:1;position:absolute;"><tr style="padding:0; margin:0; border:none; font:inherit;"><td style="padding:0; margin:0; border:none; font:inherit;white-space:nowrap;"></td></tr></table>'); $td=a("td",b);a(this).html(b);j=$td;k=t}b=k.call(this,d,j);if(b<i){e.text(d);this.style.visibility="visible";e.data("shorten-info",l);return true}h.tooltip&&this.setAttribute("title",d);if(r._native&&!g.width){n=a("<span>"+h.tail+"</span>").text();if(n.length==1&&n.charCodeAt(0)==8230){e.text(d);this.style.overflow="hidden";this.style[r._native]="ellipsis";this.style.visibility="visible";l.shortened=true;l.textOverflow="ellipsis";e.data("shorten-info",l);return true}}f=k.call(this,f,j);i-=f;f=i*1.15; if(b-f>0){f=d.substring(0,Math.ceil(p*(f/b)));if(k.call(this,f,j)>i){d=f;p=d.length}}do{p--;d=d.substring(0,p)}while(k.call(this,d,j)>=i);e.html(a.trim(a("<span/>").text(d).html())+h.tail);this.style.visibility="visible";l.shortened=true;e.data("shorten-info",l);return true})};var m=document.documentElement.style;if("textOverflow"in m)q="textOverflow";else if("OTextOverflow"in m)q="OTextOverflow";if(typeof Modernizr!="undefined"&&Modernizr.canvastext)o=Modernizr.canvastext;else{m=document.createElement("canvas"); o=!!(m.getContext&&m.getContext("2d")&&typeof m.getContext("2d").fillText==="function")}a.fn.shorten._is_canvasTextSupported=o;a.fn.shorten._native=q;a.fn.shorten.defaults={tail:"&hellip;",tooltip:true}})(jQuery);

// jQuery LazyLoadAnything
(function( $ ) {
	// Cache jQuery window
	var $window = $(window);
	// Force load flag
	var force_load_flag = false;
	// Plugin methods
	var methods = {
		'init': function(options) {
			var defaults = {
				'auto': 			true,
				'cache': 			false,
				'timeout': 			1000,
				'includeMargin': 	false,
				'repeatLoad': 		false,
				'onLoadingStart': function(e, llelements, indexes) {
					return true;
				},
				'onLoad': function(e, llelement) {
					return true;
				},
				'onLoadingComplete': function(e, llelements, indexes) {
					return true;
				}
			};
			var settings = $.extend({}, defaults, options);
			var timeout = 0;
			var llelements = [];
			var $selector = this;
			// Scroll listener
			$window.bind('scroll.lla', function(e) {
				// Check for manually/auto load
				if(!force_load_flag && !settings.auto) return false;
				force_load_flag = false;
				// Clear timeout if scrolling continues
				clearTimeout(timeout);
				// Set the timeout for onLoad
				timeout = setTimeout(function() {
					var load_elements = [];
					var windowScrollTop = $(window).scrollTop();
					var windowScrollBottom = windowScrollTop + $(window).height();
					var i, llelem_top, llelem_bottom;
					// Cycle through llelements and check if they are within viewpane
					for (i = 0; i < llelements.length; i++) {
						// Get top and bottom of llelem
						llelem_top = llelements[i].getTop();
						llelem_bottom = llelements[i].getBottom();
						if (
							// Top edge
							(llelem_top >= windowScrollTop && llelem_top <= windowScrollBottom) ||
							// Bottom edge
							(llelem_bottom >= windowScrollTop && llelem_bottom <= windowScrollBottom) ||
							// In full view
							(llelem_top <= windowScrollTop && llelem_bottom >= windowScrollBottom)) {
								// Grab index of llelements that will be loaded
								if (settings.repeatLoad || !llelements[i].loaded) load_elements.push(i);
							}
					};
					// Call onLoadingStart event
					if(settings.onLoadingStart.call(undefined, e, llelements, load_elements)) {
						// Cycle through array of indexes that will be loaded
						for(i = 0; i < load_elements.length; i++) {
							// Set loaded flag
							llelements[load_elements[i]].loaded = true;
							// Call the individual element onLoad
							if(settings.onLoad.call(undefined, e, llelements[load_elements[i]]) === false) break;
						}
						// Call onLoadingComplete event
						settings.onLoadingComplete.call(undefined, e, llelements, load_elements);
					};
				}, settings.timeout);
			});
			// LazyLoadElement class
			function LazyLoadElement($element) {
				var self = this;
				this.loaded = false;
				this.$element = $element;
				this.top = undefined;
				this.bottom = undefined;
				this.getTop = function() {
					if (self.top) return self.top;
					return self.$element.offset().top;
				};
				this.getBottom = function() {
					if(self.bottom) return self.bottom;
					var top = (self.top) ? self.top : this.getTop();
					return top + self.$element.outerHeight(settings.includeMargin);
				};
				// Cache the top and bottom of set
				if(settings.cache) {
					this.top = this.getTop();
					this.bottom = this.getBottom();
				};
			};
			// Cycle throught the selector(s)
			var chain = $selector.each(function() {
				// Add LazyLoadElement classes to the main array
				llelements.push(new LazyLoadElement($(this)));
			});
			return chain;
		},

		'destroy': function() {
			$window.unbind('scroll.lla');
		},

		'load': function() {
			force_load_flag = true;
			$window.trigger('scroll.lla');
		}
	};

	$.fn.lazyloadanything = function(method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lazyloadanything');
		}
	};
})( jQuery );

// waitForImages 1.4.2
(function ($) {
    // Namespace all events.
    var eventNamespace = 'waitForImages';

    // CSS properties which contain references to images.
    $.waitForImages = {
        hasImageProperties: ['backgroundImage', 'listStyleImage', 'borderImage', 'borderCornerImage']
    };

    // Custom selector to find `img` elements that have a valid `src` attribute and have not already loaded.
    $.expr[':'].uncached = function (obj) {
        // Ensure we are dealing with an `img` element with a valid `src` attribute.
        if (!$(obj).is('img[src!=""]')) {
            return false;
        }

        // Firefox's `complete` property will always be `true` even if the image has not been downloaded.
        // Doing it this way works in Firefox.
        var img = new Image();
        img.src = obj.src;
        return !img.complete;
    };

    $.fn.waitForImages = function (finishedCallback, eachCallback, waitForAll) {

        var allImgsLength = 0;
        var allImgsLoaded = 0;

        // Handle options object.
        if ($.isPlainObject(arguments[0])) {
            waitForAll = arguments[0].waitForAll;
            eachCallback = arguments[0].each;
			// This must be last as arguments[0]
			// is aliased with finishedCallback.
            finishedCallback = arguments[0].finished;
        }

        // Handle missing callbacks.
        finishedCallback = finishedCallback || $.noop;
        eachCallback = eachCallback || $.noop;

        // Convert waitForAll to Boolean
        waitForAll = !! waitForAll;

        // Ensure callbacks are functions.
        if (!$.isFunction(finishedCallback) || !$.isFunction(eachCallback)) {
            throw new TypeError('An invalid callback was supplied.');
        }

        return this.each(function () {
            // Build a list of all imgs, dependent on what images will be considered.
            var obj = $(this);
            var allImgs = [];
            // CSS properties which may contain an image.
            var hasImgProperties = $.waitForImages.hasImageProperties || [];
            // To match `url()` references.
            // Spec: http://www.w3.org/TR/CSS2/syndata.html#value-def-uri
            var matchUrl = /url\(\s*(['"]?)(.*?)\1\s*\)/g;

            if (waitForAll) {

                // Get all elements (including the original), as any one of them could have a background image.
                obj.find('*').andSelf().each(function () {
                    var element = $(this);

                    // If an `img` element, add it. But keep iterating in case it has a background image too.
                    if (element.is('img:uncached')) {
                        allImgs.push({
                            src: element.attr('src'),
                            element: element[0]
                        });
                    }

                    $.each(hasImgProperties, function (i, property) {
                        var propertyValue = element.css(property);
                        var match;

                        // If it doesn't contain this property, skip.
                        if (!propertyValue) {
                            return true;
                        }

                        // Get all url() of this element.
                        while (match = matchUrl.exec(propertyValue)) {
                            allImgs.push({
                                src: match[2],
                                element: element[0]
                            });
                        }
                    });
                });
            } else {
                // For images only, the task is simpler.
                obj.find('img:uncached')
                    .each(function () {
                    allImgs.push({
                        src: this.src,
                        element: this
                    });
                });
            }

            allImgsLength = allImgs.length;
            allImgsLoaded = 0;

            // If no images found, don't bother.
            if (allImgsLength === 0) {
                finishedCallback.call(obj[0]);
            }

            $.each(allImgs, function (i, img) {

                var image = new Image();

                // Handle the image loading and error with the same callback.
                $(image).bind('load.' + eventNamespace + ' error.' + eventNamespace, function (event) {
                    allImgsLoaded++;

                    // If an error occurred with loading the image, set the third argument accordingly.
                    eachCallback.call(img.element, allImgsLoaded, allImgsLength, event.type == 'load');

                    if (allImgsLoaded == allImgsLength) {
                        finishedCallback.call(obj[0]);
                        return false;
                    }

                });

                image.src = img.src;
            });
        });
    };
}(jQuery));

// Custom Layout Mode for Isotope (Centered Masonry)
(function ($) {
	if ($.isFunction($.fn.isotope)) {
		$.Isotope.prototype.flush = function() {
			this.$allAtoms = $();
			this.$filteredAtoms = $();
			//this.element.children().remove();
			//this.reLayout();
		};
		$.Isotope.prototype._getCenteredMasonryColumns = function() {
			// Assign equal height to all elements to match fitRows effect
			var maxHeight = -1;
			$('.albumWrapper').each(function() {
				maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			});
			$('.albumWrapper').each(function() {
				$(this).height(maxHeight);
			});
			var columnOffset = this.options.masonry && this.options.masonry.columnOffset || 0;
			this.width = this.element.width();
			var parentWidth = this.element.parent().width();
			// i.e. options.masonry && options.masonry.columnWidth
			var colW = this.options.masonry && this.options.masonry.columnWidth ||
			// or use the size of the first item
			this.$filteredAtoms.outerWidth(true) ||
			// if there's no items, use size of container
			parentWidth;
			var cols = Math.floor( parentWidth / colW );
			cols = Math.max( cols, 1 );
			// i.e. this.masonry.cols = ....
			this.masonry.cols = cols;
			// i.e. this.masonry.columnWidth = ...
			this.masonry.columnWidth = colW ;
		};
		$.Isotope.prototype._masonryReset = function() {
			// layout-specific props
			this.masonry = {};
			// FIXME shouldn't have to call this again
			this._getCenteredMasonryColumns();
			var i = this.masonry.cols;
			this.masonry.colYs = [];
			while (i--) {
				this.masonry.colYs.push( 0 );
			}
		};
		$.Isotope.prototype._masonryResizeChanged = function() {
			var prevColCount = this.masonry.cols;
			// get updated colCount
			this._getCenteredMasonryColumns();
			return ( this.masonry.cols !== prevColCount );
		};
		$.Isotope.prototype._masonryGetContainerSize = function() {
			var unusedCols = 0, i = this.masonry.cols;
			var gutter = this.options.masonry && this.options.masonry.gutterWidth || 0;
			// count unused columns
			while ( --i ) {
				if ( this.masonry.colYs[i] !== 0 ) {
					break;
				}
				unusedCols++;
			}
			return {
				height : Math.max.apply( Math, this.masonry.colYs ),
				// fit container to columns that have been used;
				width : (this.masonry.cols - unusedCols) * this.masonry.columnWidth
			};
		};
	};
})( jQuery );

// StickyScroll Plugin for Controlbar
(function($) {
    $.fn.stickyScroll = function(options) {
        var methods = {
            init : function(options) {
                var settings;
                if (options.mode !== 'auto' && options.mode !== 'manual') {
                    if (options.container) {
                        options.mode = 'auto';
                    }
                    if (options.bottomBoundary) {
                        options.mode = 'manual';
                    }
                };
                settings = $.extend({
                    mode: 				'auto', // 'auto' or 'manual'
                    container: 			$('body'),
                    topBoundary: 		null,
                    bottomBoundary: 	null
                }, options);
                function bottomBoundary() {
					return settings.container.offset().top + isotopeHeightContainer;
                };
                function topBoundary() {
                    return settings.container.offset().top
                };
                function elHeight(el) {
                    return $(el).attr('offsetHeight');
                };
                // make sure user input is a jQuery object
                settings.container = $(settings.container);
                if(!settings.container.length) {
                    if(console) {
                        console.log('StickyScroll: the element ' + options.container + ' does not exist, we\'re throwing in the towel.');
                    };
                    return;
                };
                // calculate automatic bottomBoundary
                if(settings.mode === 'auto') {
                    settings.topBoundary 	= topBoundary();
                    settings.bottomBoundary = bottomBoundary();
                };
                return this.each(function(index) {
                    var el = $(this), win = $(window), id = XDate.now() + index, height = elHeight(el);
                    el.data('sticky-id', id);
                    win.bind('scroll.stickyscroll-' + id, function() {
                        var top = $(document).scrollTop();
						var bottom = top - isotopeHeightContainer;
						//$("#PositionControl").html("Offset: " + settings.topBoundary + "</br></br>Container: " + isotopeHeightContainer + "</br></br>Limit: " + settings.bottomBoundary + "</br></br>Top: " + top + "</br></br>Bottom: " + (bottom - settings.topBoundary));
                        if (bottom - settings.topBoundary >= 0) {
							// Don't follow mouse further once bottom of container has been reached
                            el.offset({
                                top: $(document).height() - settings.bottomBoundary - height
                            }).removeClass('sticky-active').removeClass('sticky-inactive').addClass('sticky-stopped');
							//$(".Scroll_To_Top").show().css("marginLeft", "10px");
							$(".Scroll_To_Top").show();
                        } else if (top > settings.topBoundary) {
							// Follow mouse as long as container is still visible
                            el.offset({
                                top: $(window).scrollTop() + controlBarAdjust
                            }).removeClass('sticky-stopped').removeClass('sticky-inactive').addClass('sticky-active');
							//$(".Scroll_To_Top").show().css("marginLeft", "10px");
							$(".Scroll_To_Top").show();
                        } else if (top < settings.topBoundary) {
							// Return to original position as after page load
                            el.css({
                                position: 	'',
                                top: 		'',
                                bottom: 	''
                            })
                            .removeClass('sticky-stopped')
                            .removeClass('sticky-active')
                            .addClass('sticky-inactive');
							//$(".Scroll_To_Top").hide().css("marginLeft", "0px");
							$(".Scroll_To_Top").hide();
                        }
                    });
                    win.bind('resize.stickyscroll-' + id, function() {
                        if (settings.mode === 'auto') {
                            settings.topBoundary 		= topBoundary();
                            settings.bottomBoundary 	= bottomBoundary();
                        };
                        height = elHeight(el);
                        $(this).scroll();
                    });
                    el.addClass('sticky-processed');
                    // start it off
                    win.scroll();
                });
            },
            reset : function() {
                return this.each(function() {
                    var el = $(this), id = el.data('sticky-id');
                    el.css({
                        position: '',
                        top: '',
                        bottom: ''
                    }).removeClass('sticky-stopped').removeClass('sticky-active').removeClass('sticky-inactive').removeClass('sticky-processed');
                    $(window).unbind('.stickyscroll-' + id);
                });
            }
        };
        // if options is a valid method, execute it
        if (methods[options]) {
            return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof options === 'object' || !options) {
            return methods.init.apply(this, arguments);
        } else if(console) {
            console.log('Method' + options + ' does not exist on jQuery.stickyScroll');
        };
    };
})(jQuery);

// moment.js Plugin
(function(e){function O(e,t){return function(n){return j(e.call(this,n),t)}}function M(e){return function(t){return this.lang().ordinal(e.call(this,t))}}function _(){}function D(e){H(this,e)}function P(e){var t=this._data={},n=e.years||e.year||e.y||0,r=e.months||e.month||e.M||0,i=e.weeks||e.week||e.w||0,s=e.days||e.day||e.d||0,o=e.hours||e.hour||e.h||0,u=e.minutes||e.minute||e.m||0,a=e.seconds||e.second||e.s||0,f=e.milliseconds||e.millisecond||e.ms||0;this._milliseconds=f+a*1e3+u*6e4+o*36e5,this._days=s+i*7,this._months=r+n*12,t.milliseconds=f%1e3,a+=B(f/1e3),t.seconds=a%60,u+=B(a/60),t.minutes=u%60,o+=B(u/60),t.hours=o%24,s+=B(o/24),s+=i*7,t.days=s%30,r+=B(s/30),t.months=r%12,n+=B(r/12),t.years=n}function H(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}function B(e){return e<0?Math.ceil(e):Math.floor(e)}function j(e,t){var n=e+"";while(n.length<t)n="0"+n;return n}function F(e,t,n){var r=t._milliseconds,i=t._days,s=t._months,o;r&&e._d.setTime(+e+r*n),i&&e.date(e.date()+i*n),s&&(o=e.date(),e.date(1).month(e.month()+s*n).date(Math.min(o,e.daysInMonth())))}function I(e){return Object.prototype.toString.call(e)==="[object Array]"}function q(e,t){var n=Math.min(e.length,t.length),r=Math.abs(e.length-t.length),i=0,s;for(s=0;s<n;s++)~~e[s]!==~~t[s]&&i++;return i+r}function R(e,t){return t.abbr=e,s[e]||(s[e]=new _),s[e].set(t),s[e]}function U(e){return e?(!s[e]&&o&&require("./lang/"+e),s[e]):t.fn._lang}function z(e){return e.match(/\[.*\]/)?e.replace(/^\[|\]$/g,""):e.replace(/\\/g,"")}function W(e){var t=e.match(a),n,r;for(n=0,r=t.length;n<r;n++)A[t[n]]?t[n]=A[t[n]]:t[n]=z(t[n]);return function(i){var s="";for(n=0;n<r;n++)s+=typeof t[n].call=="function"?t[n].call(i,e):t[n];return s}}function X(e,t){function r(t){return e.lang().longDateFormat(t)||t}var n=5;while(n--&&f.test(t))t=t.replace(f,r);return C[t]||(C[t]=W(t)),C[t](e)}function V(e){switch(e){case"DDDD":return p;case"YYYY":return d;case"YYYYY":return v;case"S":case"SS":case"SSS":case"DDD":return h;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":case"a":case"A":return m;case"X":return b;case"Z":case"ZZ":return g;case"T":return y;case"MM":case"DD":case"YY":case"HH":case"hh":case"mm":case"ss":case"M":case"D":case"d":case"H":case"h":case"m":case"s":return c;default:return new RegExp(e.replace("\\",""))}}function $(e,t,n){var r,i,s=n._a;switch(e){case"M":case"MM":s[1]=t==null?0:~~t-1;break;case"MMM":case"MMMM":r=U(n._l).monthsParse(t),r!=null?s[1]=r:n._isValid=!1;break;case"D":case"DD":case"DDD":case"DDDD":t!=null&&(s[2]=~~t);break;case"YY":s[0]=~~t+(~~t>68?1900:2e3);break;case"YYYY":case"YYYYY":s[0]=~~t;break;case"a":case"A":n._isPm=(t+"").toLowerCase()==="pm";break;case"H":case"HH":case"h":case"hh":s[3]=~~t;break;case"m":case"mm":s[4]=~~t;break;case"s":case"ss":s[5]=~~t;break;case"S":case"SS":case"SSS":s[6]=~~(("0."+t)*1e3);break;case"X":n._d=new Date(parseFloat(t)*1e3);break;case"Z":case"ZZ":n._useUTC=!0,r=(t+"").match(x),r&&r[1]&&(n._tzh=~~r[1]),r&&r[2]&&(n._tzm=~~r[2]),r&&r[0]==="+"&&(n._tzh=-n._tzh,n._tzm=-n._tzm)}t==null&&(n._isValid=!1)}function J(e){var t,n,r=[];if(e._d)return;for(t=0;t<7;t++)e._a[t]=r[t]=e._a[t]==null?t===2?1:0:e._a[t];r[3]+=e._tzh||0,r[4]+=e._tzm||0,n=new Date(0),e._useUTC?(n.setUTCFullYear(r[0],r[1],r[2]),n.setUTCHours(r[3],r[4],r[5],r[6])):(n.setFullYear(r[0],r[1],r[2]),n.setHours(r[3],r[4],r[5],r[6])),e._d=n}function K(e){var t=e._f.match(a),n=e._i,r,i;e._a=[];for(r=0;r<t.length;r++)i=(V(t[r]).exec(n)||[])[0],i&&(n=n.slice(n.indexOf(i)+i.length)),A[t[r]]&&$(t[r],i,e);e._isPm&&e._a[3]<12&&(e._a[3]+=12),e._isPm===!1&&e._a[3]===12&&(e._a[3]=0),J(e)}function Q(e){var t,n,r,i=99,s,o,u;while(e._f.length){t=H({},e),t._f=e._f.pop(),K(t),n=new D(t);if(n.isValid()){r=n;break}u=q(t._a,n.toArray()),u<i&&(i=u,r=n)}H(e,r)}function G(e){var t,n=e._i;if(w.exec(n)){e._f="YYYY-MM-DDT";for(t=0;t<4;t++)if(S[t][1].exec(n)){e._f+=S[t][0];break}g.exec(n)&&(e._f+=" Z"),K(e)}else e._d=new Date(n)}function Y(t){var n=t._i,r=u.exec(n);n===e?t._d=new Date:r?t._d=new Date(+r[1]):typeof n=="string"?G(t):I(n)?(t._a=n.slice(0),J(t)):t._d=n instanceof Date?new Date(+n):new Date(n)}function Z(e,t,n,r,i){return i.relativeTime(t||1,!!n,e,r)}function et(e,t,n){var i=r(Math.abs(e)/1e3),s=r(i/60),o=r(s/60),u=r(o/24),a=r(u/365),f=i<45&&["s",i]||s===1&&["m"]||s<45&&["mm",s]||o===1&&["h"]||o<22&&["hh",o]||u===1&&["d"]||u<=25&&["dd",u]||u<=45&&["M"]||u<345&&["MM",r(u/30)]||a===1&&["y"]||["yy",a];return f[2]=t,f[3]=e>0,f[4]=n,Z.apply({},f)}function tt(e,n,r){var i=r-n,s=r-e.day();return s>i&&(s-=7),s<i-7&&(s+=7),Math.ceil(t(e).add("d",s).dayOfYear()/7)}function nt(e){var n=e._i,r=e._f;return n===null||n===""?null:(typeof n=="string"&&(e._i=n=U().preparse(n)),t.isMoment(n)?(e=H({},n),e._d=new Date(+n._d)):r?I(r)?Q(e):K(e):Y(e),new D(e))}function rt(e,n){t.fn[e]=t.fn[e+"s"]=function(e){var t=this._isUTC?"UTC":"";return e!=null?(this._d["set"+t+n](e),this):this._d["get"+t+n]()}}function it(e){t.duration.fn[e]=function(){return this._data[e]}}function st(e,n){t.duration.fn["as"+e]=function(){return+this/n}}var t,n="2.0.0",r=Math.round,i,s={},o=typeof module!="undefined"&&module.exports,u=/^\/?Date\((\-?\d+)/i,a=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|a|A|hh?|HH?|mm?|ss?|SS?S?|X|zz?|ZZ?|.)/g,f=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,l=/([0-9a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)/gi,c=/\d\d?/,h=/\d{1,3}/,p=/\d{3}/,d=/\d{1,4}/,v=/[+\-]?\d{1,6}/,m=/[0-9]*[a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF]+\s*?[\u0600-\u06FF]+/i,g=/Z|[\+\-]\d\d:?\d\d/i,y=/T/i,b=/[\+\-]?\d+(\.\d{1,3})?/,w=/^\s*\d{4}-\d\d-\d\d((T| )(\d\d(:\d\d(:\d\d(\.\d\d?\d?)?)?)?)?([\+\-]\d\d:?\d\d)?)?/,E="YYYY-MM-DDTHH:mm:ssZ",S=[["HH:mm:ss.S",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],x=/([\+\-]|\d\d)/gi,T="Month|Date|Hours|Minutes|Seconds|Milliseconds".split("|"),N={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},C={},k="DDD w W M D d".split(" "),L="M D H h m s w W".split(" "),A={M:function(){return this.month()+1},MMM:function(e){return this.lang().monthsShort(this,e)},MMMM:function(e){return this.lang().months(this,e)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(e){return this.lang().weekdaysMin(this,e)},ddd:function(e){return this.lang().weekdaysShort(this,e)},dddd:function(e){return this.lang().weekdays(this,e)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return j(this.year()%100,2)},YYYY:function(){return j(this.year(),4)},YYYYY:function(){return j(this.year(),5)},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return~~(this.milliseconds()/100)},SS:function(){return j(~~(this.milliseconds()/10),2)},SSS:function(){return j(this.milliseconds(),3)},Z:function(){var e=-this.zone(),t="+";return e<0&&(e=-e,t="-"),t+j(~~(e/60),2)+":"+j(~~e%60,2)},ZZ:function(){var e=-this.zone(),t="+";return e<0&&(e=-e,t="-"),t+j(~~(10*e/6),4)},X:function(){return this.unix()}};while(k.length)i=k.pop(),A[i+"o"]=M(A[i]);while(L.length)i=L.pop(),A[i+i]=O(A[i],2);A.DDDD=O(A.DDD,3),_.prototype={set:function(e){var t,n;for(n in e)t=e[n],typeof t=="function"?this[n]=t:this["_"+n]=t},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(e){return this._months[e.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(e){return this._monthsShort[e.month()]},monthsParse:function(e){var n,r,i,s;this._monthsParse||(this._monthsParse=[]);for(n=0;n<12;n++){this._monthsParse[n]||(r=t([2e3,n]),i="^"+this.months(r,"")+"|^"+this.monthsShort(r,""),this._monthsParse[n]=new RegExp(i.replace(".",""),"i"));if(this._monthsParse[n].test(e))return n}},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(e){return this._weekdays[e.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(e){return this._weekdaysShort[e.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(e){return this._weekdaysMin[e.day()]},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(e){var t=this._longDateFormat[e];return!t&&this._longDateFormat[e.toUpperCase()]&&(t=this._longDateFormat[e.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(e){return e.slice(1)}),this._longDateFormat[e]=t),t},meridiem:function(e,t,n){return e>11?n?"pm":"PM":n?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[last] dddd [at] LT",sameElse:"L"},calendar:function(e,t){var n=this._calendar[e];return typeof n=="function"?n.apply(t):n},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(e,t,n,r){var i=this._relativeTime[n];return typeof i=="function"?i(e,t,n,r):i.replace(/%d/i,e)},pastFuture:function(e,t){var n=this._relativeTime[e>0?"future":"past"];return typeof n=="function"?n(t):n.replace(/%s/i,t)},ordinal:function(e){return this._ordinal.replace("%d",e)},_ordinal:"%d",preparse:function(e){return e},postformat:function(e){return e},week:function(e){return tt(e,this._week.dow,this._week.doy)},_week:{dow:0,doy:6}},t=function(e,t,n){return nt({_i:e,_f:t,_l:n,_isUTC:!1})},t.utc=function(e,t,n){return nt({_useUTC:!0,_isUTC:!0,_l:n,_i:e,_f:t})},t.unix=function(e){return t(e*1e3)},t.duration=function(e,n){var r=t.isDuration(e),i=typeof e=="number",s=r?e._data:i?{}:e,o;return i&&(n?s[n]=e:s.milliseconds=e),o=new P(s),r&&e.hasOwnProperty("_lang")&&(o._lang=e._lang),o},t.version=n,t.defaultFormat=E,t.lang=function(e,n){var r;if(!e)return t.fn._lang._abbr;n?R(e,n):s[e]||U(e),t.duration.fn._lang=t.fn._lang=U(e)},t.langData=function(e){return e&&e._lang&&e._lang._abbr&&(e=e._lang._abbr),U(e)},t.isMoment=function(e){return e instanceof D},t.isDuration=function(e){return e instanceof P},t.fn=D.prototype={clone:function(){return t(this)},valueOf:function(){return+this._d},unix:function(){return Math.floor(+this._d/1e3)},toString:function(){return this.format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._d},toJSON:function(){return t.utc(this).format("YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var e=this;return[e.year(),e.month(),e.date(),e.hours(),e.minutes(),e.seconds(),e.milliseconds()]},isValid:function(){return this._isValid==null&&(this._a?this._isValid=!q(this._a,(this._isUTC?t.utc(this._a):t(this._a)).toArray()):this._isValid=!isNaN(this._d.getTime())),!!this._isValid},utc:function(){return this._isUTC=!0,this},local:function(){return this._isUTC=!1,this},format:function(e){var n=X(this,e||t.defaultFormat);return this.lang().postformat(n)},add:function(e,n){var r;return typeof e=="string"?r=t.duration(+n,e):r=t.duration(e,n),F(this,r,1),this},subtract:function(e,n){var r;return typeof e=="string"?r=t.duration(+n,e):r=t.duration(e,n),F(this,r,-1),this},diff:function(e,n,r){var i=this._isUTC?t(e).utc():t(e).local(),s=(this.zone()-i.zone())*6e4,o,u;return n&&(n=n.replace(/s$/,"")),n==="year"||n==="month"?(o=(this.daysInMonth()+i.daysInMonth())*432e5,u=(this.year()-i.year())*12+(this.month()-i.month()),u+=(this-t(this).startOf("month")-(i-t(i).startOf("month")))/o,n==="year"&&(u/=12)):(o=this-i-s,u=n==="second"?o/1e3:n==="minute"?o/6e4:n==="hour"?o/36e5:n==="day"?o/864e5:n==="week"?o/6048e5:o),r?u:B(u)},from:function(e,n){return t.duration(this.diff(e)).lang(this.lang()._abbr).humanize(!n)},fromNow:function(e){return this.from(t(),e)},calendar:function(){var e=this.diff(t().startOf("day"),"days",!0),n=e<-6?"sameElse":e<-1?"lastWeek":e<0?"lastDay":e<1?"sameDay":e<2?"nextDay":e<7?"nextWeek":"sameElse";return this.format(this.lang().calendar(n,this))},isLeapYear:function(){var e=this.year();return e%4===0&&e%100!==0||e%400===0},isDST:function(){return this.zone()<t([this.year()]).zone()||this.zone()<t([this.year(),5]).zone()},day:function(e){var t=this._isUTC?this._d.getUTCDay():this._d.getDay();return e==null?t:this.add({d:e-t})},startOf:function(e){e=e.replace(/s$/,"");switch(e){case"year":this.month(0);case"month":this.date(1);case"week":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return e==="week"&&this.day(0),this},endOf:function(e){return this.startOf(e).add(e.replace(/s?$/,"s"),1).subtract("ms",1)},isAfter:function(e,n){return n=typeof n!="undefined"?n:"millisecond",+this.clone().startOf(n)>+t(e).startOf(n)},isBefore:function(e,n){return n=typeof n!="undefined"?n:"millisecond",+this.clone().startOf(n)<+t(e).startOf(n)},isSame:function(e,n){return n=typeof n!="undefined"?n:"millisecond",+this.clone().startOf(n)===+t(e).startOf(n)},zone:function(){return this._isUTC?0:this._d.getTimezoneOffset()},daysInMonth:function(){return t.utc([this.year(),this.month()+1,0]).date()},dayOfYear:function(e){var n=r((t(this).startOf("day")-t(this).startOf("year"))/864e5)+1;return e==null?n:this.add("d",e-n)},isoWeek:function(e){var t=tt(this,1,4);return e==null?t:this.add("d",(e-t)*7)},week:function(e){var t=this.lang().week(this);return e==null?t:this.add("d",(e-t)*7)},lang:function(t){return t===e?this._lang:(this._lang=U(t),this)}};for(i=0;i<T.length;i++)rt(T[i].toLowerCase().replace(/s$/,""),T[i]);rt("year","FullYear"),t.fn.days=t.fn.day,t.fn.weeks=t.fn.week,t.fn.isoWeeks=t.fn.isoWeek,t.duration.fn=P.prototype={weeks:function(){return B(this.days()/7)},valueOf:function(){return this._milliseconds+this._days*864e5+this._months*2592e6},humanize:function(e){var t=+this,n=et(t,!e,this.lang());return e&&(n=this.lang().pastFuture(t,n)),this.lang().postformat(n)},lang:t.fn.lang};for(i in N)N.hasOwnProperty(i)&&(st(i,N[i]),it(i.toLowerCase()));st("Weeks",6048e5),t.lang("en",{ordinal:function(e){var t=e%10,n=~~(e%100/10)===1?"th":t===1?"st":t===2?"nd":t===3?"rd":"th";return e+n}}),o&&(module.exports=t),typeof ender=="undefined"&&(this.moment=t),typeof define=="function"&&define.amd&&define("moment",[],function(){return t})}).call(this);

// XDate v0.7 (Docs & Licensing: http://arshaw.com/xdate/)
var XDate = function (g, m, A, p) {
    function f() {
        var a = this instanceof f ? this : new f,
            c = arguments,
            b = c.length,
            d;
        typeof c[b - 1] == "boolean" && (d = c[--b], c = q(c, 0, b));
        if (b) if (b == 1) if (b = c[0], b instanceof g || typeof b == "number") a[0] = new g(+b);
        else if (b instanceof f) {
            var c = a,
                h = new g(+b[0]);
            if (l(b)) h.toString = w;
            c[0] = h
        } else {
            if (typeof b == "string") {
                a[0] = new g(0);
                a: {
                    for (var c = b, b = d || !1, h = f.parsers, r = 0, e; r < h.length; r++) if (e = h[r](c, b, a)) {
                        a = e;
                        break a
                    }
                    a[0] = new g(c)
                }
            }
        } else a[0] = new g(n.apply(g, c)), d || (a[0] = s(a[0]));
        else a[0] = new g;
        typeof d == "boolean" && B(a, d);
        return a
    }
    function l(a) {
        return a[0].toString === w
    }
    function B(a, c, b) {
        if (c) {
            if (!l(a)) b && (a[0] = new g(n(a[0].getFullYear(), a[0].getMonth(), a[0].getDate(), a[0].getHours(), a[0].getMinutes(), a[0].getSeconds(), a[0].getMilliseconds()))), a[0].toString = w
        } else l(a) && (a[0] = b ? s(a[0]) : new g(+a[0]));
        return a
    }
    function C(a, c, b, d, h) {
        var e = k(j, a[0], h),
            a = k(D, a[0], h),
            h = c == 1 ? b % 12 : e(1),
            f = !1;
        d.length == 2 && typeof d[1] == "boolean" && (f = d[1], d = [b]);
        a(c, d);
        f && e(1) != h && (a(1, [e(1) - 1]), a(2, [E(e(0), e(1))]))
    }

    function F(a, c, b, d) {
        var b = Number(b),
            h = m.floor(b);
        a["set" + o[c]](a["get" + o[c]]() + h, d || !1);
        h != b && c < 6 && F(a, c + 1, (b - h) * G[c], d)
    }
    function H(a, c, b) {
        var a = a.clone().setUTCMode(!0, !0),
            c = f(c).setUTCMode(!0, !0),
            d = 0;
        if (b == 0 || b == 1) {
            for (var h = 6; h >= b; h--) d /= G[h], d += j(c, !1, h) - j(a, !1, h);
            b == 1 && (d += (c.getFullYear() - a.getFullYear()) * 12)
        } else b == 2 ? (b = a.toDate().setUTCHours(0, 0, 0, 0), d = c.toDate().setUTCHours(0, 0, 0, 0), d = m.round((d - b) / 864E5) + (c - d - (a - b)) / 864E5) : d = (c - a) / [36E5, 6E4, 1E3, 1][b - 3];
        return d
    }
    function t(a) {
        var c = a(0),
            b = a(1),
            a = a(2),
            b = new g(n(c, b, a)),
            d = u(c),
            a = d;
        b < d ? a = u(c - 1) : (c = u(c + 1), b >= c && (a = c));
        return m.floor(m.round((b - a) / 864E5) / 7) + 1
    }
    function u(a) {
        a = new g(n(a, 0, 4));
        a.setUTCDate(a.getUTCDate() - (a.getUTCDay() + 6) % 7);
        return a
    }
    function I(a, c, b, d) {
        var h = k(j, a, d),
            e = k(D, a, d),
            b = u(b === p ? h(0) : b);
        d || (b = s(b));
        a.setTime(+b);
        e(2, [h(2) + (c - 1) * 7])
    }
    function J(a, c, b, d, e) {
        var r = f.locales,
            g = r[f.defaultLocale] || {}, i = k(j, a, e),
            b = (typeof b == "string" ? r[b] : b) || {};
        return x(a, c, function (a) {
            if (d) for (var b = (a == 7 ? 2 : a) - 1; b >= 0; b--) d.push(i(b));
            return i(a)
        }, function (a) {
            return b[a] || g[a]
        }, e)
    }
    function x(a, c, b, d, e) {
        for (var f, g, i = ""; f = c.match(M);) {
            i += c.substr(0, f.index);
            if (f[1]) {
                g = i;
                for (var i = a, j = f[1], l = b, m = d, n = e, k = j.length, o = void 0, q = ""; k > 0;) o = N(i, j.substr(0, k), l, m, n), o !== p ? (q += o, j = j.substr(k), k = j.length) : k--;
                i = g + (q + j)
            } else f[3] ? (g = x(a, f[4], b, d, e), parseInt(g.replace(/\D/g, ""), 10) && (i += g)) : i += f[7] || "'";
            c = c.substr(f.index + f[0].length)
        }
        return i + c
    }
    function N(a, c, b, d, e) {
        var g = f.formatters[c];
        if (typeof g == "string") return x(a, g, b, d, e);
        else if (typeof g ==
            "function") return g(a, e || !1, d);
        switch (c) {
            case "fff":
                return i(b(6), 3);
            case "s":
                return b(5);
            case "ss":
                return i(b(5));
            case "m":
                return b(4);
            case "mm":
                return i(b(4));
            case "h":
                return b(3) % 12 || 12;
            case "hh":
                return i(b(3) % 12 || 12);
            case "H":
                return b(3);
            case "HH":
                return i(b(3));
            case "d":
                return b(2);
            case "dd":
                return i(b(2));
            case "ddd":
                return d("dayNamesShort")[b(7)] || "";
            case "dddd":
                return d("dayNames")[b(7)] || "";
            case "M":
                return b(1) + 1;
            case "MM":
                return i(b(1) + 1);
            case "MMM":
                return d("monthNamesShort")[b(1)] || "";
            case "MMMM":
                return d("monthNames")[b(1)] || "";
            case "yy":
                return (b(0) + "").substring(2);
            case "yyyy":
                return b(0);
            case "t":
                return v(b, d).substr(0, 1).toLowerCase();
            case "tt":
                return v(b, d).toLowerCase();
            case "T":
                return v(b, d).substr(0, 1);
            case "TT":
                return v(b, d);
            case "z":
            case "zz":
            case "zzz":
                return e ? c = "Z" : (d = a.getTimezoneOffset(), a = d < 0 ? "+" : "-", b = m.floor(m.abs(d) / 60), d = m.abs(d) % 60, e = b, c == "zz" ? e = i(b) : c == "zzz" && (e = i(b) + ":" + i(d)), c = a + e), c;
            case "w":
                return t(b);
            case "ww":
                return i(t(b));
            case "S":
                return c = b(2), c > 10 && c < 20 ? "th" : ["st", "nd", "rd"][c % 10 - 1] || "th"
        }
    }
    function v(a, c) {
        return a(3) < 12 ? c("amDesignator") : c("pmDesignator")
    }
    function y(a) {
        return !isNaN(+a[0])
    }
    function j(a, c, b) {
        return a["get" + (c ? "UTC" : "") + o[b]]()
    }
    function D(a, c, b, d) {
        a["set" + (c ? "UTC" : "") + o[b]].apply(a, d)
    }
    function s(a) {
        return new g(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours(), a.getUTCMinutes(), a.getUTCSeconds(), a.getUTCMilliseconds())
    }
    function E(a, c) {
        return 32 - (new g(n(a, c, 32))).getUTCDate()
    }
    function z(a) {
        return function () {
            return a.apply(p, [this].concat(q(arguments)))
        }
    }
    function k(a) {
        var c = q(arguments, 1);
        return function () {
            return a.apply(p, c.concat(q(arguments)))
        }
    }
    function q(a, c, b) {
        return A.prototype.slice.call(a, c || 0, b === p ? a.length : b)
    }
    function K(a, c) {
        for (var b = 0; b < a.length; b++) c(a[b], b)
    }
    function i(a, c) {
        c = c || 2;
        for (a += ""; a.length < c;) a = "0" + a;
        return a
    }
    var o = "FullYear,Month,Date,Hours,Minutes,Seconds,Milliseconds,Day,Year".split(","),
        L = ["Years", "Months", "Days"],
        G = [12, 31, 24, 60, 60, 1E3, 1],
        M = /(([a-zA-Z])\2*)|(\((('.*?'|\(.*?\)|.)*?)\))|('(.*?)')/,
        n = g.UTC,
        w = g.prototype.toUTCString,
        e = f.prototype;
    e.length = 1;
    e.splice = A.prototype.splice;
    e.getUTCMode = z(l);
    e.setUTCMode = z(B);
    e.getTimezoneOffset = function () {
        return l(this) ? 0 : this[0].getTimezoneOffset()
    };
    K(o, function (a, c) {
        e["get" + a] = function () {
            return j(this[0], l(this), c)
        };
        c != 8 && (e["getUTC" + a] = function () {
            return j(this[0], !0, c)
        });
        c != 7 && (e["set" + a] = function (a) {
            C(this, c, a, arguments, l(this));
            return this
        }, c != 8 && (e["setUTC" + a] = function (a) {
            C(this, c, a, arguments, !0);
            return this
        }, e["add" + (L[c] || a)] = function (a, d) {
            F(this,
            c, a, d);
            return this
        }, e["diff" + (L[c] || a)] = function (a) {
            return H(this, a, c)
        }))
    });
    e.getWeek = function () {
        return t(k(j, this, !1))
    };
    e.getUTCWeek = function () {
        return t(k(j, this, !0))
    };
    e.setWeek = function (a, c) {
        I(this, a, c, !1);
        return this
    };
    e.setUTCWeek = function (a, c) {
        I(this, a, c, !0);
        return this
    };
    e.addWeeks = function (a) {
        return this.addDays(Number(a) * 7)
    };
    e.diffWeeks = function (a) {
        return H(this, a, 2) / 7
    };
    f.parsers = [function (a, c, b) {
        if (a = a.match(/^(\d{4})(-(\d{2})(-(\d{2})([T ](\d{2}):(\d{2})(:(\d{2})(\.(\d+))?)?(Z|(([-+])(\d{2})(:?(\d{2}))?))?)?)?)?$/)) {
            var d = new g(n(a[1], a[3] ? a[3] - 1 : 0, a[5] || 1, a[7] || 0, a[8] || 0, a[10] || 0, a[12] ? Number("0." + a[12]) * 1E3 : 0));
            a[13] ? a[14] && d.setUTCMinutes(d.getUTCMinutes() + (a[15] == "-" ? 1 : -1) * (Number(a[16]) * 60 + (a[18] ? Number(a[18]) : 0))) : c || (d = s(d));
            return b.setTime(+d)
        }
    }];
    f.parse = function (a) {
        return +f("" + a)
    };
    e.toString = function (a, c, b) {
        return a === p || !y(this) ? this[0].toString() : J(this, a, c, b, l(this))
    };
    e.toUTCString = e.toGMTString = function (a, c, b) {
        return a === p || !y(this) ? this[0].toUTCString() : J(this, a, c, b, !0)
    };
    e.toISOString = function () {
        return this.toUTCString("yyyy-MM-dd'T'HH:mm:ss(.fff)zzz")
    };
    f.defaultLocale = "";
    f.locales = {
        "": {
            monthNames: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
            monthNamesShort: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
            dayNames: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
            dayNamesShort: "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
            amDesignator: "AM",
            pmDesignator: "PM"
        }
    };
    f.formatters = {
        i: "yyyy-MM-dd'T'HH:mm:ss(.fff)",
        u: "yyyy-MM-dd'T'HH:mm:ss(.fff)zzz"
    };
    K("getTime,valueOf,toDateString,toTimeString,toLocaleString,toLocaleDateString,toLocaleTimeString,toJSON".split(","),

    function (a) {
        e[a] = function () {
            return this[0][a]()
        }
    });
    e.setTime = function (a) {
        this[0].setTime(a);
        return this
    };
    e.valid = z(y);
    e.clone = function () {
        return new f(this)
    };
    e.clearTime = function () {
        return this.setHours(0, 0, 0, 0)
    };
    e.toDate = function () {
        return new g(+this[0])
    };
    f.now = function () {
        return +new g
    };
    f.today = function () {
        return (new f).clearTime()
    };
    f.UTC = n;
    f.getDaysInMonth = E;
    if (typeof module !== "undefined" && module.exports) module.exports = f;
    return f
}(Date, Math, Array);

// jRumble v1.3 - http://jackrugile.com/jrumble - MIT License
(function (f) {
    f.fn.jrumble = function (g) {
        var a = f.extend({
            x: 2,
            y: 2,
            rotation: 1,
            speed: 15,
            opacity: false,
            opacityMin: 0.5
        }, g);
        return this.each(function () {
            var b = f(this),
                h = a.x * 2,
                i = a.y * 2,
                k = a.rotation * 2,
                g = a.speed === 0 ? 1 : a.speed,
                m = a.opacity,
                n = a.opacityMin,
                l, j, o = function () {
                    var e = Math.floor(Math.random() * (h + 1)) - h / 2,
                        a = Math.floor(Math.random() * (i + 1)) - i / 2,
                        c = Math.floor(Math.random() * (k + 1)) - k / 2,
                        d = m ? Math.random() + n : 1,
                        e = e === 0 && h !== 0 ? Math.random() < 0.5 ? 1 : -1 : e,
                        a = a === 0 && i !== 0 ? Math.random() < 0.5 ? 1 : -1 : a;
                    b.css("display") === "inline" && (l = true, b.css("display", "inline-block"));
                    b.css({
                        position: "relative",
                        left: e + "px",
                        top: a + "px",
                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=" + d * 100 + ")",
                        filter: "alpha(opacity=" + d * 100 + ")",
                        "-moz-opacity": d,
                        "-khtml-opacity": d,
                        opacity: d,
                        "-webkit-transform": "rotate(" + c + "deg)",
                        "-moz-transform": "rotate(" + c + "deg)",
                        "-ms-transform": "rotate(" + c + "deg)",
                        "-o-transform": "rotate(" + c + "deg)",
                        transform: "rotate(" + c + "deg)"
                    })
                }, p = {
                    left: 0,
                    top: 0,
                    "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)",
                    filter: "alpha(opacity=100)",
                    "-moz-opacity": 1,
                    "-khtml-opacity": 1,
                    opacity: 1,
                    "-webkit-transform": "rotate(0deg)",
                    "-moz-transform": "rotate(0deg)",
                    "-ms-transform": "rotate(0deg)",
                    "-o-transform": "rotate(0deg)",
                    transform: "rotate(0deg)"
                };
            b.bind({
                startRumble: function (a) {
                    a.stopPropagation();
                    clearInterval(j);
                    j = setInterval(o, g)
                },
                stopRumble: function (a) {
                    a.stopPropagation();
                    clearInterval(j);
                    l && b.css("display", "inline");
                    b.css(p)
                }
            })
        })
    }
})(jQuery);
