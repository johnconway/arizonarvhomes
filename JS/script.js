jQuery(document).ready(function($) {

        $('.info_inner').click(function(){
                $('.info').toggleClass('');
        })

        var initialize = function() {
 
  var latlng = new google.maps.LatLng(34.979641,-114.554576);
  var marker = new google.maps.MarkerImage('images/map-banner.png', new google.maps.Size(400,361), new google.maps.Point(0,0));
   
  var options = {       zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.HYBRID,
                        panControl: false,
                        zoomControl: false,
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        overviewMapControl: false
                };
                 
  var map = new google.maps.Map(document.getElementById('map'), options);

  var marker = new google.maps.Marker({ position: latlng,
                                      map: map,
                                      icon: marker, 
                                      title: 'Arizona RV Homes is Valley View @ Sunrise Hills' });

  var bew = [
  {
    featureType: "all",
    stylers: [
      { saturation: -100 }
    ]
  }
];

map.setOptions({styles: bew});

  var tooltip = '<div class="tooltip"><p>Arizona RV Homes</p><p>2530 Nez Perce Rd. - Ft. Mohave AZ 86427</p></div>';
    
  var infowindow = new google.maps.InfoWindow({
        content: tooltip
        })

  google.maps.event.addListener(marker, 'click', function() {
        
        $('.info').toggleClass('table , none');
        
        })

}



window.onload = initialize;


});